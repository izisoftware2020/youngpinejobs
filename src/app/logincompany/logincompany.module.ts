import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginCompanyComponent } from './logincompany.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ApiService } from '../common/api-service/api.service';



@NgModule({
  declarations: [LoginCompanyComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: LoginCompanyComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ApiService],
})
export class LogincompanyModule { }
