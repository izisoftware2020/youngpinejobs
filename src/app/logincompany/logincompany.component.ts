import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Candidate } from '../common/models/20candidate.models';
import { Company } from '../common/models/60company.models';
import { ApiService } from '../common/api-service/api.service';

@Component({
    selector: 'app-logincompany',
    templateUrl: './logincompany.component.html',
    styleUrls: ['./logincompany.component.scss']
})
export class LoginCompanyComponent implements OnInit {

    // cyptoSaltHash = require('crypto-salt-hash');

    constructor(private router: Router, private api: ApiService) { }

    input: Company;

    ngOnInit(): void {
        this.input = {
            email: '',
            password: '',
        }
    }

    /**
     * on Login Click
     */
    onLoginClick() {
        this.api.excuteAllByWhat(this.input, "205").subscribe((data) => {
            console.log(this.input);
            
            if (data.length > 0) {
                localStorage.setItem('companySubject', JSON.stringify(data[0]));
                this.api.companySubject.next(data[0]);

                this.api.showSuccess("Đăng nhập Thành Công ");
                this.router.navigate(['/manager/company/a1-information-company'])
            } else {
                this.api.showError("Email hoặc Password đã sai");
            }
        });
    }

}
