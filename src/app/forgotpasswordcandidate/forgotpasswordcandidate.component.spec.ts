import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordcandidateComponent } from './forgotpasswordcandidate.component';

describe('ForgotpasswordcandidateComponent', () => {
  let component: ForgotpasswordcandidateComponent;
  let fixture: ComponentFixture<ForgotpasswordcandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordcandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordcandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
