import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotpasswordcandidateComponent } from './forgotpasswordcandidate.component';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../common/api-service/api.service';



@NgModule({
  declarations: [ForgotpasswordcandidateComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    MatFormFieldModule,
    RouterModule.forChild([
      {
        path: '', component: ForgotpasswordcandidateComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ApiService],
})
export class ForgotpasswordcandidateModule { }
