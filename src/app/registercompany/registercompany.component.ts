import { Component, OnInit } from "@angular/core";
import { ApiService } from "../common/api-service/api.service";
import { Subscription } from "rxjs";
import { Company } from '../common/models/60company.models';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: "app-registercompany",
    templateUrl: "./registercompany.component.html",
    styleUrls: ["./registercompany.component.scss"],
})
export class RegistercompanyComponent implements OnInit {
    subscription: Subscription[] = [];

    // data source for combobox level
    personalscales: any[] = [
        { value: "1", viewValue: "Dưới 20 người" },
        { value: "2", viewValue: "20 - 150 người" },
        { value: "3", viewValue: "150 - 300 người" },
        { value: "4", viewValue: "Trên 300 người" },
    ];

    // data cityId
    cityId: string = '';

    //data combobox city
    citys: any[] = [];

    //data models
    input: Company;

    form: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder,private router: Router) {
    this.form = this.formBuilder.group({
        fullname: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]],
        password: [null, [Validators.required, Validators.maxLength(50),Validators.minLength(8)]],
        companyphone: [null,[Validators.required,Validators.maxLength(11),Validators.pattern("[0-9]*"),Validators.minLength(10)]],
        email: [null,[Validators.required, Validators.maxLength(50), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
        companyname: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]],
        personalscale:[null,[Validators.required]],
        address: [null, [Validators.required, Validators.minLength(5),Validators.maxLength(100)]],
        city: [null, [Validators.required]],
      });
  }

    /**
     * ngOnDestroy
     */
    ngOnDestroy() {
        this.subscription.forEach((item) => {
            item.unsubscribe();
        });
    }

    ngOnInit(): void {
        this.input = {
            email: "",
            password: "",
            fullname: "",
            companyname: "",
            companyphone: "",
            address: "",
            city: "0",
            personalscale: "0",
            status: "0",
            statusbanner: "1",
            pointcompany: "1",
        };
        this.loadDataCity();
    }

    /**
     * load Data City
     */
    loadDataCity() {
        const param = {};
        this.subscription.push(
            this.api.excuteAllByWhat(param, "147").subscribe((data) => {
                if (data.length > 0) {
                    let temp = [
                        {
                            id: "0",
                            name: "Tỉnh/Thành phố",
                        },
                    ];

                    data.forEach((item) => {
                        temp.push(item);
                    });

                    this.citys = temp;
                    // set first select citys combobox
                    this.cityId = this.citys[0].id;
                }
            })
        );
    }

    //register company
    onSubmitClick() {
        if (this.form.status != "VALID") {
            this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
            return;
        }
        this.api.excuteAllByWhat(this.input, "157").subscribe((data) => {
            if (data) {
                this.api.showSuccess("Đăng ký thành công!");
                this.router.navigate(['/logincompany'])
            }
        });
    }
}
