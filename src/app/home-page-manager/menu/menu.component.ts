import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoginCookie } from '../../common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    menuFlag: boolean = false;
    menuMobileFlag: boolean = false;
    settingButton: boolean = false;
    searchFlag: boolean = false;
    isMobile: boolean = false; 


    constructor(private login: LoginCookie, public api:ApiService) { }

    ngOnInit() {
        this.isMobile = this.isMobileDevice();
    }

    isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    }; 

    //log out candidate
    logOutStaff(){
        this.api.logoutStaff();
    }
}
