import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { MatMenuModule } from '@angular/material/menu';
import { LoginCookie } from 'src/app/common/core/login-cookie'; 
import { MenuCompanyComponent } from './menu-company.component';

@NgModule({
    declarations: [
        MenuCompanyComponent,
    ],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '', component: MenuCompanyComponent, children: [
                    {
                        path: '',
                        loadChildren: () => import('../content-company/content-company.module').then(m => m.ContentCompanyModule)
                    }, 
                ],
            }
        ]), 

        MatMenuModule,

    ],
    providers: [LoginCookie]
})
export class MenuCompanysModule { }
