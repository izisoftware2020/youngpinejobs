import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoginCookie } from '../../common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Company } from 'src/app/common/models/60company.models';

@Component({
    selector: 'app-menu-company',
    templateUrl: './menu-company.component.html',
    styleUrls: ['./menu-company.component.scss']
})
export class MenuCompanyComponent implements OnInit {

    menuFlag: boolean = false;
    menuMobileFlag: boolean = false;
    settingButton: boolean = false;
    searchFlag: boolean = false;
    isMobile: boolean = false; 

    company: Company;

    constructor(
        private login: LoginCookie,
        public api: ApiService
        ) { 
            // company value
            this.company = this.api.getCompanyValue;
            
        }

    ngOnInit() {
        this.isMobile = this.isMobileDevice();
    }

    isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    }; 

    logOutCompany(){
        this.api.logoutCompany();
     }
}
