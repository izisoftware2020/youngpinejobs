import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Candidate } from 'src/app/common/models/20candidate.models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-b5-skill-work',
  templateUrl: './b5-skill-work.component.html',
  styleUrls: ['../content-candidate.component.scss']
})
export class B5SkillWorkComponent implements OnInit {

  subscription: Subscription[] = [];

  // data source for combobox languages
  languages: any[] = [
    { value: '0', viewValue: 'Tiếng Anh A' },
    { value: '1', viewValue: 'Tiếng Anh B' },
    { value: '2', viewValue: 'Tiếng Pháp' },
    { value: '3', viewValue: 'Tiếng Hàn' },
    { value: '4', viewValue: 'Tiếng Trung' },
    { value: '5', viewValue: 'Khác' }
  ];

  // data source for combobox levels
  infomations: any[] = [
    { value: '0', viewValue: 'Tin Văn Phòng' },
    { value: '1', viewValue: 'Tin A' },
    { value: '1', viewValue: 'Tin B' },
  ];

  // menu position
  menu1: boolean = false;
  menu2: boolean = false;
  menu3: boolean = false;
  menu4: boolean = false;
  menu5: boolean = false;
  menu6: boolean = false;
  menu7: boolean = false;
  menu8: boolean = false;
  menu9: boolean = false;
  menu10: boolean = false;

  //data models
  candidate: Candidate = {

  }

  
  form: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    // add validate for controls````
    this.form = this.formBuilder.group({
      language: [null, [Validators.required]],
      infomation: [null, [Validators.required]],
    }); 
  }

  /**
    * ngOnDestroy
    */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    this.loadDataCandidate();
  }

  /**
  * load Data Candidae
  */
  loadDataCandidate() {
    const param = {
      'id': 11,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '24').subscribe(data => {

      if (data.length > 0) {
        this.candidate = data[0];

      }

      // load skill
      if (data[0].skill.search('1') >= 0) {
        this.menu1 = true;
      }
      if (data[0].skill.search('2') >= 0) {
        this.menu2 = true;
      }
      if (data[0].skill.search('3') >= 0) {
        this.menu3 = true;
      }
      if (data[0].skill.search('4') >= 0) {
        this.menu4 = true;
      }
      if (data[0].skill.search('5') >= 0) {
        this.menu5 = true;
      }
      if (data[0].skill.search('6') >= 0) {
        this.menu6 = true;
      }
      if (data[0].skill.search('7') >= 0) {
        this.menu7 = true;
      }
      if (data[0].skill.search('8') >= 0) {
        this.menu8 = true;
      }
      if (data[0].skill.search('9') >= 0) {
        this.menu9 = true;
      }
      if (data[0].skill.search('10') >= 0) {
        this.menu10 = true;
      }
    }));
  }

  /**
    * on Update Candidate 
    */
  onUpdateCandidate() {

    //load data skill
    this.candidate.skill = '';
    if (this.menu1) {
      this.candidate.skill += '1';
    }
    if (this.menu2) {
      this.candidate.skill += ',2';
    }
    if (this.menu3) {
      this.candidate.skill += ',3';
    }
    if (this.menu4) {
      this.candidate.skill += ',4';
    }
    if (this.menu5) {
      this.candidate.skill += ',5';
    }
    if (this.menu6) {
      this.candidate.skill += ',6';
    }
    if (this.menu7) {
      this.candidate.skill += ',7';
    }
    if (this.menu8) {
      this.candidate.skill += ',8';
    }
    if (this.menu9) {
      this.candidate.skill += ',9';
    }
    if (this.menu10) {
      this.candidate.skill += ',10';
    }

    // remove coret
    if (this.candidate.skill[0] == ',') {
      this.candidate.skill = this.candidate.skill.substr(1);
    }

    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return this.loadDataCandidate();
    } else {
      this.subscription.push(this.api.excuteAllByWhat(this.candidate, '22').subscribe(data => {
        if (data) {
          // load data grid
          this.loadDataCandidate();
  
          this.api.showSuccess('Cập nhật thành công');
        }
      }));
    }
  }

  /**
   * On Cancel
   */
  onCancel() {
    this.loadDataCandidate()

  }

}