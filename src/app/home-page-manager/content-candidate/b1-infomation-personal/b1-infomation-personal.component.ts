import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Candidate } from 'src/app/common/models/20candidate.models';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'app-b1-infomation-personal',
  templateUrl: './b1-infomation-personal.component.html',
  styleUrls: ['../content-candidate.component.scss']
})
export class B1InfomationPersonalComponent implements OnInit, OnDestroy {
  subscription: Subscription[] = [];

  //data module country
  countrys: any[] = [
    { value: '0', viewValue: 'Việt Nam' },
    { value: '1', viewValue: 'Hàn Quốc' },
    { value: '2', viewValue: 'Nhật Bản' },
    { value: '3', viewValue: 'Anh' },
    { value: '4', viewValue: 'Mỹ' },
    { value: '5', viewValue: 'Pháp' },
    { value: '6', viewValue: 'Nga' },
    { value: '7', viewValue: 'Trung Quốc' },
    { value: '8', viewValue: 'Thái' },
    { value: '9', viewValue: 'Campuchia' },
  ]

  //data models
  candidate: Candidate = {
    fullname: ''
  }

  //data combobox city
  citys: any[] = [];
  //data combobox provine
  provine: any[] = [];

  //data combobox sex
  sexs: any[] = [
    { value: '0', viewValue: 'Nam' },
    { value: '1', viewValue: 'Nữ' },
  ];

  // data provines
  provines: string = '';

  //data combobox marriages
  marriages: any[] = [
    { value: '0', viewValue: 'Độc thân' },
    { value: '1', viewValue: 'Đã kết hôn' },
  ];

  // validate
  maxDate = moment(new Date()).format('YYYY-MM-DD');
  form: FormGroup;
  datepicker = new FormControl(new Date());
  picker = Date;
  
  constructor(private api: ApiService, private formBuilder: FormBuilder){
    
    // add validate for controls````
    this.form = this.formBuilder.group({
      fullname: [null, [Validators.required, Validators.minLength(5),Validators.maxLength(30),  Validators.pattern("^[a-zA-Z]+(([',. -][a-zA-Z])?[a-zA-Z]*)*$")]],
      phone: [null, [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), Validators.maxLength(11)]],
      email: [null, [Validators.required, Validators.maxLength(100),Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}")]],
      born: [null, [Validators.required]],
      country: [null, [Validators.required]],
      city: [null, [Validators.required]],
      provine: [null, [Validators.required]],
      address: [null, [Validators.required, Validators.maxLength(100), Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]], 
      sex: [null, [Validators.required]],
      marriage: [null, [Validators.required]]
    });
  }
  ngOnInit(): void {
    this.loadDataCandidate();
  }

  /**
    * ngOnDestroy
    */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * load Data Candidae
   */
  loadDataCandidate() {
    const param = {
      'id': 11,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '24').subscribe(data => {
      if (data.length > 0) {
        this.candidate = data[0];
        this.loadDataCity();
        this.loadDataDistric();
      }
    }));
  } 
  /**
   * load Data City
   */
  loadDataCity() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '147').subscribe(data => {
      if (data.length > 0) {
        this.citys = data;
      }
    }));
  }
  /**
   * load Data Distric
   */
  loadDataDistric() {
    const param = {
      'idcity': this.candidate.city
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '148').subscribe(data => {
      if (data.length > 0) {
        this.provines = data;
      }
    }));
  }
  /**
   * on Update Candidate 
   */
  onUpdateCandidate() {
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      this.loadDataCandidate();
    } else {
      this.subscription.push(this.api.excuteAllByWhat(this.candidate, '22').subscribe(data => {
        if (data) {
          // load data grid
          this.loadDataCandidate();
  
          this.api.showSuccess('Cập nhật thành công');
        }
      }));
    }
    
  }
  /**
   * On Cancel
   */
  onCancel() {
    this.loadDataCandidate();
  }
}


