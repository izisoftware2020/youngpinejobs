import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Candidate } from 'src/app/common/models/20candidate.models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-b4-experient',
  templateUrl: './b4-experient.component.html',
  styleUrls: ['../content-candidate.component.scss']
})
export class B4ExperientComponent implements OnInit {

  subscription: Subscription[] = []; 

  //data models
  candidate: Candidate  = {

  }
  maxDate = moment(new Date()).format('YYYY-MM-DD');
  minDate = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');
  form: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    // add validate for controls````
    this.form = this.formBuilder.group({
      posisionold: [null, [Validators.required, Validators.maxLength(100), Validators.minLength(5), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]],
      companyold: [null, [Validators.required, Validators.maxLength(100), Validators.minLength(5),  Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]], 
      startworked: [null, [Validators.required]],
      endworked: [null, [Validators.required]],
      descriptionsoldjob:[ ],
    }); 
  }


  /**
    * ngOnDestroy
    */
   ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    this.loadDataCandidate();
  }

  /**
  * load Data Candidae
  */
 loadDataCandidate() {
  const param = {
    'id': 11,
  };
  this.subscription.push(this.api.excuteAllByWhat(param, '24').subscribe(data => {

    if (data.length > 0) {
      this.candidate = data[0]; 
      
    } 
  }));
}

 /**
   * on Update Candidate 
   */
  onUpdateCandidate() { 

    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return this.loadDataCandidate();
    } else {
      this.subscription.push(this.api.excuteAllByWhat(this.candidate, '22').subscribe(data => {
        if (data) {
          // load data grid
          this.loadDataCandidate();
  
          this.api.showSuccess('Cập nhật thành công');
        }
      }));
    }

  }

  /**
   * On Cancel
   */
  onCancel() {
    this.loadDataCandidate(); 
  }

}
