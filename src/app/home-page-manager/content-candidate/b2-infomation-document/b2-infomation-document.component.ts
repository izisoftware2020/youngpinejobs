import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Candidate } from 'src/app/common/models/20candidate.models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-b2-infomation-document',
  templateUrl: './b2-infomation-document.component.html',
  styleUrls: ['../content-candidate.component.scss']
})
export class B2InfomationDocumentComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];

  // data source for combobox academiclevel
  academiclevels: any[] = [
    { value: '0', viewValue: 'Lao động phố thông' },
    { value: '1', viewValue: 'Trung học' },
    { value: '2', viewValue: 'Trung cấp' },
    { value: '3', viewValue: 'Cao đẳng' },
    { value: '4', viewValue: 'Kỹ sư' },
    { value: '5', viewValue: 'Cử nhân' },
    { value: '6', viewValue: 'Thạc sĩ' },
  ]

  //data source for combobox typeOfWord
  typeOfWords: any[] = [
    { value: '0', viewValue: 'Tất Cả' },
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thòi gian cố định' },
    { value: '4', viewValue: 'Bán thòi gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  // data source for combobox expYears
  yearsofexperiences: any[] = [
    { value: '0', viewValue: 'Tất Cả' },
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  // data source for combobox levels
  levels: any[] = [
    { value: '0', viewValue: 'Thực tập sinh' },
    { value: '1', viewValue: 'Nhân viên' },
    { value: '2', viewValue: 'Trưởng nhóm' },
    { value: '3', viewValue: 'Trưởng phòng' },
    { value: '4', viewValue: 'Phó giám đốc' },
    { value: '5', viewValue: 'Giám đốc' }
  ];

  //data combobox city
  citys: any[] = [];

  //data combobox careers
  careers: any[] = [];

  //data models
  candidate: Candidate = { 
  }

  

  // menu position
  menu1: boolean = false;
  menu2: boolean = false;
  menu3: boolean = false;
  menu4: boolean = false;


  // validate
  form: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    // add validate for controls````
    this.form = this.formBuilder.group({
      positionapply: [null, [Validators.required,Validators.minLength(5), Validators.maxLength(100), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$") ]],
      academiclevel: [null, [Validators.required]],
      yearsofexperience: [null, [Validators.required]],
      workingplace: [null, [Validators.required]],
      career: [null, [Validators.required]],
      typeOfWord: [null, [Validators.required]],
      level: [null, [Validators.required]],
      salary: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(6),Validators.pattern('^[A-Za-z1-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      minsalary: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(6),Validators.pattern('^[A-Za-z1-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      maxsalary: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(6),Validators.pattern('^[A-Za-z1-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      descriptionstargetjob:[null,[Validators.maxLength(500)]]
    }); 
  }


  /**
    * ngOnDestroy
    */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * ng OnInit
   */
  ngOnInit(): void {
    this.loadDataCandidate();
    this.loadDataWorkingplace();
    this.loadDataCareer();
  }

  /**
  * load Data Candidae
  */
  loadDataCandidate() {
    const param = {
      'id': 11,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '24').subscribe(data => {

      if (data.length > 0) {
        this.candidate = data[0];

      }

      // load targetjob
      if (data[0].targetjob.search('1') >= 0) {
        this.menu1 = true;
      }
      if (data[0].targetjob.search('2') >= 0) {
        this.menu2 = true;
      }
      if (data[0].targetjob.search('3') >= 0) {
        this.menu3 = true;
      }
      if (data[0].targetjob.search('4') >= 0) {
        this.menu4 = true;
      }
    }));
  }

  /**
   * load Data City
   */
  loadDataWorkingplace() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '147').subscribe(data => {

      if (data.length > 0) {
        this.citys = data;
      }
    }));

  }

  /**
   * load Data City
   */
  loadDataCareer() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '130').subscribe(data => {

      if (data.length > 0) {
        this.careers = data;
      }
    }));

  }

  /**
   * on Update Candidate 
   */
  onUpdateCandidate() {

    //load data targetjob
    this.candidate.targetjob = '';
    if (this.menu1) {
      this.candidate.targetjob += '1';
    }
    if (this.menu2) {
      this.candidate.targetjob += ',2';
    }
    if (this.menu3) {
      this.candidate.targetjob += ',3';
    }
    if (this.menu4) {
      this.candidate.targetjob += ',4';
    }

    // remove coret
    if (this.candidate.targetjob[0] == ',') {
      this.candidate.targetjob = this.candidate.targetjob.substr(1);
    }

    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return this.loadDataCandidate();
    } else {
      this.subscription.push(this.api.excuteAllByWhat(this.candidate, '22').subscribe(data => {
        if (data) {
          // load data grid
          this.loadDataCandidate();
  
          this.api.showSuccess('Cập nhật thành công');
        }
      }));
    }
    
  }


  /**
   * On Cancel
   */
  onCancel() {
    this.loadDataCandidate();
  }

}
