import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MatDialog } from '@angular/material/dialog';
import { Applywork } from 'src/app/common/models/30applywork.models';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-c1-work-apply',
  templateUrl: './c1-work-apply.component.html',
  styleUrls: ['./c1-work-apply.component.scss']
})

export class C1WorkApplyComponent implements OnInit {
  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'position', 'companyname', 'salary', 'startdate', 'status', 'delete'];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);
  @ViewChild(MatPaginator,
    { static: false }) paginator: MatPaginator; @ViewChild(MatSort,
      { static: false }) sort: MatSort;

  // dialog: any;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  constructor(private api: ApiService, private dialog: MatDialog) { }
  applywork: Applywork;

  ngOnInit(): void {
    this.onLoadDataTable();
  }

  onLoadDataTable() {
    const param = {
      "idcandidate": 11
    };
    this.api.excuteAllByWhat(param, '149').subscribe(data => {
      if (data.length > 0) {
        this.applywork = data;
        // Số thứ tự    
        let stt = 1;
        data.forEach(item => {
          item.stt = stt++;
        });
        // set data for table
        this.dataSource = new MatTableDataSource(data);
      } else {
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  //onDeleteClick 
  onDeleteClick(){
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == ''){
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });
    const param = {"listId" : listId}
    // start Delete data of career
    if (listId !=''){
      this.api.excuteAllByWhat(param, '33').subscribe(data => {
        this.api.showSuccess('Xóa thành công');
      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để xóa ');
    }
    this.onLoadDataTable();
  }
}