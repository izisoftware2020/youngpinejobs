import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginCookie } from 'src/app/common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { MustMatch } from "src/app/common/validations/must-match.validator";
import { Candidate } from 'src/app/common/models/20candidate.models';


@Component({
  selector: 'app-d1-change-password',
  templateUrl: './d1-change-password.component.html',
  styleUrls: ['./d1-change-password.component.scss']
})
export class D1ChangePasswordComponent implements OnInit, OnDestroy{

  // validate
  form: FormGroup;

  //id company
  idCandidate: number;

  //old password
  oldpassword:string;

  //new password
  newpassword: string;

  //re password
  repassword: string;

  constructor(
    private login: LoginCookie,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      password: [null, [Validators.required, Validators.maxLength(50)]],
      newpassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
      repassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
    },
    { validator: MustMatch("newpassword", "repassword") }
    );

    // company value
    this.candidate = this.api.getCandidateValue;
    this.idCandidate =  this.api.getCandidateValue.id;
  }

  //value hide
  hide = true;
  hide1 = true;
  hide2 = true;

  subscription: Subscription[] = [];

  //model biding insert
  candidate: Candidate;

  ngOnInit() { 
    
  }
  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Change Password
   */
  onChangePassword() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }

    if (this.oldpassword == this.api.getCandidateValue.password) {
      this.candidate.password = this.newpassword;
      this.subscription.push(this.api.excuteAllByWhat(this.candidate, '22').subscribe(data => {
        if (data) {
          this.api.showSuccess("Thêm mới thành công!");
        }
      }));
    } else {
      this.api.showError("Mật khẩu cũ chưa đúng hoặc pass nhập lại chưa trùng");
    }
  }

  
}
