import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { D1ChangePasswordComponent } from './d1-change-password.component';

describe('D1ChangePasswordComponent', () => {
  let component: D1ChangePasswordComponent;
  let fixture: ComponentFixture<D1ChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ D1ChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(D1ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
