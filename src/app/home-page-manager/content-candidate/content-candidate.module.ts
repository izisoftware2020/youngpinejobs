import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentCandidateComponent } from './content-candidate.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ApiService } from '../../common/api-service/api.service';
import { D1ChangePasswordModule } from './d1-change-password/d1-change-password.module';
import { D1ChangePasswordComponent } from './d1-change-password/d1-change-password.component';


@NgModule({
    declarations: [ContentCandidateComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                // ng g module home-page/content/sellTicket --module content
                // ng g c home-page/content/sellTicket


                path: '',
                component: ContentCandidateComponent,
                children: [                    

                    {
                        path: 'b1-infomation-personal',
                        loadChildren: () =>
                            import('./b1-infomation-personal/b1-infomation-personal.module').then(
                                m => m.B1InfomationPersonalModule
                            )
                    },

                    {
                        path: 'b2-infomation-document',
                        loadChildren: () =>
                            import('./b2-infomation-document/b2-infomation-document.module').then(
                                m => m.B2InfomationDocumentModule
                            )
                    },

                    {
                        path: 'b3-education',
                        loadChildren: () =>
                            import('./b3-education/b3-education.module').then(
                                m => m.B3EducationModule
                            )
                    },

                    {
                        path: 'b4-experient',
                        loadChildren: () =>
                            import('./b4-experient/b4-experient.module').then(
                                m => m.B4ExperientModule
                            )
                    },

                    {
                        path: 'b5-skill-work',
                        loadChildren: () =>
                            import('./b5-skill-work/b5-skill-work.module').then(
                                m => m.B5SkillWorkModule
                            )
                    },

                    {
                        path: 'c1-work-apply',
                        loadChildren: () =>
                            import('./c1-work-apply/c1-work-apply.module').then(
                                m => m.C1WorkApplyModule
                            )
                    },

                    {
                        path: 'c2-work-save',
                        loadChildren: () =>
                            import('./c2-work-save/c2-work-save.module').then(
                                m => m.C2WorkSaveModule
                            )
                    },

                    {
                        path: 'd1-change-password',
                        loadChildren: () =>
                            import('./d1-change-password/d1-change-password.module').then(
                                m => m.D1ChangePasswordModule
                            )
                    },


                ]
            }
        ]),
    ],
    providers: [ApiService],
    entryComponents: []
})
export class ContentCandidateModule { }
