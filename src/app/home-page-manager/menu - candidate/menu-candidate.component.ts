import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoginCookie } from '../../common/core/login-cookie';

@Component({
    selector: 'app-menu-candidate',
    templateUrl: './menu-candidate.component.html',
    styleUrls: ['./menu-candidate.component.scss']
})
export class MenuCandidateComponent implements OnInit {

    menuFlag: boolean = false;
    menuMobileFlag: boolean = false;
    settingButton: boolean = false;
    searchFlag: boolean = false;
    isMobile: boolean = false; 


    constructor(private login: LoginCookie) { }

    ngOnInit() {
        this.isMobile = this.isMobileDevice();
    }

    isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    }; 
}
