import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { MatMenuModule } from '@angular/material/menu';
import { LoginCookie } from 'src/app/common/core/login-cookie';
import { MenuCandidateComponent } from './menu-candidate.component';

@NgModule({
    declarations: [
        MenuCandidateComponent,
    ],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '', component: MenuCandidateComponent, children: [
                    {
                        path: '',
                        loadChildren: () => import('../content-candidate/content-candidate.module').then(m => m.ContentCandidateModule)
                    },
                ],
            }
        ]),

        MatMenuModule,

    ],
    providers: [LoginCookie]
})
export class MenuCandidateModule { }
