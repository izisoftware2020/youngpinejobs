import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { C2ManagerWorkComponent, ManagerWorkDialog } from './c2-manager-work.component';


@NgModule({
  declarations: [C2ManagerWorkComponent, ManagerWorkDialog],
  imports: [
    TransferHttpCacheModule,
    CommonModule,

    RouterModule.forChild([
      {
        path: '',
        component: C2ManagerWorkComponent,
        children: []
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
  ],
  entryComponents: [ManagerWorkDialog]
})
export class C2ManagerWorkModule { }
