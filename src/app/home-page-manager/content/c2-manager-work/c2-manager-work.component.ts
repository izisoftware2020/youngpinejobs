import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Jobs } from 'src/app/common/models/10jobs.models';

export interface DialogData {
  // find item
}
 
export interface PeriodicElement {

}
@Component({
  selector: 'app-c2-manager-work',
  templateUrl: './c2-manager-work.component.html',
  styleUrls: ['./c2-manager-work.component.scss']
})
export class C2ManagerWorkComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'name', 'company', 'salary', 'work_at', 'view','numberview', 'edit'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls
  }
 // status 012
 status: any[]=[
   { value:'0', viewValue:'Tất Cả'},
   {value:'1',viewValue:'Chưa Duyệt'},
   {value:'2', viewValue:'Đã Duyệt'}
 ];
 /**
  * status id
  */
 statusId: string='0';
 nameCompanyId: string='';
 nameCompany: any[]=[];

   // validate
  form: FormGroup;
  /**
   * ngoninit
   */
  ngOnInit() {
    this.loadDataSystem();  
  }

//call jobs
  loadDataSystem(){
    const param={};
    this.api.excuteAllByWhat(param, '60').subscribe(data=>{
      if(data){
        let temp =[
          {
            id:'0',
            companyname:'Tất Cả'
          }
        ];
        data.forEach(item=>{
          temp.push(item);
        })
        //this.nameCompanyId = this.nameCompany[0].id;  
        this.nameCompany=temp;
        this.nameCompanyId = this.nameCompany[0].id
        this.filterCompany();
      }
    });
  }

  /**
   * filter company work
   */
  filterCompany(){
    const param = {
      'idcompany' :this.nameCompanyId
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '19').subscribe(data => {

      // declare property stt
      let stt = 0;
      if (data.length > 0) {
        data.forEach(item => {

          // assign stt for item
          item.stt = ++stt;
        });

        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));
  }

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }
/**
 * onDelete Click
 */
  onDeleteClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId += item.id
      } else {
        listId += ',' + item.id;
      }
    });
    
    const param = { 'listid': listId }

    // start delete
    if (listId !== '') {
      this.api.excuteAllByWhat(param, '13').subscribe(data => {
        // load data grid
         this.filterCompany();

        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });

        // show toast success
        this.api.showSuccess('Xóa thành công ');
      });
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

    /**
   * open Dialog Update
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerWorkDialog, {
      width: '100%',
      height: '600px',
      data: { type: 1, input: element }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }
}

@Component({
  selector: 'c2-manager-work-dialog',
  templateUrl: './c2-manager-work-dialog.component.html',
  styleUrls: ['./c2-manager-work.component.scss']
})
export class ManagerWorkDialog {
  subscription: Subscription[] = [];
  type:number;
 
  // init input value	
  input: Jobs = {
    idcompany:'',
    position: '',
    amountrecruitment: '',
    positionrecruitment: '',
    salary: '',
    asomission: '',
    addresswork: '',
    career: '',
    descriptionwork: '',
    benefit: '',
    exprience: '',
    academiclevel: '',
    sex: '',
    languagelevel: '',
    deadlineapply: '',
    requirementwork: '',
    requirementdocument: '',
    fullname: '',
    phone: '',
    email: '',
    positioncontact: '',
    point: '',
    startads: '',
    endads: '',
    numberview:'',
  };
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ManagerWorkDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    if(this.type=1){ 
    this.input = data['input']; 
    }
     // add validate for controls
     this.form = this.formBuilder.group({
      positioncontact:[null,[Validators.minLength(5),Validators.maxLength(255),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      position: [null, [Validators.required,Validators.minLength(5), Validators.maxLength(100),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      addresswork: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(500),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      salary: [null,[Validators.required, Validators.minLength(6), Validators.maxLength(105),Validators.pattern('^[A-Za-z1-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      career: [null,[Validators.required,Validators.minLength(5), Validators.maxLength(105),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      descriptionwork: [null,[Validators.maxLength(500)]],
      benefit: [null,[Validators.required, Validators.minLength(5),Validators.maxLength(500),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      requirementDocument: [null,[ Validators.minLength(5),Validators.maxLength(105),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      fullname: [null,[Validators.minLength(5),Validators.required, Validators.maxLength(105),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phone:[null,[Validators.required,Validators.minLength(10),Validators.pattern('[0-9]*'),Validators.maxLength(11)]],
      email: [null,[ Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}"),Validators.required, Validators.maxLength(100)]],
      numberview:{value:null, disabled:true},
    });
  }


  /**	
   * ng On Init	
   */
  ngOnInit(): void {

  }

  /**
   * ng On Destroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }
  /**
   * onNoClick
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

    /**	
 * on Ok click	
 */
onOkClick(): void {
  if (this.form.status != 'VALID') {
    this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
    return;
  }else{
  this.api.excuteAllByWhat(this.input, '' + Number(11 + this.type) + '').subscribe(data => {
  this.dialogRef.close(true);
  this.api.showSuccess("Xử Lý Thành Công ");
  });
}

}
  /**
   * onDeleteClick
   */
  onDeleteClick() {
    this.api.excuteAllByWhat(this.input, '13').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xóa Thành Công ");
    });
  }

}
