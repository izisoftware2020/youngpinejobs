import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';

export interface DialogData { 
  // find item
}

export interface PeriodicElement {
  stt: number;
  name: string;
  content: string;
  name_coaching: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
];


@Component({
  selector: 'app-e3-manager-comment',
  templateUrl: './e3-manager-comment.component.html',
  styleUrls: ['./e3-manager-comment.component.scss']
})
export class E3ManagerCommentComponent implements OnInit , OnDestroy {

  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'name', 'content', 'name_coaching'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  // end for table

  // flag insert
  insertFlag: boolean = false;


  // validate
  form: FormGroup;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService) {
    // add validate for controls

  }

  nameCoachingId: string='';
  nameCoaching: any[]=[];

  ngOnInit(): void {
this.loadDataCommentCoaching();
this.filterComment();
  }

  /**
   * loadDataCommentCoaching(){
   */
  loadDataCommentCoaching(){
    const param={};
    this.api.excuteAllByWhat(param, '90').subscribe(data=>{
      if(data){
        let temp = [{
          id: '0',
          fullname: 'Tất cả'
        }];
        data.forEach(item => {
          temp.push(item);
        })

        //this.nameCompanyId = this.nameCompany[0].id;  
        this.nameCoaching=temp;
        this.nameCoachingId = this.nameCoaching[0].id;
        this.filterComment();
      }
    });
  }
/**
 * on delete click comment coaching
 */
  onDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId += item.id
      } else {
        listId += ',' + item.id;
      }
    });
    const param = { 'listid': listId }
  
    // start delete
    if (listId !== '') {
      this.api.excuteAllByWhat(param, '123').subscribe(data => {
        // load data grid
      this.filterComment();
        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });
        // show toast success
        this.api.showSuccess('Xóa thành công ');
      });
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * Filter Coaching
   */
  filterComment(){
    const param = {
      'idcoaching' :this.nameCoachingId,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '127').subscribe(data => {

      // declare property stt
      let stt = 0;
      if (data.length > 0) {
        data.forEach(item => {

          // assign stt for item
          item.stt = ++stt;
        });

        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }
      this.selection = new SelectionModel<any>(true, []);
    }));
  }

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
     * on Insert Click
     */
    onInsertClick() {

      this.insertFlag = !this.insertFlag;

      setTimeout(() => {
          window.scroll({ left: 0, top: 1000, behavior: 'smooth' });
      }, 100);
  }
}
