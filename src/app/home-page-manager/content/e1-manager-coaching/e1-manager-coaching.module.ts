import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { E1ManagerCoachingComponent, ManagerCoachingDialog } from './e1-manager-coaching.component';
import { CKEditorModule } from 'ng2-ckeditor';  


@NgModule({
  declarations: [E1ManagerCoachingComponent, ManagerCoachingDialog],
  imports: [
    TransferHttpCacheModule,
    CommonModule,

    RouterModule.forChild([
      {
        path: '',
        component: E1ManagerCoachingComponent,
        children: []
      }
    ]),

    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    CKEditorModule
  ],
  entryComponents: [ManagerCoachingDialog]
})
export class E1ManagerCoachingModule { }
