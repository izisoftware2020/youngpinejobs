import { Component, OnInit, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer  } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators,FormControl,FormGroupDirective,NgForm } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import {ErrorStateMatcher} from '@angular/material/core';
import { Coaching } from 'src/app/common/models/90coaching.models';

export interface DialogData {
  // find item
}


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export interface PeriodicElement {
  stt: number;
  name: string;
  image: string;
  born: string;
  phone: string;
  address: string;
  account_order: number;
}

@Component({
  selector: 'app-e1-manager-coaching',
  templateUrl: './e1-manager-coaching.component.html',
  styleUrls: ['./e1-manager-coaching.component.scss']
})
export class E1ManagerCoachingComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'name', 'email','sex', 'born', 'phone', 'address', 'skill', 'about', 'account_order', 'edit'];

  dataSource = new MatTableDataSource<PeriodicElement>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  // data for fillter
  // data source for combobox gender
  genders : any[] = [
    {value: '2', viewValue: 'Tất cả'},
    {value: '0', viewValue: 'Nam'},
    {value: '1', viewValue: 'Nữ'},
  ]

  // biding models
  // gender
  genderId : string = '2';
  nameCoach = '';

  // validate
  formSearch: FormGroup;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService, private formBuilder: FormBuilder,
    public dialog: MatDialog) {
    // add validate for controls Search career 
    this.formSearch = this.formBuilder.group({
      coachName: [null, [Validators.maxLength(50)]],
      coachGender: [null]
    });
  }
  
  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.onFilterClick();
  }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }


  /**
   * Get Name Gender By Gender Number
   * @param genderNumber 
   */
  getNameGenderByGenderNumber(genderNumber){
    if(genderNumber=='0'){
      return 'Nam';
    }
    else {
      if (genderNumber=='1'){
        return 'Nữ';
      }
    }
  }

  
   /**
    * shorten String
    * @param str 
    */
   shortenString(str){
      return (str.length <= 30 ) ? str : str.substr(0,30) + '...';
  }

  /**
   * onFillterClick
   */
  onFilterClick(){
    const param={"fullname": this.nameCoach, "sex": this.genderId};
     this.subscription.push(this.api.excuteAllByWhat(param, '97').subscribe(data => {
      if (data.length > 0) {
        let stt = 1;
        data.forEach(element => {
          element.stt = stt++;
        });
        // set data for table
        this.dataSource = new MatTableDataSource(data);

      }
      else {
        // set data for table
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
     }));
   }

   /**
   * onDeleteClick
   */
  onDeleteClick(){
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == ''){
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = {"id" : listId}

    // start Delete data of career
    if (listId !=''){
      this.api.excuteAllByWhat(param, '93').subscribe(data => {
        // load data grid
        this.onFilterClick();

        this.api.showSuccess('Xóa thành công');

      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để xóa ');
    }
  }

  /**	
   * open Dialog Insert
   */
  openDialogInsert() {
    const dialogRef = this.dialog.open(ManagerCoachingDialog, {
      width: '100%',
      height: '600px',
      data: { type: 0, id: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onFilterClick();
    });
  }

  /**
   * openDialogUpdate
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerCoachingDialog, {
      width: '100%',
      height: '600px',
      // data: element
      data: { type: 1, input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onFilterClick();
    });
  }
}

@Component({
  selector: 'e1-manager-coaching-dialog',
  templateUrl: './e1-manager-coaching-dialog.component.html',
  styleUrls: ['./e1-manager-coaching.component.scss']
})
export class ManagerCoachingDialog implements OnInit, OnDestroy {
  subscription: Subscription[] = [];
  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  // init input value	
  input: Coaching = {
    fullname: '',
    born: '',
    sex: '',
    phone: '',
    address: '',
    about: '',
    skill: '',
    email: '',
    // set default avatar
    avatar: 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
    numberView:'',
  };

  // validate
  form: FormGroup;
  
  //declare min max length
  minCoachName = 5;
  maxCoachName = 50;
  minCoachPhone = 10;
  maxCoachPhone = 11;
  minCoachAddress = 5;
  maxCoachAddress = 255;
  maxCoachAboutAndSKill = 500;
  maxCoachEmail = 50;
  minCoachEmail = 5;

  // data for fillter   
  // data source for combobox gender
  genders : any[] = [
    {value: '0', viewValue: 'Nam'},
    {value: '1', viewValue: 'Nữ'}
  ]
  
  constructor(
    public dialogRef: MatDialogRef<ManagerCoachingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private api: ApiService,
    private uploadService: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.type = data.type;

    // if UPDATE	
    if (this.type == 1) {
      this.input = data.input;
    }

    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });

    // add validate for controls
    this.form = this.formBuilder.group({
      coachName: [null, [Validators.required, Validators.maxLength(this.maxCoachName), Validators.minLength(this.minCoachName),Validators.pattern('^[A-Za-z0-9][A-Za-z0-9 -]*$')]],
      coachBorn: [{value:null,onkeypress:false},[Validators.required]],
      coachGender:[null,[Validators.required]],
      coachPhone: [null,[Validators.required,Validators.minLength(this.minCoachPhone), Validators.maxLength(this.maxCoachPhone), Validators.pattern('[0-9]*')]],
      coachAddress: [null, [Validators.required,Validators.minLength(this.minCoachAddress), Validators.maxLength(this.maxCoachAddress),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      coachAbout: [null,[Validators.maxLength(this.maxCoachAboutAndSKill),Validators.required]],
      coachSkill: [null,[Validators.maxLength(this.maxCoachAboutAndSKill),Validators.required]],
      coachAvatar: [null],
      coachEmail: [null,[Validators.required, Validators.maxLength(this.maxCoachEmail), Validators.minLength(this.minCoachEmail),Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]]
    });
  }
  
  //set max date of birth of coach 
  todayDOB = new Date();
  dd = this.todayDOB.getDate();
  mm = this.todayDOB.getMonth()+1; //January is 0!
  yyyy = this.todayDOB.getFullYear();
  maxDate= this.yyyy+'-'+ (this.mm > 9 ? this.mm : ('0' + this.mm))+'-'+ (this.dd > 9 ? this.dd : ('0' + this.dd));
  //set min date of birth of coach 
  minDate= "1900-01-01";
  /**	
   * ng On Init	
   */
  ngOnInit(): void {

  }

  /**	
   * ng No Click
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * ng On Destroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**	
   * on Ok click	
   */
  onOkClick(): void {
    // Data processing time -> date (yyyy-mm-dd)
    this.input.born = this.api.formatDate(new Date(this.input.born));
  
    // return if error
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else{     
      this.api.excuteAllByWhat(this.input, '' + Number(91 + this.type) + '').subscribe(data => {
        this.dialogRef.close(true);
        this.api.showSuccess("Xử Lý Thành Công ");
      });
    }
  }

  /**	
   * on Delete Click	
   */
  onDeleteClick() {
    this.api.excuteAllByWhat(this.input, '93').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xóa Thành Công ");
    });
  }

  /**
   * ----------------------UPLOAD AVATAR COACH-------------------------
   */
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];


  fileToUpload: File = null;

  /**
   * uploadFiles
   */
  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  // logo url
  imageUrl:string="";
  isUpload=false;

   // get time save image 
   today = new Date(); date = this.today.getFullYear()+'-'+  ((this.today.getMonth()+1) > 9 ? (this.today.getMonth()+1) : ('0'+(this.today.getMonth()+1)))+'-'+(this.today.getDate() > 9 ? this.today.getDate() : ('0'+this.today.getDate())); 
   time = (this.today.getHours() > 9 ? this.today.getHours() : ('0'+this.today.getHours())) + '-' + (this.today.getMinutes() > 9 ? this.today.getMinutes() : ('0'+this.today.getMinutes())) + '-' + (this.today.getSeconds() > 9 ? this.today.getSeconds() : ('0'+this.today.getSeconds())); 
   dateTime = this.date+'-'+this.time;

  /**
   * uploadFile 
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('date1', this.dateTime );
    this.imageUrl = "http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/" + this.dateTime  + file.data.name;
    this.input.avatar=this.imageUrl;
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
        }
      });
  }

  /**
   * onOpenUploadFileClick
   */
  onOpenUploadFileClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  // url avatar
  public imagePath;
  public message: string;
  imgPreviewURL: any;
  
  /**
   * previewImage
   * @param files 
   */
  previewImage(files) {
    this.isUpload=true;
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgPreviewURL = reader.result;    
    }
  }
}
