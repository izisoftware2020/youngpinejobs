import { Component, OnInit, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';


export interface DialogData {
}
export interface PeriodicElement {
}
const ELEMENT_DATA: PeriodicElement[] = [
];

@Component({
  selector: 'app-b3-manager-advertisement-work',
  templateUrl: './b3-manager-advertisement-work.component.html',
  styleUrls: ['./b3-manager-advertisement-work.component.scss']
})
export class B3ManagerAdvertisementWorkComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'namecompany', 'name_work', 'time_end', 'point', 'advertisement', 'edit'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];
  

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  selection = new SelectionModel<any>(true, []);
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    private uploadService: ApiService,
    public dialog: MatDialog) {
    // add validate for controls
  }
  nameEmployer: any[] = [];
  jobId: string = '';
  statues: any[] = [
    { value: '2', viewValue: 'Tất Cả' },
    { value: '1', viewValue: 'Đã Duyệt' },
    { value: '0', viewValue: 'Chưa Duyệt' } 
  ]
  statusId: string = '2';

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.loadDataAdvertising();
  }

  /**
   * duyet
   */
  onDuyetClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { "listid": listId, "statusid": '1' }

    // start Delete data of career
    if (listId != '') {
      this.api.excuteAllByWhat(param, '192').subscribe(data => {
        // load data grid
        this.getFilterCompany();

        this.api.showSuccess('Duyệt thành công ');
      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để duyệt ');
    }
  }

  /**
   * load data systym
   */
  loadDataAdvertising() {
    const param = {};
    this.api.excuteAllByWhat(param, '60').subscribe(data => {
      
      if (data) {
        let temp=[
          {
            id:'0',
            companyname:'Tất Cả'
          }
        ];
        data.forEach(item=>{
          temp.push(item)
        })
        //this.nameCompanyId = this.nameCompany[0].id;  
        this.nameEmployer = temp;
        this.jobId = this.nameEmployer[0].id
        this.getFilterCompany();
      }
    });
  }
   
  /**
   * get Data Company
   */
  getFilterCompany() {
    const param = {
      "jobId": this.jobId,
      "status": this.statusId
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '18').subscribe(data => {

      // declare property stt
      let stt = 0;
      if (data.length > 0) {
        data.forEach(item => {

          // assign stt for item
          item.stt = ++stt;
        });
        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));
  }

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * get tin con han het han
   */
  getInfo(number) {
    if (number < '0') {
      return "Tin Hết Hạn"
    } else {
      return number + " ngày";
    }
  }

  /**
   * openDialogUpdate
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerAdvertisementWorkDialog, {
      width: '100%',
      height: '600px',
      data: { type: 1, input: element }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getFilterCompany();
    });
  }
}
 
@Component({
  selector: 'b3-manager-advertisement-work-dialog.component',
  templateUrl: './b3-manager-advertisement-work-dialog.component.html',
  styleUrls: ['./b3-manager-advertisement-work.component.scss']
})
export class ManagerAdvertisementWorkDialog {
  subscription: Subscription[] = [];
  observable: Observable<any>;
  observer: Observer<any>;
  imgURL: any;
  fileToUpload: File = null; 
  type: number;
  minDate;
  
  input: any = {
    idcompany: '',
    position: '',
    amountrecruitment: '',
    positionrecruitment: '',
    salary: '',
    asomission: '',
    addresswork: '',
    career: '',
    descriptionwork: '',
    benefit: '',
    exprience: '',
    academiclevel: '',
    sex: '',
    languagelevel: '',
    deadlineapply: '',
    requirementwork: '',
    requirementdocument: '',
    fullname: '',
    phone: '',
    email: '',
    positioncontact: '',
    point: '',
    endads: '',
    status: '',
    avatar: '',
  }
  //upload file images
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];
  // validate
  form: FormGroup;
  daystart = this.input.startads;
  isUpload=false;

  covertStatus(number) {
      return number == 0 ? false : true;
  }
   
  constructor(
    public dialogRef: MatDialogRef<ManagerAdvertisementWorkDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private uploadService: ApiService,
  ) {
    if (this.type = 1) {
      this.input = data['input'];
      this.input.status=this.covertStatus(data['input']['status']);
    } 
    this.minDate = this.input.startads;
      
    // add validate for controls
    this.form = this.formBuilder.group({
      companyname: { value: null, disabled: true },
      addresswork: { value: null, disabled: true },
      descriptionwork: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      point: [null, [Validators.required,Validators.maxLength(5),Validators.pattern('[0-9]*')]],
      endads: [null,[Validators.required]],
      phone: [null,[Validators.required,Validators.maxLength(11),Validators.pattern('[0-9]*'),Validators.minLength(10)]],
      companylogo:[null],
      status:[null],
      startads:{value:null, disabled:true}
    });
    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }
  // data source for combobox times
  times: any[] = [
    { value: '0', viewValue: '1 Tuần' },
    { value: '1', viewValue: '2 Tuần' },
    { value: '2', viewValue: '3 Tuần' }
  ];


  // url upload avatar
  public imagePath;
  public message: string;
  private uploadFiles() {
      this.fileUpload.nativeElement.value = '';
      this.files.forEach(file => {
      this.uploadFile(file);
    });
  }
  startDate = new Date();
  //khoi tao myurrl
  myUrl:string="";
  // get time save image 
  today = new Date(); 
  date = this.today.getFullYear()+'-'+  ((this.today.getMonth()+1) > 9 ? (this.today.getMonth()+1) : ('0'+(this.today.getMonth()+1)))+'-'+(this.today.getDate() > 9 ? this.today.getDate() : ('0'+this.today.getDate())); 
  time = (this.today.getHours() > 9 ? this.today.getHours() : ('0'+this.today.getHours())) + '-' + (this.today.getMinutes() > 9 ? this.today.getMinutes() : ('0'+this.today.getMinutes())) + '-' + (this.today.getSeconds() > 9 ? this.today.getSeconds() : ('0'+this.today.getSeconds())); 
  dateTime = this.date+'-'+this.time;
  /**
   * upload file
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('date1', this.dateTime );
    this.myUrl="http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/"+this.dateTime + file.data.name;
    this.input.avatar = this.myUrl;
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
        }
      });
  }
 
  /** 
   * on No Click
   */
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  /**
   * preview img
   */
  preview(files) {
    this.isUpload =true;
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

  /**	
 * on Ok click	
 */
  onClickJob(): void {
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    }else{
    this.input.endads = this.api.formatDate(new Date(this.input.endads));
    //delete khoangtrang
    this.input.descriptionwork = this.input.descriptionwork.trim();
    this.input.status = this.input.status ? '1':'0';
    this.api.excuteAllByWhat(this.input, '' + Number(202 + this.type) + '').subscribe(data => {
    this.dialogRef.close(true);
    this.api.showSuccess("Xử Lý Thành Công ");
    });
  }
 
  }
 /**
   * on lick insert img
   */
  onClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  onduyetclick(status){
   if(status==1){
     return true;
   }if(status==0){
     return false;
   }
  }

}



