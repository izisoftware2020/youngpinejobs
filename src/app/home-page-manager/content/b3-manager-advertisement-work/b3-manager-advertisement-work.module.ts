import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { MatPaginatorModule } from '@angular/material/paginator'; 
import { MatSelectModule } from '@angular/material/select'; 
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table'; 
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { MatDialogModule } from '@angular/material/dialog';
import { ManagerAdvertisementWorkDialog, B3ManagerAdvertisementWorkComponent } from './b3-manager-advertisement-work.component';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import { MatIconModule} from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule,  } from '@angular/material/toolbar';


@NgModule({
  declarations: [B3ManagerAdvertisementWorkComponent, ManagerAdvertisementWorkDialog],
  imports: [
    TransferHttpCacheModule,
    CommonModule,

    RouterModule.forChild([
      {
        path: '',
        component: B3ManagerAdvertisementWorkComponent,
        children: []
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatCardModule,
    MatCheckboxModule,   
    MatPaginatorModule,  
    MatSelectModule,    
    MatSortModule,
    MatTableModule,   
    MatInputModule,
    MatButtonModule, 
    MatDialogModule,
    MatNativeDateModule,
    MatRippleModule,
    HttpClientModule,
    MatIconModule,
    MatProgressBarModule,
    MatToolbarModule


    

  ],
  entryComponents: [ManagerAdvertisementWorkDialog]
})
export class B3ManagerAdvertisementWorkModule { }
