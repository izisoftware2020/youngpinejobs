import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Company } from 'src/app/common/models/60company.models';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
export interface DialogData {

}

@Component({
  selector: 'app-c1-manager-company',
  templateUrl: './c1-manager-company.component.html',
  styleUrls: ['./c1-manager-company.component.scss']
})
export class C1ManagerCompanyComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'companyname', 'address', 'phone', 'personalscale', 'website', 'point', 'edit'];

  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * filterCompany
   * @param event 
   */
  filterCompany(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  // end for table;
  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls

  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.getDataCompany();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * get Data Company
   */
  getDataCompany() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '60').subscribe(data => {

      // declare property stt
      let stt = 0;
      if (data.length > 0) {
        data.forEach(item => {

          // assign stt for item
          item.stt = ++stt;
        });

        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));
  }

  /**
 * on Delete Click
 */
  onDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId += item.id
      } else {
        listId += ',' + item.id;
      }
    });

    // listId: 1,2,3
    const param = { 'listid': listId }

    // start delete
    if (listId !== '') {
      this.api.excuteAllByWhat(param, '63').subscribe(data => {
        // load data grid
        this.getDataCompany();
 
        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });

        // show toast success
        this.api.showSuccess('Xóa thành công ');
      });
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * open Dialog Update
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerEmployerDialog, {
      width: '100%',
      height: '600px',
      data: { input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getDataCompany();
      }
    });
  }

}

@Component({
  selector: 'c1-manager-company-dialog',
  templateUrl: './c1-manager-company-dialog.component.html',
  styleUrls: ['./c1-manager-company.component.scss']
})
export class ManagerEmployerDialog implements OnInit, OnDestroy {
  subscription: Subscription[] = [];

  observable: Observable<any>;
  observer: Observer<any>;
  form: FormGroup;

  // init input value	
  input: Company;

  constructor(
    public dialogRef: MatDialogRef<ManagerEmployerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder,
  ) {

    this.input = data.input;
        // add validate for controls
        this.form = this.formBuilder.group({
          companyname: { value: null, disabled: true },
        });
    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**	
   * ng On Init	
   */
  ngOnInit() {

  }

  /**
   * ng On Destroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**	
   * on Ok click	
   */
  onOkClick(): void {

    this.api.excuteAllByWhat(this.input, '' + Number(62) + '').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xử Lý Thành Công ");
    });
  }

  /**	
   * on Delete Click
   */
  onDeleteClick() {
    // listId: 1,2,3
    const param = { 'listid': this.input.id };

    this.api.excuteAllByWhat(param, '63').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xóa Thành Công ");
    });
  }
}