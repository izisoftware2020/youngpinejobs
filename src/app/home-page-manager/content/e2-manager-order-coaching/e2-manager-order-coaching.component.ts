import { Component, OnInit, OnDestroy, Inject,ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog'; 
export interface DialogData {  
  // find item
}

export interface PeriodicElement {
  stt: number;
  name: string;
  package: string;
  price: string;
  name_coaching: string;
  order_at: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
];

@Component({
  selector: 'app-e2-manager-order-coaching',
  templateUrl: './e2-manager-order-coaching.component.html',
  styleUrls: ['./e2-manager-order-coaching.component.scss']
})
export class E2ManagerOrderCoachingComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'name', 'package', 'price', 'name_coaching' , 'order_at', 'edit'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) { 
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls
  }
  nameCoachingId: string='';
  nameCoaching: any[]=[];
  // gender 012
  sex: any[]=[
    { value:'0', viewValue:'Nam'},
    {value:'1',viewValue:'Nữ'},
    {value:'2', viewValue:'Khác'}
  ];
 
  /**
   * gender id
   */
  sexId: string='0';

/**
 * ngoninit
 */
  ngOnInit(): void {
    this.loadDataCoaching();  
  }

/**
 * loadDataCoaching
 */
  loadDataCoaching(){
    const param={};
    this.api.excuteAllByWhat(param, '90').subscribe(data=>{
      if(data){
        let temp=[
          {
            id:'0',
            fullname:'Tất Cả'
          }
        ];
        data.forEach(item=>{
          temp.push(item);
        })
        //this.nameCompanyId = this.nameCompany[0].id;  
        this.nameCoaching=temp;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.nameCoachingId = this.nameCoaching[0].id;
        this.filterCoaching(); 
      }
    }); 
  }

  /**
   * Filter Coaching
   */
  filterCoaching(){
    const param = {
      'idcoaching' :this.nameCoachingId,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '117').subscribe(data => {
      
      // declare property stt
      let stt = 0;
      if (data.length > 0) {
        data.forEach(item => {

          // assign stt for item
          item.stt = ++stt;
        });

        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));
  }

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

   /**
   * Get Name Gender By Gender Number
   * @param genderNumber 
   */
  getNameGenderByGenderNumber(genderNumber){
    if(genderNumber=='0'){
      return 'Nam';
    }
    else {
      if (genderNumber=='1'){
        return 'Nữ';
      }
    }
  }

   /**
 * on Delete Click
 */
onDelClick() {
  // get listId selection example: listId='1,2,6'
  let listId = '';
  this.selection.selected.forEach(item => {
    if (listId == '') {
      listId += item.id
    } else {
      listId += ',' + item.id;
    }
  });
  const param = { 'listid': listId }

  // start delete
  if (listId !== '') {
    this.api.excuteAllByWhat(param, '113').subscribe(data => {
      // load data grid
    this.filterCoaching();
      //scroll top
      window.scroll({ left: 0, top: 0, behavior: 'smooth' });
      // show toast success
      this.api.showSuccess('Xóa thành công ');
    });
  } else {
    // show toast warning
    this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
  }
  this.selection = new SelectionModel<any>(true, []);
}
  /**
   * open Dialog Add
   */
  openDialogAdd() {
    const dialogRef = this.dialog.open(ManagerOrderCoachingDialog, {
      width: '100%',
      height: '600px',
      data: { type: 0, id: 0 }
    });
  }

  /**
   * openDialogUpdate
   * @param element 
   */
  openUpdateOrder(element): void {
    const dialogRef = this.dialog.open(ManagerOrderCoachingDialog, {
      width: '100%',
      height: '600px',
      data: { type: 1, input: element }
    });
  }
  }

@Component({
  selector: 'e2-manager-order-coaching-dialog',
  templateUrl: './e2-manager-order-coaching-dialog.component.html',
  styleUrls: ['./e2-manager-order-coaching.component.scss']

})
export class ManagerOrderCoachingDialog {
  subscription: Subscription[] = [];
  type: number;
  title = 'datepicker-input-check';

    // max min Date is the start of today.
  minDate = new Date(); 



   // init input value	
   input: any = {
    idcoaching:'',
    idcandidate:'',
    idpackage:'',
    startdate:'',
    startconnect:'',
    statuspayment:'',
    amount:'',
    fullname:'',
    born:'',
    sex:'',
    phone:'',
    address:'',
    price:'',
    namecdd:'',
    phonecdd:'',
    borncdd:'',
    sexcdd:'',
    name:'',
    addresscdd:'',
    email:'',

  };
  // validate
  form: FormGroup;
   // data source for combobox gender
   genders : any[] = [
    {value: '0', viewValue: 'Nam'},
    {value: '1', viewValue: 'Nữ'}
  ]
  constructor(
    public dialogRef: MatDialogRef<ManagerOrderCoachingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    if(this.type=1){ 
      this.input = data['input']; 
      }  

    // add validate for controls
    this.form = this.formBuilder.group({
      nameCdd: [null, [Validators.required, Validators.maxLength(100),Validators.minLength(5),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      addressCdd: [null, [Validators.required, Validators.maxLength(150),Validators.minLength(5),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phoneCdd: [null, [Validators.required, Validators.maxLength(11),Validators.minLength(10),Validators.pattern('[0-9]*')]],
      bornCdd: [null, [Validators.maxLength(50)]],
      sexCdd: [null],
      name: { value: null, disabled: true },
      fullName:[null, [Validators.required, Validators.maxLength(100),Validators.minLength(5),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      born:[null, [Validators.maxLength(50)]],
      address:[null, [Validators.required, Validators.maxLength(150),Validators.minLength(5),,Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phone:[null, [Validators.required, Validators.maxLength(11),Validators.minLength(10),Validators.pattern('[0-9]*')]],
      sex:[null],
      email:[null,[Validators.required,Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,4}'),Validators.maxLength(50),Validators.minLength(5)]],
      price:{ value: null, disabled: true }
    });
  }
   
/**
 * onNoClick
 */
  onNoClick(): void {
    this.dialogRef.close();
    
  }

 /**	
   * on Ok click	
   */
  onClickAdd(): void {
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    }else{
    //update birthday
    this.input.borncdd = this.api.formatDate(new Date(this.input.borncdd));
    this.input.born = this.api.formatDate(new Date(this.input.born));
    //xoa khoang trang
    this.input.namecdd = this.input.namecdd.trim();
    this.input.fullname = this.input.fullname.trim();
    this.input.addresscdd = this.input.addresscdd.trim();
    this.input.address = this.input.address.trim();
    this.input.name = this.input.name.trim();
    //update 
    this.api.excuteAllByWhat(this.input, '' + Number(111 + this.type) + '').subscribe(data => {
    this.dialogRef.close(true);
    this.api.showSuccess("Xử Lý Thành Công ");
    });
  }
  }
  /**	
   * on Delete Click	
   */
  onDelClick() {
    this.api.excuteAllByWhat(this.input, '113').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xóa Thành Công ");
    });
  }
  
}