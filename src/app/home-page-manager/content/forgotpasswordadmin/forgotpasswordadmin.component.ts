import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginCookie } from 'src/app/common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { MustMatch } from "src/app/common/validations/must-match.validator";
import { Staff } from 'src/app/common/models/140staff.models';


@Component({
  selector: 'app-forgotpasswordadmin',
  templateUrl: './forgotpasswordadmin.component.html',
  styleUrls: ['./forgotpasswordadmin.component.scss']
})
export class ForgotpasswordadminComponent implements OnInit, OnDestroy{

  // validate
  form: FormGroup;

  //id staff
  idStaff: number;

  //old password
  oldpassword:string;

  //new password
  newpassword: string;

  //re password
  repassword: string;

  constructor(
    private login: LoginCookie,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      password: [null, [Validators.required, Validators.maxLength(50)]],
      newpassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
      repassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
    },
    { validator: MustMatch("newpassword", "repassword") }
    );

    // staff value
    this.staff = this.api.getStaffValue;
    this.idStaff =  this.api.getStaffValue.id;
  }

  //value hide
  hide = true;
  hide1 = true;
  hide2 = true;

  subscription: Subscription[] = [];

  //model biding insert
  staff: Staff;

  ngOnInit() { 
    
  }
  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Change Password
   */
  onChangePassword() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }

    if (this.oldpassword == this.api.getStaffValue.password) {
      this.staff.password = this.newpassword;
      this.subscription.push(this.api.excuteAllByWhat(this.staff, '142').subscribe(data => {
        if (data) {
          
          this.api.showSuccess("Thêm mới thành công!");
        }
      }));
    } else {
      this.api.showError("Mật khẩu cũ chưa đúng hoặc pass nhập lại chưa trùng");
    }
  }

  
}

