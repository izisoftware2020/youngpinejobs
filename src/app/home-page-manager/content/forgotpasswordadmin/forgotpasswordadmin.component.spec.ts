import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordadminComponent } from './forgotpasswordadmin.component';

describe('ForgotpasswordadminComponent', () => {
  let component: ForgotpasswordadminComponent;
  let fixture: ComponentFixture<ForgotpasswordadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
