import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ApiService } from '../../common/api-service/api.service';

@NgModule({
    declarations: [ContentComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                // ng g module home-page/content/sellTicket --module content
                // ng g c home-page/content/sellTicket

                path: '',
                component: ContentComponent,
                children: [
                    {
                        path: 'a1-dashboard',
                        loadChildren: () =>
                            import('./a1-dashboard/a1-dashboard.module').then(m => m.A1DashboardModule)
                    },
                    {
                        path: 'b1-manager-advertisement-banner',
                        loadChildren: () =>
                            import('./b1-manager-advertisement-banner/b1-manager-advertisement-banner.module').then(
                                m => m.B1ManagerAdvertisementBannerModule
                            )
                    },

                    {
                        path: 'b2-manager-advertisement-company',
                        loadChildren: () =>
                            import('./b2-manager-advertisement-company/b2-manager-advertisement-company.module').then(
                                m => m.B2ManagerAdvertisementCompanyModule
                            )
                    },

                    {
                        path: 'b3-manager-advertisement-work',
                        loadChildren: () =>
                            import('./b3-manager-advertisement-work/b3-manager-advertisement-work.module').then(
                                m => m.B3ManagerAdvertisementWorkModule
                            )
                    },

                    {
                        path: 'c1-manager-company',
                        loadChildren: () =>
                            import('./c1-manager-company/c1-manager-company.module').then(
                                m => m.C1ManagerCompanyModule
                            )
                    },

                    {
                        path: 'c2-manager-work',
                        loadChildren: () =>
                            import('./c2-manager-work/c2-manager-work.module').then(
                                m => m.C2ManagerWorkModule
                            )
                    },

                    {
                        path: 'c3-manager-occupation',
                        loadChildren: () =>
                            import('./c3-manager-occupation/c3-manager-occupation.module').then(
                                m => m.C3ManagerOccupationModule
                            )
                    },

                    {
                        path: 'c4-manager-message',
                        loadChildren: () =>
                            import('./c4-manager-message/c4-manager-message.module').then(
                                m => m.C4ManagerMessageModule
                            )
                    },

                    {
                        path: 'd1-manager-candidate',
                        loadChildren: () =>
                            import('./d1-manager-candidate/d1-manager-candidate.module').then(
                                m => m.D1ManagerCandidateModule
                            )
                    },

                    {
                        path: 'e1-manager-coaching',
                        loadChildren: () =>
                            import('./e1-manager-coaching/e1-manager-coaching.module').then(
                                m => m.E1ManagerCoachingModule
                            )
                    },

                    {
                        path: 'e2-manager-order-coaching',
                        loadChildren: () =>
                            import('./e2-manager-order-coaching/e2-manager-order-coaching.module').then(
                                m => m.E2ManagerOrderCoachingModule
                            )
                    },

                    {
                        path: 'e3-manager-comment',
                        loadChildren: () =>
                            import('./e3-manager-comment/e3-manager-comment.module').then(
                                m => m.E3ManagerCommentModule
                            )
                    },

                    {
                        path: 'forgotpasswordadmin',
                        loadChildren: () =>
                            import('./forgotpasswordadmin/forgotpasswordadmin.module').then(
                                m => m.ForgotpasswordadminModule
                            )
                    },

                ]
            }
        ]),

    ],
    providers: [ApiService],
    entryComponents: []
})
export class ContentModule { }
