import { Component, OnInit, OnDestroy, Inject, ViewChild,ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer  } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Company } from 'src/app/common/models/60company.models';

export interface DialogData {

}

export interface PeriodicElement {
  stt: number;
  namecompany: string;
  address: string;
  title: string;
  package: string;
  time_end: string;
  point: string;
}

@Component({
  selector: 'app-b2-manager-advertisement-company',
  templateUrl: './b2-manager-advertisement-company.component.html',
  styleUrls: ['./b2-manager-advertisement-company.component.scss']
})
export class B2ManagerAdvertisementCompanyComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'namecompany', 'address','title', 'start_ads', 'time_end', 'point', 'status', 'edit'];

  dataSource = new MatTableDataSource<PeriodicElement>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  // data for fillter
  // data source for combobox status
  statuses : any[] = [
    {value: '2', viewValue: 'Tất cả'},
    {value: '1', viewValue: 'Đã duyệt'},
    {value: '0', viewValue: 'Chưa duyệt'},
  ]

  // data source for combobox name company
  companies : any=[];

  // biding models
  // status
  statusId : string = '2';

  // company
  companyId : string = '';

  // validate
  form: FormGroup;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls

  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    // load data combobox company
    this.onLoadDataCompany();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * onLoadDataCompany
   */
  onLoadDataCompany(){
    const param={};
     this.subscription.push(this.api.excuteAllByWhat(param, '60').subscribe(data => {

       if (data.length>0) {
        let temp = [{
          id : '0',
          companyname : 'Tất Cả'
        }];
        data.forEach(item => {
          temp.push(item);
        })
        this.companies = temp;

         // set first select company combobox
        this.companyId = this.companies[0].id;   

        this.onFilterClick();
      }
     }));
  }

  /**
   * onFillterClick
   */
  onFilterClick(){
    const param={"companyId" : this.companyId, "status" : this.statusId};

     this.subscription.push(this.api.excuteAllByWhat(param, '65').subscribe(data => {
      if (data.length > 0) {
        let stt = 1;
        data.forEach(element => {
          element.stt = stt++;
        });
        
        // set data for table
        this.dataSource = new MatTableDataSource(data);
      }
      else {
        // set data for table
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
     }));
    
   }

   /**
   * convertStatusAdsCompany
   * @param statusNumber 
   */
  convertStatusAdsCompany(statusNumber) {
    return (statusNumber == '1') ? 'Duyệt' : 'Chưa duyệt';
  }

   /**
    * checkTimeAdsLeft
    * @param time 
    */
   checkTimeAdsLeft(time){
     return time < 0 ? 'Hết hạn' : time +' ngày';
   }

   /**
    * On Accept Click
    */
   onAcceptClick(){
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == ''){
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = {"listid" : listId, "status" : '1'}

    // start Delete data of career
    if (listId !=''){
      this.api.excuteAllByWhat(param, '201').subscribe(data => {
        // load data grid
        this.onFilterClick();

        this.api.showSuccess('Duyệt thành công');

      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để duyệt ');
    }
  }

  /**
   * openDialogUpdate
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerAdvertisementCompanyDialog, {
      width: '100%',
      height: '600px',
      // data: element
      data: {input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onFilterClick();
    });
  }

}


@Component({
  selector: 'b2-manager-advertisement-company-dialog.component',
  templateUrl: './b2-manager-advertisement-company-dialog.component.html',
  styleUrls: ['./b2-manager-advertisement-company.component.scss']
})
export class ManagerAdvertisementCompanyDialog implements OnInit, OnDestroy  {
  subscription: Subscription[] = [];
  observable: Observable<any>;
  observer: Observer<any>;


  // init input value	
  input: Company = {}; 

  // declare min end date ads value
  minDate;

  // validate
  form= new FormGroup({  });

  constructor(
    public dialogRef: MatDialogRef<ManagerAdvertisementCompanyDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private uploadService: ApiService,
  ) {
    this.input = data.input;
    this.minDate = this.input.startads;
    this.checked=this.convertStatusToChecked(this.input.status);
    
    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
    // add validate for controls
    this.form = this.formBuilder.group({
      companyName: {value:null,disabled: true},
      companyAddress: {value:null,disabled: true},
      endAds: [{value:null,onkeypress:false},[Validators.required]],
      companyPhone: {value:null,disabled: true},
      companyLogo: [null],
      updateStatus: {checked:null}
    });
  }

  /**
   * on ok click
   */
  onOkClick(): void {
    // Data processing time -> date (yyyy-mm-dd)
    this.input.endads = this.api.formatDate(new Date(this.input.endads));

    // convertCheckedToStatus
    this.input.status=this.convertCheckedToStatus(this.checked);

    // return if error
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else{   
      this.api.excuteAllByWhat(this.input, '62').subscribe(data => {
        this.dialogRef.close(true);
        this.api.showSuccess("Xử Lý Thành Công ");
        this.imageUrl="";
      });
    }
  }
 
  /**
   * on No Click
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**	
   * ng On Init	
   */
  ngOnInit(): void {

  }

  /**
   * ng On Destroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * ----------------------UPLOAD LOGO-------------------------
   */
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];


  fileToUpload: File = null;

  /**
   * uploadFiles
   */
  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  // logo url
  imageUrl:string="";
  isUpload=false;

  /**
   * convert Status To Checked
   * @param status 
   */
  convertStatusToChecked(status){
    return status==1 ? true : false
  }

  checked;
  /**
   * convertCheckedToStatus
   * @param checked 
   */
  convertCheckedToStatus(checked){
    return checked==true ? '1' : '0';
  }
  
  // get time save image 
  today = new Date(); date = this.today.getFullYear()+'-'+  ((this.today.getMonth()+1) > 9 ? (this.today.getMonth()+1) : ('0'+(this.today.getMonth()+1)))+'-'+(this.today.getDate() > 9 ? this.today.getDate() : ('0'+this.today.getDate())); 
  time = (this.today.getHours() > 9 ? this.today.getHours() : ('0'+this.today.getHours())) + '-' + (this.today.getMinutes() > 9 ? this.today.getMinutes() : ('0'+this.today.getMinutes())) + '-' + (this.today.getSeconds() > 9 ? this.today.getSeconds() : ('0'+this.today.getSeconds())); 
  dateTime = this.date+'-'+this.time;

  /**
   * uploadFile
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('date1', this.dateTime );
    this.imageUrl = "http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/" + this.dateTime  + file.data.name;
    this.input.logo=this.imageUrl;
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        
        if (typeof (event) === 'object') {
        }
      });
  }

  /**
   * onOpenUploadFileClick
   */
  onOpenUploadFileClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  // url avatar
  public imagePath;
  public message: string;
  imgPreviewURL: any;

  /**
   * previewImage
   * @param files 
   */
  previewImage(files) {
    this.isUpload=true;
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgPreviewURL = reader.result;    
    }
  }
}
