import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
  selector: 'app-a1-dashboard',
  templateUrl: './a1-dashboard.component.html',
  styleUrls: ['./a1-dashboard.component.scss']
})
export class A1DashboardComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];

  // binding data
  totalJob: any;

  totalCompany: any;

  totalApply: any;

  totalCandidate: any;

  totalCoaching: any;

  constructor(private api: ApiService, ) { }


  /**
   * ngOnInit
   */
  ngOnInit(): void {
    // count Jobs
    this.countJobs();

    this.countCompany();

    this.countApply();

    this.countCandidate();

    this.countCoaching();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * count jobs
   */
  countJobs() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '15').subscribe(data => {
      if (data.length > 0) {
        // assign totalJob
        this.totalJob = data[0]['COUNT(1)'];
      } else {
        this.totalJob = 0;
      }
    }));
  }

  /**
   * count Company
   */
  countCompany() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '66').subscribe(data => {
      if (data.length > 0) {
        // assign totalCompany
        this.totalCompany = data[0]['COUNT(1)'];
      } else {
        this.totalCompany = 0;
      }
    }));
  }

  /**
   * count Apply
   */
  countApply() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '35').subscribe(data => {
      if (data.length > 0) {
        // assign totalApply
        this.totalApply= data[0]['COUNT(1)'];
      } else {
        this.totalApply = 0;
      }
    }));
  }

  /**
   * count candidate
   */
  countCandidate() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '26').subscribe(data => {
      if (data.length > 0) {
        // assign totalJob
        this.totalCandidate = data[0]['COUNT(1)'];
      } else {
        this.totalCandidate = 0;
      }
    }));
  }

  /**
   * count Coaching
   */
  countCoaching() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '96').subscribe(data => {
      if (data.length > 0) {
        // assign totalJob
        this.totalCoaching = data[0]['COUNT(1)'];
      } else {
        this.totalCoaching = 0;
      }
    }));
  }
  

}
