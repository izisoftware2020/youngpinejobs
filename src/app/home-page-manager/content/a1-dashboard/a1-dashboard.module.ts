import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common'; 
import { RouterModule } from '@angular/router'; 
import { A1DashboardComponent } from './a1-dashboard.component';


@NgModule({
  declarations: [A1DashboardComponent],
  imports: [
    TransferHttpCacheModule,  
    CommonModule,  
    RouterModule.forChild([ 
      {
        path: '', component: A1DashboardComponent, children: [ 
        ],
      } 
    ]),
  ]
})
export class A1DashboardModule { }
