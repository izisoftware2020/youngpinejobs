
import { Component, OnInit, ViewChild, OnDestroy, Inject } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Candidate } from 'src/app/common/models/20candidate.models';
import { City } from 'src/app/common/models/150city.model';

export interface DialogData {
  // find item
}

@Component({
  selector: 'app-d1-manager-candidate',
  templateUrl: './d1-manager-candidate.component.html',
  styleUrls: ['./d1-manager-candidate.component.scss']
})
export class D1ManagerCandidateComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'fullname', 'phone', 'address', 'numberview', 'edit'];

  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // filter Name Candidate
  filterNameCandidate(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  // end for table

  // data for filter
  sex: any[] = [
    { value: '1', viewValue: 'Nữ' },
    { value: '0', viewValue: 'Nam' },
    { value: '2', viewValue: 'Tất Cả' },
  ];

  typeOfWord: any[] = [
    { value: '0', viewValue: 'Tất Cả' },
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thời gian cố định' },
    { value: '4', viewValue: 'Bán thời gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  expYears: any[] = [
    { value: '0', viewValue: 'Tất Cả' },
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  city: any[] = [];

  // binding sexId init filter
  cityId: string = '48';

  // binding expYearId init filter
  typeOfWordId: string = '0'

  // binding expYearId init filter
  expYearId: string = '0'

  // binding sexId init filter
  sexId: string = '2';

  // occupation
  occupation: any = [];

  // set occupationId init
  occupationId: string = '';

  //name
  name:string='';

  // candidate
  candidate: any[] = [];

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls

  }

  ngOnInit(): void {
    //  Load Data Occupation
    this.onLoadDataOccupation();

    // Load Data City
    this.onLoadDataCity();
  };

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * on Load Data Occupation
   */
  onLoadDataOccupation() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '130').subscribe(data => {
      if (data.length > 0) {
        let temp = [
          {
            id: "0",
            name: "Tất cả"
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });
        // set data for occupation	
        this.occupation = temp;

        this.occupationId = this.occupation[0].id;

        // on Filter Click
        this.onFilterClick();
      }
    }))
  }

  /**
 * on Load Data City
 */
  onLoadDataCity() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '147').subscribe(data => {
      if (data.length > 0) {

        let temp = [
          {
            id: "0",
            name: "Tất cả"
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });
        // set data for city	
        this.city = temp;

        // on Filter Click
        this.onFilterClick();
      }
    }))
  }

  /**
   * on Load Data Occupation
   */
  onFilterClick() {
    const param = {
      'sexid': this.sexId,
      'occupationid': this.occupationId,
      'typeofworkid': this.typeOfWordId,
      'cityid': this.cityId,
      'expyearid': this.expYearId,
      'name': this.name,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '27').subscribe(data => {
      if (data.length > 0) {
        //set data for table
        this.dataSource = new MatTableDataSource(data);

        // binding stt
        let stt = 0;
        data.forEach(item => {
          item.stt = ++stt;
        });
      } else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }))
  }

  /**
 * on Delete Click
 */
  onDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId += item.id
      } else {
        listId += ',' + item.id;
      }
    });

    // listId: 1,2,3
    const param = { 'listid': listId }

    // start delete
    if (listId !== '') {
      this.api.excuteAllByWhat(param, '23').subscribe(data => {
        // load data grid
        this.onFilterClick();
 
        //scroll top
        window.scroll({ left: 0, top: 0, behavior: 'smooth' });

        // show toast success
        this.api.showSuccess('Xóa thành công ');
      });
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }


  /**
   * openDialogUpdate
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerCandidateDialog, {
      width: '100%',
      height: '600px',
      data: { input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onFilterClick();
      }
    });
  }
}

@Component({
  selector: 'd1-manager-candidate-dialog',
  templateUrl: './d1-manager-candidate-dialog.component.html',
  styleUrls: ['./d1-manager-candidate.component.scss']
})
export class ManagerCandidateDialog implements OnInit, OnDestroy {
  subscription: Subscription[] = [];

  observable: Observable<any>;
  observer: Observer<any>;

  // data for update
  sex: any[] = [
    { value: '0', viewValue: 'Nam' },
    { value: '1', viewValue: 'Nữ' },
    { value: '2', viewValue: 'Khác' },
  ];

  typeOfWord: any[] = [
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thòi gian cố định' },
    { value: '4', viewValue: 'Bán thòi gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  expYears: any[] = [
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  city: City;

  listCity: any[] = [];

  // init input value	
  input: Candidate;

  constructor(
    public dialogRef: MatDialogRef<ManagerCandidateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private api: ApiService
  ) {

    // assign input empty = input has element
    this.input = data['input'];

    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**	
   * ng On Init	
   */
  ngOnInit() {
    // Load Data City
    this.onLoadDataCity();
  }

  /**
   * ng On Destroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
* on Load Data City
*/
  onLoadDataCity() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '147').subscribe(data => {
      if (data) {

        let temp = [
          {
            id: "0",
            name: "Tất cả"
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });
        // set data for city	
        this.listCity = temp;
      }
    }))
  }

  /**	
   * on Delete Click	
   */
  onDeleteClick() {
    const param = { 'listid': this.input.id };
    this.subscription.push(this.api.excuteAllByWhat(param, '23').subscribe(data => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xóa Thành Công ");
    }));
  }
}