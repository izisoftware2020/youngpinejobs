import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators,FormControl,FormGroupDirective,NgForm } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Career } from 'src/app/common/models/130career.models';
import {ErrorStateMatcher} from '@angular/material/core';

export interface DialogData {
  // find item
}

export interface PeriodicElement {
  stt: number;
  occupation: string;
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-c3-manager-occupation',
  templateUrl: './c3-manager-occupation.component.html',
  styleUrls: ['./c3-manager-occupation.component.scss']
})
export class C3ManagerOccupationComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'occupation', 'edit'];

  dataSource = new MatTableDataSource<any>();

  matcher = new MyErrorStateMatcher();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * occupation filter
   * @param event 
   */
  occupationFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  // end for table

  // model biling insert
  career : Career;

  // flag insert
  insertFlag: boolean = false;

  // validate
  form: FormGroup;
  formSearch: FormGroup;

  // declare length name
  minLengthName: number = 5;
  maxLengthName: number =150;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    
    // add validate for controls
    this.form = this.formBuilder.group({
      careerName: [null, [Validators.required, Validators.minLength(this.minLengthName), Validators.maxLength(this.maxLengthName),Validators.pattern('^[A-Za-z0-9][A-Za-z0-9 -]*$')]],
    });

    // add validate for controls Search career 
    this.formSearch = this.formBuilder.group({
      careerName: [null, [Validators.maxLength(this.maxLengthName)]],
    });
  }

  ngOnInit(): void {
    // load data
    this.onFilterClick();

  }

  /**
     * ngOnDestroy
     */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * on Insert Click
   */
  onInsertClick() {
    // init value for model
    this.career = {
      name: ''
    }

    this.insertFlag = !this.insertFlag;

    setTimeout(() => {
        window.scroll({ left: 0, top: 10000, behavior: 'smooth' });
    }, 100);
  }

  /**
   * onDeleteClick
   */
  onDeleteClick(){
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == ''){
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = {"id" : listId}

    // start Delete data of career
    if (listId !=''){
      this.api.excuteAllByWhat(param, '133').subscribe(data => {
        // load data grid
        this.onFilterClick();

        // scroll top
        window.scroll({left: 0, top: 0, behavior: 'smooth'});

        this.api.showSuccess('Xóa thành công');

      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để xóa ');
    }
  }

  /**
   * onUpdateClick
   * @param row
   */
  onUpdateClick(row){
    this.career = row;
    this.insertFlag = true;

    setTimeout(() => {
        window.scroll({ left: 0, top: 10000, behavior: 'smooth' });
    }, 200);
  }

  /**
   * onSubmitClick
   */
  onSubmitClick(){

    // return if error
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      this.onFilterClick();
      return;
    } else {

      // check update or insert 
      if(this.career.id == undefined){    
          this.api.excuteAllByWhat(this.career,'131').subscribe(data => {
            if(data){
              this.api.showSuccess('Thêm mới thành công ');
                  
              // clear data
              this.onCancelClick();
      
              // scroll top
              window.scroll({left:0, top: 0, behavior: 'smooth'});
            }    
          });     
      } else {    
          this.api.excuteAllByWhat(this.career,'132').subscribe(data => {
            if(data){
              this.api.showSuccess('Cập nhật thành công ');
      
              // clear data
              this.onCancelClick();
      
              // scroll top
              window.scroll({left:0, top: 0, behavior: 'smooth'});
            }
          });     
      }
    }    
  }

  /**
   * onCancelClick
   */
  onCancelClick(){
    this.career = {
      name: ''
    };
    this.onFilterClick();
    
    this.insertFlag = false;
  }

  /**
   * onFilterClick
   */
  
  onFilterClick(){
   const param={};
    this.subscription.push(this.api.excuteAllByWhat(param, '130').subscribe(data => {
      if (data.length > 0) {
        let stt = 1;
        data.forEach(element => {
          element.stt = stt++;
        });
        // set data for table
        this.dataSource = new MatTableDataSource(data);
      }
      else {
        // set data for table
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));
  }
}
