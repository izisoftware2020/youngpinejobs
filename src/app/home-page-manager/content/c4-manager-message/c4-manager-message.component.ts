import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

export interface DialogData {
  // find item
}

export interface PeriodicElement {
  stt: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  type_work: string;
  rank_want: string;
  work_at: string;
  view: number;
  updated_at: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { stt: 1, name: 'daicatu', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Đà Nãng', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 2, name: 'daicaphuong', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Quảng Ngãi', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 3, name: 'daicabinh', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Gia Lai', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 4, name: 'daicamt4', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Quảng Nam', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
];

@Component({
  selector: 'app-c4-manager-message',
  templateUrl: './c4-manager-message.component.html',
  styleUrls: ['./c4-manager-message.component.scss']
})
export class C4ManagerMessageComponent implements OnInit {

  subscription: Subscription[] = [];

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getAllCompanies();
  }

  // data source for combo box companies
  companies: any[] = [];

  // companyId
  companyId: string = '';

  // listChatCandidate
  listChatCandidate: any[] = [];

  /**
   * loadDataSystemSite
   */
  getAllCompanies() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '60').subscribe(data => {
      if (data.length > 0) {
        this.companies = data;

        // set first select websites comboBox
        this.companyId = this.companies[0].id;
      }
    }));
  }

  /**
   * onFilterClick
   */
  onFilterClick() {
    const param = { "companyid": this.companyId };

    this.subscription.push(this.api.excuteAllByWhat(param, '57').subscribe(data => {
      if (data.length > 0) {
        
        // set data for listChatCandidate
        this.listChatCandidate = data;
      }
      else {
        // set data for table	
        this.listChatCandidate = [];
      }
    }));

  }

}
