import { Component, OnInit, OnDestroy, Inject, ViewChild,ElementRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription, Observable, Observer } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Company } from 'src/app/common/models/60company.models';

export interface DialogData {

}

@Component({
  selector: 'app-b1-manager-advertisement-banner',
  templateUrl: './b1-manager-advertisement-banner.component.html',
  styleUrls: ['./b1-manager-advertisement-banner.component.scss']
})
export class B1ManagerAdvertisementBannerComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'namecompany', 'address', 'time_end', 'point', 'advertisement', 'edit'];

  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }
  // end table

  // data for filter
  // data source for comboBox status
  statuses: any[] = [
    { value: '2', viewValue: 'Tất cả' },
    { value: '1', viewValue: 'Đã duyệt' },
    { value: '0', viewValue: 'Chưa duyệt' },
  ];

  // data source for comboBox name company
  companies: any = [];

  // biding models
  // status
  statusId: string = '2';

  // company
  companyId: string = '';

  // validate
  form: FormGroup;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    public dialog: MatDialog) {
    // add validate for controls

  }

  ngOnInit(): void {
    // load data comboBox company
    this.onLoadDataCompany();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * onLoadDataCompany
   */
  onLoadDataCompany() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '60').subscribe(data => {

      if (data.length > 0) {
        let temp = [{
          id: '0',
          companyname: 'Tất cả'
        }];

        data.forEach(item => {
          temp.push(item);
        })

        // assign companies to data all
        this.companies = temp;

        // set first select company comboBox
        this.companyId = this.companies[0].id;

        this.onFilterClick();
      }
    }));
  }

  /**
   * onFilterClick
   */
  onFilterClick() {
    const param = { "companyid": this.companyId, "statusid": this.statusId };

    this.subscription.push(this.api.excuteAllByWhat(param, '601').subscribe(data => {
      if (data.length > 0) {
        let stt = 1;
        data.forEach(element => {
          element.stt = stt++;
        });

        // set data for table
        this.dataSource = new MatTableDataSource(data);
      }
      else {
        // set data for table	
        this.dataSource = new MatTableDataSource([]);
      }

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.selection = new SelectionModel<any>(true, []);
    }));

  }

  /**
   * On Accept Click
   */
  onAcceptClick() {
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { "listid": listId, "statusid": '1' }

    // start Delete data of career
    if (listId != '') {
      this.api.excuteAllByWhat(param, '602').subscribe(data => {
        // load data grid
        this.onFilterClick();

        this.api.showSuccess('Duyệt thành công ');
      });
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để duyệt ');
    }
  }

  /**
   * convertStatusBanner
   * @param statusNumber 
   */
  convertStatusBanner(statusNumber) {
    return (statusNumber == '1') ? 'Duyệt' : 'Chưa duyệt';
  }

  /**
    * checkTimeBannerAdsLeft
    * @param time 
    */
   checkTimeBannerAdsLeft(time){
    return time < 0 ? 'Hết hạn' : time +' ngày';
  }

  /**
   * open Dialog Update
   * @param element 
   */
  openDialogUpdate(element): void {
    const dialogRef = this.dialog.open(ManagerAdvertisementBannerDialog, {
      width: '100%',
      height: '600px',
      data: { input: element }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // load data grid
        this.onFilterClick();
      }
    });
  }
}

@Component({
  selector: 'b1-manager-advertisement-banner-dialog.component',
  templateUrl: './b1-manager-advertisement-banner-dialog.component.html',
  styleUrls: ['./b1-manager-advertisement-banner.component.scss']
})
export class ManagerAdvertisementBannerDialog {
  subscription: Subscription[] = [];
  observable: Observable<any>;
  observer: Observer<any>;

  // init input value	
  input: Company;

  // validate
  form: FormGroup;

  minDate;

  constructor(
    public dialogRef: MatDialogRef<ManagerAdvertisementBannerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private api: ApiService,
    private uploadService: ApiService,
    private formBuilder: FormBuilder
  ) {
      this.input = data['input'];
      this.minDate = this.input.startadsbanner;
      this.checked=this.convertStatusBannerToChecked(this.input.statusbanner);
 
    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });

    // add validate for controls
    this.form = this.formBuilder.group({
      companyName: {value:null,disabled: true},
      companyAddress: {value:null,disabled: true},
      endAds: [null,[Validators.required]],
      companyPhone: {value:null,disabled: true},
      bannerCompany: [null]
    });

  };

  /**
   * on ok click
   */
  onOkClick(): void {
    // Data processing time -> date (yyyy-mm-dd)
    this.input.endadsbanner = this.api.formatDate(new Date(this.input.endadsbanner));

    // convertCheckedToStatus
    this.input.statusbanner=this.convertCheckedToStatusBanner(this.checked);

    // return if error
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else{   
      this.subscription.push(this.api.excuteAllByWhat(this.input, '62').subscribe(data => {
        this.dialogRef.close(true);
        this.api.showSuccess("Cập Nhật Thành Công ");
      }));
    }
  }

  /**
   * on No Click
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * convert Status To Checked
   * @param status 
   */
  convertStatusBannerToChecked(status){
    return status==1 ? true : false
  }

  checked;
  /**
   * convertCheckedToStatus
   * @param checked 
   */
  convertCheckedToStatusBanner(checked){
    return checked==true ? '1' : '0';
  }

  /**
   * ----------------------UPLOAD BANNER-------------------------
   */
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];


  fileToUpload: File = null;

  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  // logo url
  imageUrl:string="";
  isUpload=false;

   // get time save image 
   today = new Date(); date = this.today.getFullYear()+'-'+  ((this.today.getMonth()+1) > 9 ? (this.today.getMonth()+1) : ('0'+(this.today.getMonth()+1)))+'-'+(this.today.getDate() > 9 ? this.today.getDate() : ('0'+this.today.getDate())); 
   time = (this.today.getHours() > 9 ? this.today.getHours() : ('0'+this.today.getHours())) + '-' + (this.today.getMinutes() > 9 ? this.today.getMinutes() : ('0'+this.today.getMinutes())) + '-' + (this.today.getSeconds() > 9 ? this.today.getSeconds() : ('0'+this.today.getSeconds())); 
   dateTime = this.date+'-'+this.time;

  /**
   * uploadFile
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('date1', this.dateTime );
    this.imageUrl = "http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/" + this.dateTime  + file.data.name;
    this.input.banner=this.imageUrl;
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
        }
      });
  }

  /**
   * onOpenUploadFileClick
   */
  onOpenUploadFileClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  // url avatar
  public imagePath;
  public message: string;
  imgPreviewURL: any;
  
  /**
   * previewImage
   * @param files 
   */
  previewImage(files) {
    this.isUpload=true;
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgPreviewURL = reader.result;    
    }
  }

}
