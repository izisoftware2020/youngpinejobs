import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentCompany } from './content-company.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ApiService } from '../../common/api-service/api.service';  
import { B5AddPointModule } from './b5-add-point/b5-add-point.module';
import { E1ChangePasswordModule } from './e1-change-password/e1-change-password.module';
import { E1ChangePasswordComponent } from './e1-change-password/e1-change-password.component'; 



@NgModule({
    declarations: [ContentCompany],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                // ng g module home-page/content/sellTicket --module content
                // ng g c home-page/content/sellTicket
                path: '',
                component: ContentCompany,
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('./home/home.module').then(m => m.HomeModule)
                    },

                    {
                        path: 'a1-information-company',
                        loadChildren: () =>
                            import('./a1-information-company/a1-information-company.module').then(
                                m => m.A1InformationCompanyModule
                            )
                    },

                    {
                        path: 'b1-list-recruitment',
                        loadChildren: () =>
                            import('./b1-list-recruitment/b1-list-recruitment.module').then(
                                m => m.B1ListRecruitmentModule
                            )
                    },

                    {
                        path: 'b2-add-recruitment',
                        loadChildren: () =>
                            import('./b2-add-recruitment/b2-add-recruitment.module').then(
                                m => m.B2AddRecruitmentModule
                            )
                    },

                    {
                        path: 'b3-ads-company',
                        loadChildren: () =>
                            import('./b3-ads-company/b3-ads-company.module').then(
                                m => m.B3AdsCompanyModule
                            )
                    },

                    {
                        path: 'b4-ads-banner',
                        loadChildren: () =>
                            import('./b4-ads-banner/b4-ads-banner.module').then(
                                m => m.B4AdsBannerModule
                            )
                    },

                    {
                        path: 'c1-saved-profile',
                        loadChildren: () =>
                            import('./c1-saved-profile/c1-saved-profile.module').then(
                                m => m.C1SavedProfileModule
                            )
                    },

                    {
                        path: 'c2-viewed-profile',
                        loadChildren: () =>
                            import('./c2-viewed-profile/c2-viewed-profile.module').then(
                                m => m.C2ViewedProfileModule
                            )
                    },

                    {
                        path: 'c3-employer-profile',
                        loadChildren: () =>
                            import('./c3-employer-profile/c3-employer-profile.module').then(
                                m => m.C3EmployerProfileModule
                            )
                    },

                    {
                        path: 'd1-message',
                        loadChildren: () =>
                            import('./d1-message/d1-message.module').then(
                                m => m.D1MessageModule
                            )
                    },
                    {
                        path: 'e1-change-password',
                        loadChildren: () =>
                            import('./e1-change-password/e1-change-password.module').then(
                                m => m.E1ChangePasswordModule
                            )
                    },

                ]
            }
        ]), 
        B5AddPointModule
    ],
    
    providers: [ApiService],
    entryComponents: []
})
export class ContentCompanyModule { }
