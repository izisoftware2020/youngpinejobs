import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
import { Subscription } from "rxjs";
import {
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { Jobs } from "src/app/common/models/10jobs.models";
import { MatTableDataSource } from "@angular/material/table";
import { ApiService } from "src/app/common/api-service/api.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-b2-add-recruitment",
  templateUrl: "./b2-add-recruitment.component.html",
  styleUrls: ["./b2-add-recruitment.component.scss"],
})
export class B2AddRecruitmentComponent implements OnInit {
  // data source for combobox experience
  expYears: any[] = [
    
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  // data source for combobox level
  levels: any[] = [
    { value: "0", viewValue: "Tốt nghiệp phổ thông" },
    { value: "1", viewValue: "Trung cấp" },
    { value: "2", viewValue: "Cao đẳng" },
    { value: "3", viewValue: "Đại học" },
    { value: "4", viewValue: "Trên đại học" },
  ];

  // data source for combobox city
  employs: any[] = [
    { value: "1", viewValue: "Thực tập sinh" },
    { value: "2", viewValue: "Nhân viên" },
    { value: "3", viewValue: "Trưởng nhóm" },
    { value: "4", viewValue: "Trưởng phòng" },
    { value: "5", viewValue: "Phó giám đốc" },
    { value: "6", viewValue: "Giám đốc" },
  ];

  // data source for combobox type
  typeOfWord: any[] = [
    
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thòi gian cố định' },
    { value: '4', viewValue: 'Bán thòi gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  // data source for combobox city
  citys: any[] = [];
  // data source for combobox sex
  sexs: any[] = [
    { value: "0", viewValue: "Nam" },
    { value: "1", viewValue: "Nữ" },
    { value: "2", viewValue: "Không yêu cầu" },
  ];

  // data source for combobox language
  languages: any[] = [
    { value: "0", viewValue: "Tiếng Anh" },
    { value: "1", viewValue: "Tiếng Pháp" },
    { value: "2", viewValue: "Tiếng Hàn" },
  ];

  //data source for combobox branchs
  careers: any[] = [];

  // data source for combobox times
  times: any[] = [
    { value: "0", viewValue: "1 Tuần" },
    { value: "1", viewValue: "2 Tuần" },
    { value: "2", viewValue: "3 Tuần" },
  ];

  //Rule
  // validate
  form: FormGroup;
  form1: FormGroup;
  form2: FormGroup;
  constructor(
    public dialog: MatDialog,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    // add validate for controls
    this.form = this.formBuilder.group({
      position: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]],
      amountrecruitment: [null,[Validators.required,Validators.maxLength(10), Validators.min(1),Validators.pattern("[0-9]*")]],
      positionrecruitment: [null, [Validators.required]],
      typeofwork: [null, [Validators.required]],
      salary: [null,[Validators.required,Validators.maxLength(30),Validators.minLength(6),Validators.pattern("[0-9]*"),Validators.min(100000),]],
      asomission: [null,[Validators.maxLength(30),Validators.pattern("[0-9]*")]],
      addresswork: [null, [Validators.required, Validators.maxLength(100)]],
      career: [null,[Validators.required,Validators.maxLength(100),Validators.pattern("[0-9]*")]],
      descriptionwork: [null, [Validators.required,Validators.maxLength(500),Validators.minLength(5)]],
      benefit: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5)]],
    });
    this.form1 = this.formBuilder.group({
      exprience: [null, [Validators.required, Validators.maxLength(200)]],
      academiclevel: [null, [Validators.required, Validators.maxLength(100)]],
      sex: [null, [Validators.required]],
      languagelevel: [null, [Validators.required]],
      deadlineapply: [null, [Validators.required]],
      requirementwork: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5)]],
      requirementdocument: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5)]],
      
    });
    this.form2 = this.formBuilder.group({
      fullname: [null, [Validators.required, Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phone: [null,[Validators.required,Validators.maxLength(11),Validators.pattern("[0-9]*"),Validators.minLength(10)]],
      email: [null,[Validators.required, Validators.maxLength(50), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
      positioncontact: [null, [Validators.maxLength(100)]]
    });
  }
  minDate = new Date();

  //flag insert
  insertFlag: boolean = false;

  //model building insert
  job_config: Jobs;

  // constructor(private api: ApiService) {}

  ngOnInit() {
    this.job_config = {
      idcompany: "1",
      position: "",
      //hoa hồng
      amountrecruitment: "",

      //vị trí tuyển dụng
      positionrecruitment: "",
      //lương
      salary: "",
      asomission: "",
      //địa chỉ
      addresswork: "",
      //ngành nghề
      career: "",
      //mô tả
      descriptionwork: "",
      //quyền lợi
      benefit: "",
      //kinh nghiệm
      exprience: "",
      //trình độ học vấn
      academiclevel: "",
      //giới tính
      sex: "",
      //trình độ ngoại ngữ
      languagelevel: "",
      //hạn nộp hồ sơ
      deadlineapply: "",
      //yêu cầu công việc
      requirementwork: "",
      //Yêu cầu hồ sơ
      requirementdocument: "",
      fullname: "",
      phone: "",
      email: "",
      //địa chỉ
      positioncontact: "",
      //kinh nghiệm

      //loại hình công việc
      typeofwork: "",

      status: "0",

      postdate: new Date(),
    };
    this.loadDataSystemCitys();
    this.loadDataSystemCareer();
  }

  /**
   * on Update Click
   */
  onUpdateClick() {}

  /**
   * on Clear Click
   */
  onClearClick() {}

  /**
   * on Submit Click
   */
  onSubmitClick() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    if (this.form1.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    if (this.form2.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    this.job_config.deadlineapply = this.api.formatDate(
      new Date(this.job_config.deadlineapply)
    );
    this.job_config.postdate = this.api.formatDate(
      new Date(this.job_config.postdate)
    );
    this.job_config.descriptionwork = this.job_config.descriptionwork.trim();
    this.job_config.benefit = this.job_config.benefit.trim();
    this.job_config.requirementdocument = this.job_config.requirementdocument.trim();
    this.job_config.requirementwork = this.job_config.requirementwork.trim();
    this.api.excuteAllByWhat(this.job_config, "11").subscribe((data) => {
      if (data) {
        this.api.showSuccess("thêm mới thành công!");
      }
      
    });
    this.form.reset();
    this.form1.reset();
    this.form2.reset();
  }

  /**
   * load Data System Citys
   */
  loadDataSystemCitys() {
    const param = {};
    this.api.excuteAllByWhat(param, "147").subscribe((data) => {
      if (data) {
        this.citys = data;
      }
    });
  }

  /**
   * load Data System Career
   */
  loadDataSystemCareer() {
    const param = {};
    this.api.excuteAllByWhat(param, "130").subscribe((data) => {
      if (data) {
        this.careers = data;
      }
    });
  }

  /**
   * on Cancel Click
   */
  onCancelClick() {
    this.job_config = {
      idcompany: "1",
      position: "",

      //hoa hồng
      amountrecruitment: "",

      //vị trí tuyển dụng
      positionrecruitment: "",

      //lương
      salary: "",
      asomission: "",

      //địa chỉ
      addresswork: "",

      //ngành nghề
      career: "",

      //mô tả
      descriptionwork: "",

      //quyền lợi
      benefit: "",

      //kinh nghiệm
      exprience: "",

      //trình độ học vấn
      academiclevel: "",

      //giới tính
      sex: "",

      //trình độ ngoại ngữ
      languagelevel: "",

      //hạn nộp hồ sơ
      deadlineapply: "",

      //yêu cầu công việc
      requirementwork: "",

      //Yêu cầu hồ sơ
      requirementdocument: "",

      fullname: "",
      phone: "",
      email: "",

      //địa chỉ
      positioncontact: "",
      //kinh nghiệm

      //loại hình công việc
      typeofwork: "",

      status: "0",
    };
  }
}
