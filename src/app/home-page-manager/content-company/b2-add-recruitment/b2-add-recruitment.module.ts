import { NgModule } from '@angular/core';
import {NgForm, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { B2AddRecruitmentComponent } from './b2-add-recruitment.component'
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { MatNativeDateModule} from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { CKEditorModule } from 'ng2-ckeditor';  

@NgModule({
  declarations: [B2AddRecruitmentComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: B2AddRecruitmentComponent, children: [
        ],
      }
    ]),
    MatCardModule, 
    MatInputModule,
    MatSelectModule, 
    MatSortModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,  
    CKEditorModule
  ]
})
export class B2AddRecruitmentModule { }
