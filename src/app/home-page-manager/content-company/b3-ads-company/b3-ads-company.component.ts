import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { SelectionModel } from "@angular/cdk/collections";
import { B5AddPointComponent } from "../b5-add-point/b5-add-point.component";
import { MatDialog } from "@angular/material/dialog";
import { ApiService } from "src/app/common/api-service/api.service";
import { Company } from "src/app/common/models/60company.models";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

export interface DialogLocationEmployer {
  location: Number;
  name: any;
}

type NewType = DialogLocationEmployer;

@Component({
  selector: "app-b3-ads-company",
  templateUrl: "./b3-ads-company.component.html",
  styleUrls: ["./b3-ads-company.component.scss"],
})
export class B3AdsCompanyComponent implements OnInit {
  //** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ["stt", "companyname", "pointcompany"];
  dataSource = new MatTableDataSource<DialogLocationEmployer>();
  selection = new SelectionModel<NewType>(true, []);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; files = [];

  // data source for combobox times
  times: any[] = [
    { value: "0", viewValue: "1 Tuần" },
    { value: "1", viewValue: "2 Tuần" },
    { value: "2", viewValue: "3 Tuần" },
    { value: "4", viewValue: "4 Tuần" },
  ];
  form: FormGroup;
  minDate = new Date();
  constructor(
    public dialog: MatDialog,
    private api: ApiService,
    private formBuilder: FormBuilder,
    private uploadService: ApiService,
  ) {
    this.form = this.formBuilder.group({
      pointcompany: [
        null,
        [
          Validators.required,
          Validators.maxLength(11),
          Validators.pattern("[0-9]*"),
          Validators.min(500),
        ],
      ],
      endads: [null, [Validators.required]],
      startads: [null, [Validators.required]],
    });
  }
  imgURL:any;
  fileToUpload: File = null;
  type: number;
  //input default
  input: Company = {
    id: 22,
    pointcompany: "",
    endads: "",
    startads: "",
    logo:""
  };
  // open Checkout Dialog
  openCheckoutDialog(): void {
    const dialogRef = this.dialog.open(B5AddPointComponent, {
      width: "600px",
      height: "310px",
    });

    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  ngOnInit() {
    this.loadDataJobs();
  }
  /**
   * load Data Jobs
   */
  loadDataJobs() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, "150").subscribe((data) => {
        if (data.length > 0) {
          //số thứ tự
          let stt = 1;
          data.forEach((item) => {
            item.stt = stt++;
          });
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    );
  }


  // url logo
  public imagePath;
  public message: string;

  

  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  myUrl:string="";
  isUpdate = false;
  
  // get time save image 
  today = new Date(); 
  date = this.today.getFullYear()+'-'+  ((this.today.getMonth()+1) > 9 ? (this.today.getMonth()+1) : ('0'+(this.today.getMonth()+1)))+'-'+(this.today.getDate() > 9 ? this.today.getDate() : ('0'+this.today.getDate())); 
  time = (this.today.getHours() > 9 ? this.today.getHours() : ('0'+this.today.getHours())) + '-' + (this.today.getMinutes() > 9 ? this.today.getMinutes() : ('0'+this.today.getMinutes())) + '-' + (this.today.getSeconds() > 9 ? this.today.getSeconds() : ('0'+this.today.getSeconds())); 
  dateTime = this.date+'-'+this.time;
  /**
   * upload file
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('date1', this.dateTime );
    this.myUrl="http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/"+this.dateTime + file.data.name;
    this.input.logo = this.myUrl;
    file.inProgress = true;
    this.uploadService.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
        }
      });
  }

  //preview on upload
  preview(files) {
    this.isUpdate = true;
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    
    }  
  }

  /**
     * on click
     */
  onClick() {
    const fileUpload = this.fileUpload.nativeElement; fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  
  /**
   * on Ok Click
   */
  onOkClick() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    const param = {
      times: this.times.values,
    };
    this.input.startads = this.api.formatDate(new Date(this.input.startads));
    this.api.excuteAllByWhat(this.input, "153").subscribe((data) => {
      this.api.showSuccess("Xử Lý Thành Công ");
    });
    window.location.reload();
  }

  //clear btn
  onClickClear() {
    window.location.reload();
  }
}
