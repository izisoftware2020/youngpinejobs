import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { B3AdsCompanyComponent } from './b3-ads-company.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common'; 
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { MatPaginatorModule } from '@angular/material/paginator'; 
import { MatSelectModule } from '@angular/material/select'; 
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table'; 
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { MatNativeDateModule} from '@angular/material/core';
import { MatIconModule} from '@angular/material/icon';




@NgModule({
  declarations: [B3AdsCompanyComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: B3AdsCompanyComponent, children: [
        ],
      }
    ]),
    MatCardModule, 
    MatInputModule,
    MatSelectModule, 
    MatSortModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatIconModule
  ]
})
export class B3AdsCompanyModule { }
