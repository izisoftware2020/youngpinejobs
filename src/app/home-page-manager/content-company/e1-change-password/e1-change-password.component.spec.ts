import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { E1ChangePasswordComponent } from './e1-change-password.component';

describe('E1ChangePasswordComponent', () => {
  let component: E1ChangePasswordComponent;
  let fixture: ComponentFixture<E1ChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ E1ChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E1ChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
