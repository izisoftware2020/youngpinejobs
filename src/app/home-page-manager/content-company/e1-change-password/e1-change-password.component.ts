import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginCookie } from 'src/app/common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Company } from 'src/app/common/models/60company.models';
import { MustMatch } from "src/app/common/validations/must-match.validator";


@Component({
  selector: 'app-e1-change-password',
  templateUrl: './e1-change-password.component.html',
  styleUrls: ['./e1-change-password.component.scss']
})
export class E1ChangePasswordComponent implements OnInit, OnDestroy{

  // validate
  form: FormGroup;

  //id company
  idCompany: number;

  //old password
  oldpassword:string;

  //new password
  newpassword: string;

  //re password
  repassword: string;

  constructor(
    private login: LoginCookie,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      password: [null, [Validators.required, Validators.maxLength(50)]],
      newpassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
      repassword: [null, [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
    },
    { validator: MustMatch("newpassword", "repassword") }
    );

    // company value
    this.company = this.api.getCompanyValue;
    this.idCompany =  this.api.getCompanyValue.id;
  }

  //value hide
  hide = true;
  hide1 = true;
  hide2 = true;

  subscription: Subscription[] = [];

  //model biding insert
  company: Company;

  ngOnInit() { 
    
  }
  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Change Password
   */
  onChangePassword() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }

    if (this.oldpassword == this.api.getCompanyValue.password) {
      this.company.password = this.newpassword;
      this.subscription.push(this.api.excuteAllByWhat(this.company, '62').subscribe(data => {
        if (data) {
          
          this.api.showSuccess("Thêm mới thành công!");
        }
      }));
    } else {
      this.api.showError("Mật khẩu cũ chưa đúng hoặc pass nhập lại chưa trùng");
    }
  }

  
}
