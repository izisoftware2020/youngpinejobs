import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  stt: number;
  name: string;
  email: string;
  phone: string;
  address: string;
  type_work: string;
  rank_want: string;
  work_at: string;
  view: number;
  updated_at: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { stt: 1, name: 'daicatu', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Đà Nãng', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 2, name: 'daicaphuong', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Quảng Ngãi', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 3, name: 'daicabinh', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Gia Lai', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
  { stt: 4, name: 'daicamt4', email: 'phamtu613@gmail.com', phone: '0782105516', address: 'Quảng Nam', type_work: 'Part time', rank_want: 'manager', 'work_at': 'Quảng Ngãi', 'view': 20, 'updated_at': '12/4/2020' },
];

@Component({
  selector: 'app-d1-message',
  templateUrl: './d1-message.component.html',
  styleUrls: ['./d1-message.component.scss']
})
export class D1MessageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
