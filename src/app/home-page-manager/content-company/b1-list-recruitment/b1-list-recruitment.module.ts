import { B1ListRecruitmentComponent, EmployerAdsDialog, DialogEditEmploy } from './b1-list-recruitment.component'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { MatPaginatorModule } from '@angular/material/paginator'; 
import { MatSelectModule } from '@angular/material/select'; 
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table'; 
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { MatNativeDateModule} from '@angular/material/core'; 





@NgModule({
  declarations: [B1ListRecruitmentComponent,EmployerAdsDialog,DialogEditEmploy],
  imports: [
    CommonModule,    
    TransferHttpCacheModule,
    RouterModule.forChild([
      {
        path: '', component: B1ListRecruitmentComponent, children: [
        ],
      }
    ]),
    MatCardModule, 
    MatInputModule,
    MatSelectModule, 
    MatSortModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule, 
  ] 
})
export class B1ListRecruitmentModule { }
