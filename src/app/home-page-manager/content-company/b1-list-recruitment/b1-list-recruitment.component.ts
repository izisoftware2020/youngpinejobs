import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription, Observable, Observer } from "rxjs";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { SelectionModel } from "@angular/cdk/collections";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from "@angular/material/dialog";
import { B5AddPointComponent } from "../b5-add-point/b5-add-point.component";
import { ApiService } from "src/app/common/api-service/api.service";
import { Jobs } from "src/app/common/models/10jobs.models";
import { DialogEditProfile } from "../c3-employer-profile/c3-employer-profile.component";
import { DialogData } from "../../content/b1-manager-advertisement-banner/b1-manager-advertisement-banner.component";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-b1-list-recruitment",
  templateUrl: "./b1-list-recruitment.component.html",
  styleUrls: ["./b1-list-recruitment.component.scss"],
})
export class B1ListRecruitmentComponent implements OnInit {
  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = [
    "stt",
    "position",
    "status",
    "postdate",
    "deadlineapply",
    "edit",
    "ads",
  ];

  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${this.selection.isSelected(row) ? "deselect" : "select"} row ${
      row.positionrecruitment + 1
    }`;
  }

  // data source for combobox status
  status: any[] = [
    { value: "0", viewValue: "Tất cả" },
    { value: "1", viewValue: "Tin hết hạn" },
    { value: "2", viewValue: "Tin còn hạn" },
  ];

  trangthai: any[] = [];
  positionId: string ='';

  setNameStatus(number) {
    if (number < 0) {
      return "Tin hết hạn";
    } else {
      return "Tin còn hạn";
    }
    // return (number < 0) ? 'Tin hết hạn' : 'Tin còn hạn ';
  }

  // setNameStatus(number) {
  //   return (number < 0) ? 'Tin hết hạn' : 'Tin còn hạn ';
  // }  

  /**
   *
   * @param positionrecruitment
   */
  setNamePositionrecruitment(positionrecruitment): string {
    switch (positionrecruitment) {
      case (positionrecruitment = "0"):
        return "Thực tập sinh";
      case (positionrecruitment = "1"):
        return "Nhân viên";
      case (positionrecruitment = "2"):
        return "Trưởng nhóm";
      case (positionrecruitment = "3"):
        return "Trưởng phòng";
      case (positionrecruitment = "4"):
        return "Phó giám đốc";
      case (positionrecruitment = "5"):
        return "Giám đốc";
    }
    return "";
  }
  //validate
  form: FormGroup;
  //trangthai
  trangthaiId: string = "0";
  constructor(public dialog: MatDialog, 
    private api: ApiService,
    private formBuilder: FormBuilder) {
    
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.loadDataSystemJobs();
    this.onFillterClick();
  }

  /**
   * load Data System Jobs
   */
  loadDataSystemJobs() {
    const param = {};
    this.api.excuteAllByWhat(param, "68").subscribe((data) => {
      if (data) {
        this.trangthai = data;

        this.onFillterClick();
      }
    });
  }

  /**
   * on Fillter Click
   */
  onFillterClick() {
    const param = {
      'status': this.trangthaiId,
      'position': this.positionId,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '68').subscribe(data => { 

      if (data.length > 0) {
        // Số thứ tự
        let stt = 1;
        data.forEach(item => {
          item.stt = stt++;
        });

        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        this.dataSource = new MatTableDataSource([]);
      }

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }));
  }

  /**
   * open Dialog
   *
   */
  openDialogEmployerAds(element): void {
    const dialogRef = this.dialog.open(EmployerAdsDialog, {
      width: "1100px",
      height: "800px",
      data: { input: element },
    });

    dialogRef.afterClosed().subscribe((result) => {
    });
  }

  // open dialog
  updateClick(element) {
    const dialogRef = this.dialog.open(DialogEditEmploy, {
      width: "1200px",
      height: "800px",
      data: { input: element },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.onFillterClick();
    });
  }
}

/**
 * AD COMPANY
 */

@Component({
  selector: "b1-list-recruitment-ads-dialog.component",
  templateUrl: "b1-list-recruitment-ads-dialog.component.html",
  styleUrls: ["./b1-list-recruitment.component.scss"],
})
export class EmployerAdsDialog {
  //** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ["stt", "companyname", "position", "point"];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);
  observable: Observable<any>;
  observer: Observer<any>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  form: FormGroup;
  //input value jobs
  input: Jobs = {
    id: 0,
    idcompany: "1",
    descriptionwork: "",
    point: "",
    status: "0",
    startads: "",
    endads: "",
  };
  minDate = new Date();
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<EmployerAdsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.input = data.input;
    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });

    this.form = this.formBuilder.group({
      point: [null,[Validators.required,Validators.maxLength(30),Validators.pattern("[0-9]*"),Validators.min(500)]],
      position: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      endads: [null, [Validators.required]],
      startads: [null, [Validators.required]],
      descriptionwork: [null, [Validators.required,Validators.maxLength(500),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
    });
  }

  // data source for combobox times
  times: any[] = [
    { value: "1", viewValue: "1 Tuần" },
    { value: "2", viewValue: "2 Tuần" },
    { value: "3", viewValue: "3 Tuần" },
    { value: "4", viewValue: "4 Tuần" },
  ];
  timeId:string='';
 
  // open Checkout Dialog
  openCheckoutDialog(): void {
    const dialogRef = this.dialog.open(B5AddPointComponent, {
      width: "600px",
      height: "310px",
    });

    dialogRef.afterClosed().subscribe((result) => {
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.loadDataJobs();
  }
  /**
   * load Data Jobs
   */
  loadDataJobs() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, "152").subscribe((data) => {
        if (data.length > 0) {
          //số thứ tự
          let stt = 1;
          data.forEach((item) => {
            item.stt = stt++;
          });
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    );
  }

  /**
   * on Ok Click
   */
  onOkClick() {
    const param = {
      'times' : this.timeId,
      'id': this.input.id,
      'point' : this.input.point,
      'position' : this.input.position,
      'descriptionwork' : this.input.descriptionwork,
      'startads' : this.input.startads,
    };
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    this.input.startads = this.api.formatDate(new Date(this.input.startads));
    this.api.excuteAllByWhat(param, "151").subscribe((data) => {
      console.log(this.data);
      
      this.dialogRef.close(true);
      this.api.showSuccess("Xử Lý Thành Công ");
    });
  }
}

// Dialog Edit Employ
@Component({
  selector: "b1-list-recruitment-edit-dialog.component",
  templateUrl: "b1-list-recruitment-edit-dialog.component.html",
  styleUrls: ["./b1-list-recruitment.component.scss"],
})
export class DialogEditEmploy {
  subscription: Subscription[] = [];
  observable: Observable<any>;
  observer: Observer<any>;

  //input value jobs
  input: Jobs = {
    idcompany: " ",
    position: "",
    amountrecruitment: "",
    positionrecruitment: "",
    salary: "",
    asomission: "",
    addresswork: "",
    career: "",
    descriptionwork: "",
    benefit: "",
    exprience: "",
    academiclevel: "",
    sex: "",
    languagelevel: "",
    deadlineapply: "",
    requirementwork: "",
    requirementdocument: "",
    fullname: "",
    phone: "",
    email: "",
    positioncontact: "",
    typeofwork: "",
    status: "",
  };
  form: FormGroup;
  form1: FormGroup;
  form2: FormGroup;

  minDate = new Date();
  constructor(
    public dialogRef: MatDialogRef<DialogEditProfile>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.input = data.input;
    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });

    // edit validate for controls
    this.form = this.formBuilder.group({
      position: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      amountrecruitment: [null,[Validators.required,Validators.maxLength(10), Validators.min(1),Validators.pattern("[0-9]*")]],
      positionrecruitment: [null, [Validators.required]],
      typeofwork: [null, [Validators.required]],
      salary: [null,[Validators.required,Validators.maxLength(30),Validators.minLength(6),Validators.pattern("[0-9]*"),Validators.min(100000),]],
      asomission: [null,[Validators.maxLength(30), Validators.min(6),Validators.pattern("[0-9]*")]],
      addresswork: [null, [Validators.required, Validators.maxLength(100)]],
      career: [null,[Validators.required,Validators.maxLength(100),Validators.pattern("[0-9]*")]],
      descriptionwork: [null, [Validators.required,Validators.maxLength(500),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      benefit: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
    });
    this.form1 = this.formBuilder.group({
      exprience: [null, [Validators.required]],
      academiclevel: [null, [Validators.required]],
      sex: [null, [Validators.required]],
      languagelevel: [null, [Validators.required]],
      deadlineapply: [null, [Validators.required]],
      requirementwork: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      requirementdocument: [null, [Validators.required, Validators.maxLength(500),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
     
    });
    this.form2 = this.formBuilder.group({
      fullname: [null, [Validators.required, Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phone: [null,[Validators.required,Validators.maxLength(11),Validators.pattern("[0-9]*"),Validators.minLength(10)]],
      email: [null,[Validators.required, Validators.maxLength(50), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
      positioncontact: [null, [Validators.maxLength(100)]]
    });
  }

  //save me
  onOkClick() {
    // return if error
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    if (this.form1.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    if (this.form2.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    this.input.deadlineapply = this.api.formatDate(
      new Date(this.input.deadlineapply)
    );
    this.input.postdate = this.api.formatDate(
      new Date(this.input.postdate)
    );
    
    this.input.deadlineapply = this.api.formatDate(
      new Date(this.input.deadlineapply)
    );
    this.input.endads = this.api.formatDate(new Date(this.input.endads));
    this.api.excuteAllByWhat(this.input, "12").subscribe((data) => {
      this.dialogRef.close(true);
      this.api.showSuccess("Xử Lý Thành Công ");
    });
  }

  jobs1: Jobs;
  // data source for combobox experience
  experiences: any[] = [
    { value: '0', viewValue: 'Kinh nghiệm' },
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  // data source for combobox level
  levels: any[] = [
    { value: "0", viewValue: "Tốt nghiệp phổ thông" },
    { value: "1", viewValue: "Trung cấp" },
    { value: "2", viewValue: "Cao đẳng" },
    { value: "3", viewValue: "Đại học" },
    { value: "4", viewValue: "Trên đại học" },
  ];

  // data source for combobox
  employs: any[] = [
    { value: "0", viewValue: "Thực tập sinh" },
    { value: "1", viewValue: "Nhân viên" },
    { value: "2", viewValue: "Trưởng nhóm" },
    { value: "3", viewValue: "Trưởng phòng" },
    { value: "4", viewValue: "Phó giám đốc" },
    { value: "5", viewValue: "Giám đốc" },
  ];

  // data source for combobox type
  types: any[] = [
    { value: '0', viewValue: 'Loại hình' },
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thòi gian cố định' },
    { value: '4', viewValue: 'Bán thòi gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  // data source for combobox city
  citys: any[] = [];

  // data source for combobox sex
  sexs: any[] = [
    { value: "0", viewValue: "Nam" },
    { value: "1", viewValue: "Nữ" },
  ];

  // data source for combobox language
  languages: any[] = [
    { value: "0", viewValue: "Tiếng Anh" },
    { value: "1", viewValue: "Tiếng Pháp" },
    { value: "2", viewValue: "Tiếng Hàn" },
  ];

  //data source for combobox branchs
  careers: any[] = [];

  /**
   * onNoClick
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.loadDataSystemEdit();
    this.loadDataSystemCareer();
  }
  /**
   * load Data System Edit
   */
  loadDataSystemEdit() {
    const param = {};
    this.api.excuteAllByWhat(param, "147").subscribe((data) => {
      if (data) {
        this.citys = data;
      }
    });
  }

  /**
   * load Data System Career
   */
  loadDataSystemCareer() {
    const param = {};
    this.api.excuteAllByWhat(param, "130").subscribe((data) => {
      if (data) {
        this.careers = data;
      }
    });
  }
}
