

////////////////////////////////////////////////////////////////////////
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs'; import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Applywork } from 'src/app/common/models/30applywork.models';
import { delay, windowTime } from 'rxjs/operators';

@Component({
  selector: 'app-c3-employer-profile',
  templateUrl: './c3-employer-profile.component.html',
  styleUrls: ['./c3-employer-profile.component.scss']
})

export class C3EmployerProfileComponent implements OnInit {
  /** for table */
  subscription: Subscription[] = [];
  displayedColumns: string[] = ['select', 'stt', 'id', 'fullname', 'position', 'startdate', 'status', 'edit', 'delete'];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator,
    { static: false }) paginator: MatPaginator; @ViewChild(MatSort,
      { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  // data source for combobox status
  status: any[] = [
  {value: '0', viewValue: 'Tất Cả'},
  { value: '1', viewValue: 'Chưa liên hệ' },
  { value: '2', viewValue: 'Đã liên hệ' },
  { value: '3', viewValue: 'Trúng tuyển' },
  { value: '4', viewValue: 'Không trúng tuyển' }
  ];

  //data positions
  positions: any[] = [];

  // translate status 
  getStatus(status): string {
    switch (status) {
      case this.applywork.status = '1':
        return 'Chưa liên hệ';
      case this.applywork.status = '2':
        return 'Đã liên hệ';
      case this.applywork.status = '3':
        return 'Trúng tuyển';
      case this.applywork.status = '4':
        return 'Không trúng tuyển';
    }    return '';
  }
  // Biding model

  /** * @param api */
  constructor(private api: ApiService, private dialog: MatDialog) { }

  applywork: Applywork;

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.onLoadDataTable();
    this.onFillterClick();
  }

  onLoadDataTable() {
    const param = {
      "idcompany": 1
    };
    this.api.excuteAllByWhat(param, '206').subscribe(data => {
      if (data.length > 0) {
        this.positions = data;
      }
    });
  }

  /**   * on Fillter Click   */
  positionId: String = '';
  statusId: String = '0';

  onFillterClick() {
    const param = {
      "status": this.statusId,
      "position": this.positionId
    };
    this.api.excuteAllByWhat(param, '38 ').subscribe(data => {
      console.log(data);
      if (data.length > 0) {
        //Stt 
        let stt = 1;
        data.forEach(item => {
          item.stt = stt++;
        });
        this.applywork = data;
        //Set data for table    
        this.dataSource = new MatTableDataSource(data);
      } else {
        //Set data for table    
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  /**   * openDialog Edit Profile   * 
   * @param row  
   *   */
  openDialogEditProfile(row): void {
    const dialogRef = this.dialog.open(DialogEditProfile, {
      width: '400px', height: '240px', 
      data: { input: row}, 
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); 
      this.onFillterClick();
    });
  }

  // DialogViewProfile
  DialogViewProfile(row): void {
    const dialogRef = this.dialog.open(DialogViewProfile, {
      width: '600px', height: '290px', data: { input: row },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  //onDeleteClick 
  onDeleteClick() {
    let listId = '';
    this.selection.selected.forEach(item => {
      if (listId == '') {
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    }); const param = { "listId": listId };
    //start update status  
    if (listId != '') {
      this.api.excuteAllByWhat(param, '33').subscribe(data => {
        this.onFillterClick();
        this.api.showSuccess('Xoa Thanh Cong')
      });
    } else {
      this.api.showSuccess('Vui long chon it nhat mot muc')
    }
  }
}

// OpenDialogEditProfile
@Component({
  selector: 'c3-edit-profile-dialog.component',
  templateUrl: 'c3-edit-profile-dialog.component.html',
  styleUrls: ['./c3-employer-profile.component.scss']
})
export class DialogEditProfile {
 
  // data source for combobox status
  status1: any[] = [
    { value: '1', viewValue: 'Chưa liên hệ' },
    { value: '2', viewValue: 'Đã liên hệ' },
    { value: '3', viewValue: 'Trúng tuyển' },
    { value: '4', viewValue: 'Không trúng tuyển' }
  ]; 

  //data input
  input: Applywork; 

  //data applywork
  applywork = {
    idcandidate: '',
    idjobs: '',
    startdate: '',
    status: '',
  }
  constructor(public dialogRef: MatDialogRef<DialogEditProfile>, @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService) {
    this.input = data.input;

  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  /**
   * ngOnInit
   */
  ngOnInit() {
    this.onLoadDataTable();
  }

  /**
   *on LoadDataTable
   */
  onLoadDataTable() {
    const param = {
      "id": this.input.id
    };
    this.api.excuteAllByWhat(param, '34').subscribe(data => {
      if (data.length > 0) {
        this.applywork = data[0];
      }
    });
  }

  /**
   * updateStatusOnClick
   */
  updateStatusOnClick() { 
    this.api.excuteAllByWhat(this.applywork, '32').subscribe(data => {
      if (data) {
        this.onNoClick();
      }
      this.api.showSuccess('Cập nhật thành công')
      
    }); 
    
  } 
}

// Open Dialog View Profile
@Component({
  selector: 'c3-view-profile-dialog.component',
  templateUrl: 'c3-view-profile-dialog.component.html',
  styleUrls: ['./c3-employer-profile.component.scss']
})

export class DialogViewProfile {
  //data input
  input: Applywork = {};

  //data applyworks2
  applyworks2 = {
    id: "",
    fullname: "",
    email: "",
    phone: "",
  };

  constructor(
    public dialogRef: MatDialogRef<DialogViewProfile>,
    @Inject(MAT_DIALOG_DATA) public data: any, private api: ApiService) {
    this.input = data.input;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.loadDataViewProfile();
  }

  /**
   * loadDataViewProfile
   */
  loadDataViewProfile() {
    const param = {
      'idcandidate': this.input.idcandidate
    }
    this.api.excuteAllByWhat(param, '39').subscribe(data => {
      if (data.length > 0) {
        this.applyworks2 = data[0];
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}