import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { C3EmployerProfileComponent, DialogEditProfile, DialogViewProfile } from './c3-employer-profile.component'
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common'; 
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { MatPaginatorModule } from '@angular/material/paginator';  
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';  
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';



@NgModule({
  declarations: [C3EmployerProfileComponent,DialogEditProfile,DialogViewProfile],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: C3EmployerProfileComponent, children: [
        ],
      }
    ]),
    MatCardModule,
    MatProgressBarModule,
    MatDividerModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    TransferHttpCacheModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSortModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    
  ]
})
export class C3EmployerProfileModule { }
