
import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog'; 
import { ApiService } from 'src/app/common/api-service/api.service';
import { Candidatesave } from 'src/app/common/models/70candidatesave.models';


@Component({
  selector: 'app-c1-saved-profile',
  templateUrl: './c1-saved-profile.component.html',
  styleUrls: ['./c1-saved-profile.component.scss']
})
export class C1SavedProfileComponent implements OnInit, OnDestroy{
  
  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select', 'stt', 'fullname', 'startdate', 'view', 'unsave'];

  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${
      this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  //model biding insert
  candidatesave: Candidatesave;

  constructor(public dialog: MatDialog, private api: ApiService) { 
    
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.loadDataCandidatesave();
  }

  /**
     * ngOnDestroy
     */
    ngOnDestroy() {
      this.subscription.forEach(item => {
          item.unsubscribe();
      });
  }

  /**
   * load Data Candidatesave
   */
  loadDataCandidatesave() {
    const param = {
      'idcompany': 47
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '77').subscribe(data => {  
      if (data.length > 0) {
        // Số thứ tự
        let stt = 1;
        data.forEach(item => {
          item.stt = stt++;
        }); 
        // set data for table	
        this.dataSource = new MatTableDataSource(data);
      } else {
        this.dataSource = new MatTableDataSource([]);
      } 
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }));
  }

  /**
   * on Delete Click
   */
  onDeleteClick() {
    // get listid selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
        if (listId == '') {
            listId = item.id;
        } else {
            listId += ',' + item.id;
        }
    });

    const param = { 'id': listId };  
    // listId 1,2,3,4
    if (listId != '') {            
            this.subscription.push(this.api.excuteAllByWhat(param, '73').subscribe(data => {
                // load data grid
                this.loadDataCandidatesave();

                // scroll top
                window.scroll({ left: 0, top: 0, behavior: 'smooth' });
                this.api.showSuccess('Xóa thành công');
            }));
        }
     else {
        this.api.showWarning('Vui lòng chọn ít nhất một mục');
    }
    this.selection = new SelectionModel<any>(true, []);
  } 
}


