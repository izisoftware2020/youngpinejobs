import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { C2ViewedProfileComponent } from './c2-viewed-profile.component'
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon'; 
import { TransferHttpCacheModule } from '@nguniversal/common'; 
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { MatPaginatorModule } from '@angular/material/paginator';  
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';  
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatTabsModule } from '@angular/material/tabs';



@NgModule({
  declarations: [C2ViewedProfileComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: C2ViewedProfileComponent, children: [
        ],
      }
    ]),
    MatCardModule,
    MatProgressBarModule,
    MatDividerModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    TransferHttpCacheModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatSortModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,

  ]
})
export class C2ViewedProfileModule { }
