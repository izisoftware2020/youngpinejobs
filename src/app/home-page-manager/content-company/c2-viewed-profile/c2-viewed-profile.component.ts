import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Candidateview } from 'src/app/common/models/80candidateview.models';

@Component({
  selector: 'app-c2-viewed-profile',
  templateUrl: './c2-viewed-profile.component.html',
  styleUrls: ['./c2-viewed-profile.component.scss']
})
export class C2ViewedProfileComponent implements OnInit, OnDestroy {
  /** for table */
  subscription: Subscription[] = [];

  displayedColumns: string[] = ['select','stt','id', 'username', 'phone',  'startdate', 'point','view', 'delete'];

  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selections = new SelectionModel<any>(true, []);

 /** Whether the number of selected elements matches the total number of rows. */
 isAllSelected() {
  if (this.dataSource) {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  return null;
}

/** Selects all rows if they are not all selected; otherwise clear selection. */
masterToggle() {
  this.isAllSelected()
    ? this.selection.clear()
    : this.dataSource.data.forEach(row => this.selection.select(row));
}

/** The label for the checkbox on the passed row */
checkboxLabel(row?: any): string {
  if (!row) {
    return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  }
  return `${
    this.selection.isSelected(row) ? 'deselect' : 'select'
    } row ${row.position + 1}`;
}



  candidateview : Candidateview;
  // data source for combobox phone
  phone: any[] = [];
/**
 * constructor
 * @param api 
 */
  constructor(public dialog: MatDialog,private api: ApiService) { 
  
  }
  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
  });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.loadDataCandidateview();
  }

  /**
   * on Fillter Click
   */
  loadDataCandidateview() {
    const param = {
      'idcompany': 14
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '87').subscribe(data=>{
      console.log(data);
      if(data.length >0){

        //số thứ tự
        let stt = 1;
        data.forEach(item =>{
          item.stt = stt++;
        });

        this.dataSource = new MatTableDataSource(data);
      } else {
        this.dataSource = new MatTableDataSource([]);
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }));
  }
  
  /**
   * on Delete Click
   */
  onDeleteClick() {
    // get listid selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach(item => {
        if (listId == '') {
            listId = item.id;
        } else {
            listId += ',' + item.id;
        }
    });

    const param = { 'id': listId };

    // start update status approved to one
    if (listId != '') {            
            this.subscription.push(this.api.excuteAllByWhat(param, '83').subscribe(data => {
                // load data grid
                this.loadDataCandidateview();
                // scroll top
                window.scroll({ left: 0, top: 0, behavior: 'smooth' });
                this.api.showSuccess('Xóa thành công');
            }));
        }
    else {
        this.api.showWarning('Vui lòng chọn ít nhất một mục');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

}

