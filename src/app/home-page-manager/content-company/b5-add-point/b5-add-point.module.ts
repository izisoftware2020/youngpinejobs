import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { B5AddPointComponent } from './b5-add-point.component'; 
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from "@angular/forms" 




@NgModule({
  declarations: [B5AddPointComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressBarModule,
    MatDividerModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule, 
    FormsModule
    
  ]
})
export class B5AddPointModule { }
