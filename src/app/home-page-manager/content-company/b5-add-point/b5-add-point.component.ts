import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-b5-add-point',
  templateUrl: './b5-add-point.component.html',
  styleUrls: ['./b5-add-point.component.scss']
})
export class B5AddPointComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<B5AddPointComponent>,
    @Inject(MAT_DIALOG_DATA) public input: B5AddPointComponent,
    public dialog: MatDialog) {
  }

  money: string;

  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
     * numberOnly
     * @param event 
     */
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngOnInit(): void {
  }

}
