
import { Component, OnInit, ElementRef, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs'
import { B5AddPointComponent } from '../b5-add-point/b5-add-point.component';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Company } from 'src/app/common/models/60company.models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { LoginCookie } from 'src/app/common/core/login-cookie';


@Component({
  selector: "app-a1-information-company",
  templateUrl: "./a1-information-company.component.html",
  styleUrls: ["./a1-information-company.component.scss"],
})
export class A1InformationCompanyComponent implements OnInit, OnDestroy {
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef;
  files = [];
  @ViewChild("fileUploadBanner", { static: false })
  fileUploadBanner: ElementRef;
  filesBanner = [];
  
  idCompany: number;

  

  subscription: Subscription[] = [];

  // data source for combobox level
  personalscales: any[] = [
    { value: "1", viewValue: "Dưới 20 người" },
    { value: "2", viewValue: "20 - 150 người" },
    { value: "3", viewValue: "150 - 300 người" },
    { value: "4", viewValue: "Trên 300 người" },
  ];

  //model biding insert
  company: Company;

  citys: any[]=[];

  //upload File
  imgURL: any;
  imgURLBanner: any;
  fileToUpload: File = null; 
  type: number;

  // url logo
  public imagePath;
  public message: string;
  myUrl: string = "";
  myUrlBanner: string = "";
  isUpdate = false;
  isUpdateBanner = false;

  // get time save image
  today = new Date();
  date =
    this.today.getFullYear() +
    "-" +
    (this.today.getMonth() + 1 > 9
      ? this.today.getMonth() + 1
      : "0" + (this.today.getMonth() + 1)) +
    "-" +
    (this.today.getDate() > 9
      ? this.today.getDate()
      : "0" + this.today.getDate());
  time =
    (this.today.getHours() > 9
      ? this.today.getHours()
      : "0" + this.today.getHours()) +
    "-" +
    (this.today.getMinutes() > 9
      ? this.today.getMinutes()
      : "0" + this.today.getMinutes()) +
    "-" +
    (this.today.getSeconds() > 9
      ? this.today.getSeconds()
      : "0" + this.today.getSeconds());
  dateTime = this.date + "-" + this.time;

  // validate
  form: FormGroup;
  form1: FormGroup;

  constructor(
    private login: LoginCookie,public dialog: MatDialog, private api: ApiService,
    private formBuilder: FormBuilder, private uploadService: ApiService) {

      // company value
      this.company = this.api.getCompanyValue;
      this.idCompany =  this.api.getCompanyValue.id;

      

    // add validate for controls
    this.form = this.formBuilder.group({
      companyname: [null, [Validators.required, Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      address: [null, [Validators.required, Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      city: [null, [Validators.required]],
      personalscale: [null, [Validators.required]],
      companyphone: [
        null,
        [
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(11),
          Validators.minLength(10), 
        ],
      ],
      website: [null, [Validators.required, Validators.maxLength(50),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      career: [null, [Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      about: [null],
    });
    this.form1 = this.formBuilder.group({
      fullname: [null, [Validators.required, Validators.maxLength(30),Validators.minLength(5),Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
      phone: [
        null,
        [
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.maxLength(11),
          Validators.minLength(10),

        ],
      ],
      personalemail: [
        null,
        [
          Validators.required,
          Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}"),
          Validators.maxLength(50),
        ],
      ],
      position: [null,[Validators.maxLength(150),Validators.minLength(5), Validators.pattern('^[A-Za-z0-9!@#$%^&*()/,.?":{}[]|<>-][A-Za-z0-9 -!@#$%^&*()/,.?":{}[]|<>-]*$')]],
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.loadDataCity();    
    this.loadDataCompany();

  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on click
   */
  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }


  

  private uploadFiles() {
    this.fileUpload.nativeElement.value = "";
    this.files.forEach((file) => {
      this.uploadFile(file);
    });
  }

  /**
   * upload file
   */
  uploadFile(file) {
    const formData = new FormData();
    formData.append("file", file.data);
    formData.append("date1", this.dateTime);
    this.myUrl =
      "http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/" +
      this.dateTime +
      file.data.name;
    this.company.logo = this.myUrl;
    console.log(this.company.logo)
    file.inProgress = true;
    this.uploadService
      .upload(formData)
      .pipe(
        map((event) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              file.progress = Math.round((event.loaded * 100) / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          file.inProgress = false;
          return of(`${file.data.name} upload failed.`);
        })
      )
      .subscribe((event: any) => {
        if (typeof event === "object") {
        }
      });
  }

  //preview on upload
  preview(files) {
    this.isUpdate = true;
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
  }


  /**
   * on click for banner
   */
  onClickBanner() {
    const fileUploadBanner = this.fileUploadBanner.nativeElement;
    fileUploadBanner.onchange = () => {
      for (let index = 0; index < fileUploadBanner.files.length; index++) {
        const file = fileUploadBanner.files[index];
        this.filesBanner.push({ data: file, inProgress: false, progress: 0 });

      }
      this.uploadFilesBanner();
    };
    fileUploadBanner.click();
  }

  private uploadFilesBanner() {
    this.fileUploadBanner.nativeElement.value = "";
    this.filesBanner.forEach((file) => {
      this.uploadFileBanner(file);
    });
  }

  /**
   * upload file
   */
  uploadFileBanner(file) {
    const formData = new FormData();
    formData.append("file", file.data);
    formData.append("date1", this.dateTime);
    this.myUrlBanner =
    "http://hoctienganhphanxa.com/hoctienganhphanxa.com/hce/youngpinejobapi/Controller/assets/images/" +
      this.dateTime +
      file.data.name;
    this.company.bussinesslicence = this.myUrlBanner;
    console.log(this.company.bussinesslicence)
    file.inProgress = true;
    this.uploadService
      .upload(formData)
      .pipe(
        map((event) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              file.progress = Math.round((event.loaded * 100) / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          file.inProgress = false;
          return of(`${file.data.name} upload failed.`);
        })
      )
      .subscribe((event: any) => {
        if (typeof event === "object") {
        }
      });
  }

  //preview on upload
  previewBanner(files) {
    this.isUpdateBanner = true;
    if (files.length === 0) return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }


    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURLBanner = reader.result;
    };
  }


    /**
   * load Data City
   */
  loadDataCity() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '147').subscribe(data => {
      if (data.length > 0) {
        this.citys = data;
      }
    }));
  }

  /**
   * load Data Company
   */
  loadDataCompany() {
    const param = {
      'id': this.idCompany,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '64').subscribe(data => {
      console.log(data);
      if (data.length > 0) {
        this.company = data[0];
        
      }
    })); 
  }  

  /**
   * on Update Company
   */
  onUpdateCompany() {
    // return if error
    if (this.form.status != "VALID") {
      this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
      return;
    }
    // this.company.logo = this.linkImage;

    this.subscription.push(
      this.api.excuteAllByWhat(this.company, "62").subscribe((data) => {
        if (data) {
          // load data grid
          // this.loadDataCompany();


          this.api.showSuccess("Cập nhật thành công");
        }
      })
    );
  }

  /**
   * On Cancel
   */
  OnCancel() {
    this.form.reset();
    this.form1.reset();
    this.loadDataCompany();
    
  }  

  // open Checkout Dialog
  openCheckoutDialog(): void {
    const dialogRef = this.dialog.open(B5AddPointComponent, {
      width: "600px",
      height: "310px",
    });


    dialogRef.afterClosed().subscribe((result) => {});

  }
}


