import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { A1InformationCompanyComponent } from './a1-information-company.component'
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from "@angular/forms" 
import {MatButtonModule} from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';    
import { CKEditorModule } from 'ng2-ckeditor'; 

@NgModule({
  declarations: [A1InformationCompanyComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: A1InformationCompanyComponent, children: [
        ],
      }
    ]),
    MatCardModule,
    MatProgressBarModule,
    MatDividerModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule, 
    FormsModule,
    MatButtonModule,
    ReactiveFormsModule,
    CKEditorModule
   
  ]
})
export class A1InformationCompanyModule { }
