import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../common/api-service/api.service';
import { LoginCookie } from '../common/core/login-cookie'; 
import { Staff } from '../common/models/140staff.models';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    constructor(private api: ApiService, private router: Router) { }

    input: Staff;

    ngOnInit(): void {
        this.input = {
            username: "",
            password: "",
        };
    }

    onLoginClick() {
        this.api.excuteAllByWhat(this.input, "207").subscribe((data) => {
            if (data.length > 0) {
                
                localStorage.setItem('staffSubject', JSON.stringify(data[0]));
                this.api.staffSubject.next(data[0]);

                this.api.showSuccess("Đăng nhập Thành Công ");
                this.router.navigate(['/manager/admin/a1-dashboard'])
            } else {
                this.api.showError("Email hoặc Password đã sai");
            }
        });
    }
}