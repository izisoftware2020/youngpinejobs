import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { JwtInterceptor, ErrorInterceptor, AuthGuard } from './common/_helpers';
import { ApiService } from './common/api-service/api.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'hce-edu' }),
        RouterModule.forRoot([
            {
                path: '',
                loadChildren: () => import('./home-page-user/home-page.module').then(m => m.HomePageModule),
            },
            {
                path: 'manager',
                loadChildren: () => import('./home-page-manager/home-page.module').then(m => m.HomePageModule),
            },
            {
                path: 'login',
                loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
            },
            {
                path: 'logincandidate',
                loadChildren: () => import('./logincandidate/logincandidate.module').then(m => m.LogincandidateModule)
            },
            {
                path: 'logincompany',
                loadChildren: () => import('./logincompany/logincompany.module').then(m => m.LogincompanyModule),
            },
            {
                path: 'registercandidate',
                loadChildren: () => import('./registercandidate/registercandidate.module').then(m => m.RegistercandidateModule)
            },
            {
                path: 'registercompany',
                loadChildren: () => import('./registercompany/registercompany.module').then(m => m.RegistercompanyModule)
            },
            {
                path: 'forgotpasswordcandidate',
                loadChildren: () => import('./forgotpasswordcandidate/forgotpasswordcandidate.module').then(m => m.ForgotpasswordcandidateModule)
            },
            {
                path: 'forgotpasswordcompany',
                loadChildren: () => import('./forgotpasswordcompany/forgotpasswordcompany.module').then(m => m.ForgotpasswordcompanyModule)
            },
        ]),
        // https://www.npmjs.com/package/ngx-toastr
        ToastrModule.forRoot(), // ToastrModule added
        TransferHttpCacheModule,
        CommonModule,
        HttpClientModule,

        // https://www.npmjs.com/package/angular-animations
        BrowserAnimationsModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        ApiService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
