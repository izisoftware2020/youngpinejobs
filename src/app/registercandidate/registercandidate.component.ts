import { Component, OnInit } from "@angular/core";
import { Candidate } from "../common/models/20candidate.models";
import { Router } from '@angular/router';
import { ApiService } from "../common/api-service/api.service";
import { Subscription } from "rxjs/internal/Subscription";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: "app-registercandidate",
  templateUrl: "./registercandidate.component.html",
  styleUrls: ["./registercandidate.component.scss"],
})
export class RegistercandidateComponent implements OnInit {


  // newpassword
  repassword: string;

  candidate: Candidate;

  subscription: Subscription[] = [];

  form: FormGroup;

  constructor(private api: ApiService, private formBuilder: FormBuilder,private router: Router) {
    this.form = this.formBuilder.group({
        fullname: [null,[Validators.required,Validators.maxLength(100),Validators.minLength(5), Validators.pattern("^[a-zA-Z0-9]+(([',. -][a-zA-Z0-9])?[a-zA-Z0-9]*)*$")]],
        password: [null, [Validators.required, Validators.maxLength(50),Validators.minLength(8)]],
        repassword: [null, [Validators.required, Validators.maxLength(50),Validators.minLength(8)]],
        phone: [null,[Validators.required,Validators.maxLength(11),Validators.pattern("[0-9]*"),Validators.minLength(10)]],
        email: [null,[Validators.required, Validators.maxLength(50), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]],
      });
  }

  input: Candidate;
  ngOnInit() {
    this.input = {
      email: "",
      password: "",
      fullname: "",
      phone: "",
      role: "candidate",
    };
  }
  onSubmitClick() {
    if (this.input.password == this.repassword) {
      if (this.form.status != "VALID") {
        this.api.showWarning("Vui lòng nhập các mục đánh dấu *");
        return;
      }
      this.api.excuteAllByWhat(this.input, "156").subscribe((data) => {
        if (data) {
          this.api.showSuccess("thêm mới thành công!");
          this.router.navigate(['/logincandidate'])
        }
      });
    } else {
      this.api.showError("Pass không trùng nhau");
    }
  }
}
