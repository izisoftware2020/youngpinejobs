import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistercandidateComponent } from './registercandidate.component';
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../common/api-service/api.service';



@NgModule({
  declarations: [RegistercandidateComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    MatFormFieldModule,
    RouterModule.forChild([
      {
        path: '', component: RegistercandidateComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ApiService],
})
export class RegistercandidateModule { }
