import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { F1CompanyComponent } from './f1-company.component';

@NgModule({
    declarations: [F1CompanyComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: F1CompanyComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class F1CompanyModule { }
