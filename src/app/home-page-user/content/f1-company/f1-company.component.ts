import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-f1-company',
  templateUrl: './f1-company.component.html',
  styleUrls: ['./f1-company.component.scss']
})
export class F1CompanyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
