import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
  selector: 'app-e4-checkout-coaching',
  templateUrl: './e4-checkout-coaching.component.html',
  styleUrls: ['./e4-checkout-coaching.component.scss']
})
export class E4CheckoutCoachingComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];

  //Quantity
  quantity: any[] = [
    { value: '1' },
    { value: '2' },
    { value: '3' },
    { value: '4' },
  ]

  //data suggest
  suggest: string;

  //data quantity
  quantityId: string = '1';

  //data modelinformation
  information = {
    id: '',
    fullname: '',
    born: '',
    sex: '',
    phone: '',
    address: '',
    about: '',
    skill: '',
    avatar: '',
    email: '',
    numberview: ''
  }

  //data model
  packages = {
    id: '',
    name: '',
    price: '',
    about: ''
  }

  // total price
  total(quantityId, price) {
    return quantityId * price;
  }

  //data datacoaching
  datacoaching: any = [];

  // id user login
  candidateId: number;

  constructor(private activatedRoute: ActivatedRoute, private api: ApiService,
    private router: Router) { }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.activatedRoute.params.subscribe(input => {
      this.datacoaching = input;;
      this.getDataCoaching();

      // assign candidateId with user login
      this.candidateId = this.api.getCandidateValue.id;

    });
    this.suggest = localStorage.getItem('suggest');
  }

  /**
   * load Data Coaching
   */
  getDataCoaching() {
    const param = {
      'id': this.datacoaching.idcoaching,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '94', true).subscribe(data => {
      if (data.length > 0) {
        this.information = data[0];
        this.getDataPackage();
      }
    }));
  }

  /**
   * load Data Package
   */
  getDataPackage() {
    const param = {
      'id': this.datacoaching.idpackage
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '104', true).subscribe(data => {
      if (data.length > 0) {
        this.packages = data[0];
      }
    }));

  }


  /**
   * 
   */
  onSubmit() {
    const param = {
      'idcoaching': this.information.id,
      'idcandidate': this.candidateId,
      'idpackage': this.packages.id,
      'startdate': this.api.formatDate(new Date),
      'startconnect': this.api.formatDate(new Date),
      'statuspayment': 0,
      'amount': this.total(this.quantityId, this.packages.price),
      'suggest': this.suggest

    }
    this.subscription.push(this.api.excuteAllByWhat(param, '111', true).subscribe(data => {
      if (data) {
        this.api.showSuccess('Cảm ơn bạn');
      }
    }));
    setTimeout(() => {
      const url = '/h1-thank-you';
      this.router.navigate([url]);
    }, 100);

  }


}
