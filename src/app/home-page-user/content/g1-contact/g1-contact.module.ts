import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { G1ContactComponent } from './g1-contact.component';

@NgModule({
  declarations: [G1ContactComponent],
  imports: [
    TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: G1ContactComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
  ]
})
export class G1ContactModule { }
