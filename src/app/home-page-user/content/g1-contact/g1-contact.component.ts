import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-g1-contact',
  templateUrl: './g1-contact.component.html',
  styleUrls: ['./g1-contact.component.scss']
})
export class G1ContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
