import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { B1ListWorkComponent } from './b1-list-work.component';

@NgModule({
    declarations: [B1ListWorkComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        
        RouterModule.forChild([
            {
                path: '',
                component: B1ListWorkComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class B1ListWorkModule { }
