import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-b1-list-work',
  templateUrl: './b1-list-work.component.html',
  styleUrls: ['./b1-list-work.component.scss']
})
export class B1ListWorkComponent implements OnInit {

  subscription: Subscription[] = [];

  // data for comboBox
  typeOfWork: any[] = [
    { value: 'none', viewValue: 'Loại hình' },
    { value: '1', viewValue: 'Toàn thời gian cố định' },
    { value: '2', viewValue: 'Toàn thời gian tạm thời' },
    { value: '3', viewValue: 'Bán thòi gian cố định' },
    { value: '4', viewValue: 'Bán thòi gian tạm thời' },
    { value: '5', viewValue: 'Theo hợp đồng / tư vấn' },
    { value: '6', viewValue: 'Thực tập' },
    { value: '7', viewValue: 'Khác' },
  ];

  expYears: any[] = [
    { value: "none", viewValue: "Kinh nghiệm" },
    { value: '1', viewValue: 'Chưa có kinh nghiệm' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
    { value: '6', viewValue: '4 năm' },
    { value: '7', viewValue: '5 năm' },
    { value: '8', viewValue: 'Trên 5 năm' },
  ];

  sexs: any[] = [
    { value: "1", viewValue: "Nam" },
    { value: "0", viewValue: "Nữ" },
    { value: "none", viewValue: "Giới tính" }
  ];

  salaries: any[] = [
    { value: "none", viewValue: "Mức lương" },
    { value: "1", viewValue: "1-3 triệu" },
    { value: "2", viewValue: "3-5 triệu" },
    { value: "3", viewValue: "5-7 triệu" },
    { value: "4", viewValue: "7-10 triệu" },
    { value: "5", viewValue: "10-12 triệu" },
    { value: "6", viewValue: "12-15 triệu" },
    { value: "7", viewValue: "15-20 triệu" },
    { value: "8", viewValue: "20-25 triệu" },
    { value: "9", viewValue: "25-30 triệu" },
    { value: "10", viewValue: "30 triệu trở lên" },
  ];

  // binding data
  listCareer: any;

  listCity: any;

  listJob: any;

  listBannerAds: any;

  listJobNew: any = [];

  countJob: number;

  //  binding search
  // career search
  career: string = '0'

  // city search
  city: string = '0';

  // salary search
  salary: string = 'none';

  // exp search
  exp: string = 'none';

  // typeofwork search
  typeofwork: string = 'none';

  // sex search
  sex: string = 'none';

  // keyword search
  keyword: 'none';

  // pageination
  // page: number = 1;
  // limit: number = 2;

  // pagination
  totalRecords: string;
  page: number = 1;

  // idcandidate: number;

  constructor(private api: ApiService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    // get Career
    this.getCareer();

    // get City
    this.getCity();

    //listHotNew
    this.getJobNew();

    this.activatedRoute.params.subscribe(params => {
      this.career = params['career'];
      this.city = params['city'];
      this.salary = params['salary'];
      this.exp = params['exp'];
      this.typeofwork = params['typeofwork'];
      this.sex = params['sex'];
      this.keyword = params['keyword'] == 'none' ? '' : params['keyword'];

      // on Filter Job
      this.onFilterJob();

      // this.idcandidate = this.api.getCandidateValue.id;
    });



    // get Banner Ads
    this.getBannerAds();
  }

  /**
   * changePage
   * @param page 
   */
  changePage(page) {
    this.page = page;
    this.onFilterJob();
  }

  /**
   * on Filter Job
   */
  onFilterJob() {
    this.countJob = 0;
    const param = {
      career: this.career,
      city: this.city,
      salary: this.salary,
      exp: this.exp,
      typeofwork: this.typeofwork,
      sex: this.sex,
      keyword: this.keyword,
      // offset: (this.page - 1) * this.limit,
      // limit: 2
    }

    this.subscription.push(this.api.excuteAllByWhat(param, '13', true).subscribe(data => {
      // set data for listJob
      if (data.length > 0) {
        // format salary million
        data[0].salary = data[0]['salary'] / 1000000;
        this.listJob = data;

        // get number of job has just been found
        data.forEach(element => {
          this.countJob = this.countJob + 1;
        });

      } else {
        this.listJob = null;
      }
    }))
  }

  /**
  * get Career
  */
  getCareer() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '130', true).subscribe(data => {
      if (data.length > 0) {

        let temp = [
          {
            id: "0",
            name: "Ngành nghề"
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });

        // set data for listCareer	
        this.listCareer = temp;
      } else {
        this.listCareer = 0;
      }
    }));
  }

  /**
  * get City
  */
  getCity() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '200', true).subscribe(data => {
      if (data.length > 0) {

        let temp = [
          {
            id: "0",
            name: "Địa điểm"
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });
        // set data for city	
        this.listCity = temp;
      }
    }))
  }

  /**
  * get Banner Ads
  */
  getBannerAds() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '69', true).subscribe(data => {
      if (data.length > 0) {
        // set data for city	
        this.listBannerAds = data;
      }
    }))
  }

  /**
  * get Job New
  */
  getJobNew() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '18', true).subscribe(data => {
      if (data.length > 0) {
        data.forEach(element => {
          element.salary = element.salary/1000000;
        });
        // set data for list jog hot	
        let colSlide = [];
        let processList = [
          {
            rowSlide: [
              colSlide = [data[0], data[1], data[2], data[3], data[4], data[5]],
            ],
            active: true
          },
          {
            rowSlide: [
              colSlide = [data[6], data[7], data[8], data[9], data[10], data[11]],
            ],
            active: false
          }
        ];

        this.listJobNew = processList;
      }
    }))
  }

  onDetailJobClick(idjob) {
    const url = '/b2-detail-work/' + idjob;
    this.router.navigate([url]);
  }

  /**
   * on Detail Company Click
   * @param idcompany 
   */
  onDetailCompanyClick(idcompany) {
    const url = '/d2-detail-company/' + idcompany;
    this.router.navigate([url]);
  }

}
