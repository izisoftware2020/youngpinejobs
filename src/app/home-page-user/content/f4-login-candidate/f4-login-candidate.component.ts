import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-f4-login-candidate',
  templateUrl: './f4-login-candidate.component.html',
  styleUrls: ['./f4-login-candidate.component.scss']
})
export class F4LoginCandidateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
