import { Component, OnInit } from '@angular/core';
import { Subscription, Observable, Observer  } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-d1-list-company',
  templateUrl: './d1-list-company.component.html',
  styleUrls: ['./d1-list-company.component.scss']
})
export class D1ListCompanyComponent implements OnInit {
  /** for table */
  subscription: Subscription[] = [];

  newestCompanies: any=[];
  mostVistedCompanies: any=[];
  companies: any=[];
  cities: any=[];
  careeres: any=[];
  cityId = '';
  careerId = '';
  companyName:string;

  observable: Observable<any>;  
  observer: Observer<any>;
  count=0;

  // pageination
  page: number = 1;
  limit: number = 1;
  countCompanies: number;
  numberPages: number;
  limitPage =  5;   
  limitPages:any;

  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService,
    private router: Router, private activatedRoute: ActivatedRoute) {
    // add validate for controls
    
    // xử lý bất đồng bộ  
    this.observable = Observable.create((observer: any) => {  
      this.observer = observer; 
    });
  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.observable.subscribe(done=>{
      if(done==4){
        this.activatedRoute.params.subscribe(params => {
          this.companyName = params['keyword'] == 'none' ? '' : params['keyword'];       
          this.careerId = params.career;
          this.cityId = params.city;
        });
        this.onFilterClick();    
      }
    })
    
    this.onLoadDataCity();
    this.onLoadDataCareer();
    this.onLoadNewestCompanyData();
    this.onLoadMostVistedCompanyData();
  }

  /**
   * onLoadDataCity
   */
  onLoadDataCity(){
    const param={};
     this.subscription.push(this.api.excuteAllByWhat(param, '63', true).subscribe(data => {

       if (data.length>0) {
        let temp = [{
          id : '0',
          name : 'Địa điểm'
        }];
        data.forEach(item => {
          temp.push(item);
        })
        this.cities = temp;

         // set first select city combobox
        this.cityId = this.cities[0].id;  
        this.count++;
        this.observer.next(this.count);
      }
     }));
  }

  /**
   * onLoadDataCareer
   */
  onLoadDataCareer(){
    const param={};
     this.subscription.push(this.api.excuteAllByWhat(param, '64', true).subscribe(data => {

       if (data.length>0) {
        let temp = [{
          id : '0',
          name : 'Ngành nghề'
        }];
        data.forEach(item => {
          temp.push(item);
        })
        this.careeres = temp;

         // set first select career combobox
        this.careerId = this.careeres[0].id;  
        
        this.count++;
        this.observer.next(this.count);
      }
     }));
  }

  /**
   * change Page
   * @param page 
   */
  changePage(page) {
      this.page = page;
    this.onFilterClick();
  }

  /**
   * onFillterClick
   */
  onFilterClick(){
    const param={
                  "companyname": this.companyName, 
                  "city": this.cityId, 
                  "career": this.careerId,
                  "offset": (this.page - 1) * this.limit,
                  "limit": this.limit};
     this.subscription.push(this.api.excuteAllByWhat(param, '66', true).subscribe(data => {
      if (data.length > 0) {
        // set data for table
        this.companies = data;
      }
      else {
        // set data for table
        this.companies =[];
      }
     }));  
     this.countNumberOfPageSearchResult(); 
   }

   /**
    * countNumberOfPageSearchResult
    */
   countNumberOfPageSearchResult(){
    this.countCompanies = 0;
    this.numberPages = 0;
    const param={
                  "companyname": this.companyName, 
                  "city": this.cityId, 
                  "career": this.careerId,
                  "offset": (this.page - 1) * this.limit,
                  "limit": this.limit};
     this.subscription.push(this.api.excuteAllByWhat(param, '612', true).subscribe(data => {
      if (data.length > 0) {
        this.countCompanies = data[0].countcompanies;
        if (this.countCompanies % this.limit == 0){
          this.numberPages = this.countCompanies/this.limit;
          this.limitPagesPagination(this.page);
        }
        else{
          this.numberPages = Math.floor(this.countCompanies/this.limit) + 1;
          this.limitPagesPagination(this.page);
        }
      }
     }));
   }

   /**
    * onChangeAfterPageClick
    * @param curpage 
    */
   onChangeAfterPageClick(curpage){
     if (curpage < (this.limitPage * Math.floor(this.numberPages/this.limitPage))){
      if (curpage==1){
        this.page = curpage + this.limitPage;
      }
      else 
        if(curpage%this.limitPage!=0 ){
          if(this.numberPages%this.limitPage==0 && curpage >( this.numberPages - this.limitPage)){
            this.page = this.page
          }
          else{
            this.page = Math.floor(curpage/this.limitPage)*this.limitPage + this.limitPage + 1;
          }
        }
        else
          if(curpage%this.limitPage==0){
            this.page = curpage + 1;
          }
     }
      this.onFilterClick();
   }

     /**
    * onChangeBeforePageClick
    * @param curpage 
    */
   onChangeBeforePageClick(curpage){
    if (curpage > this.limitPage){   
       if(curpage%this.limitPage!=0){
         this.page = Math.floor(curpage/this.limitPage)*this.limitPage - this.limitPage + 1;
       }
       else
         if(curpage%this.limitPage==0 ){
           this.page = curpage - (2*this.limitPage) + 1;
         }
    }
     this.onFilterClick();
  }

  /**
   * limitPagesPagination
   * @param curpage 
   */
  limitPagesPagination(curpage){
    this.limitPages=[];
     if (curpage <= (Math.floor(this.numberPages/this.limitPage)*this.limitPage)){ 
      for (var i = 0; i < this.limitPage; i++)
        {
          if (curpage%this.limitPage==0){
            this.limitPages[i] = i + curpage-this.limitPage+1;
          }
          else {
            this.limitPages[i] = i + Math.floor(curpage/this.limitPage)*this.limitPage+1;
          }
        }
     }
    else{
      for (var i = 0; i < (this.numberPages - Math.floor(this.numberPages/this.limitPage)*this.limitPage); i++)
        {
          if (curpage%this.limitPage==0){
            this.limitPages[i] = i + curpage-this.limitPage+1;
          }
          else {
            this.limitPages[i] = i + Math.floor(curpage/this.limitPage)*this.limitPage+1;
          }
        }
    }
    
  }

   /**
    * onLoadNewestCompanyData
    */
  onLoadNewestCompanyData(){
    const param={};
     this.subscription.push(this.api.excuteAllByWhat(param, '62', true).subscribe(data => {
      if (data.length>0) {
        
        let colSlide = [];
        let processList = [
          {
              rowSlide : [
                colSlide = [data[0]],
                colSlide = [data[1]],
                colSlide = [data[2]],
                colSlide = [data[3]],
            ],
            active: true
          },
          {
            rowSlide : [
                colSlide = [data[4]],
                colSlide = [data[5]],
                colSlide = [data[6]],
                colSlide = [data[7]],
                ],
            active: false
          }
        ];
        this.newestCompanies = processList;
        this.count++;
        this.observer.next(this.count);
      }
     }));
  }

   onLoadMostVistedCompanyData(){
    const param={};
     this.subscription.push(this.api.excuteAllByWhat(param, '67', true).subscribe(data => {
      if (data.length>0) {
        
        let colSlide = [];
        let processList = [
          {
              rowSlide : [
                colSlide = [data[0]],
                colSlide = [data[1]],
                colSlide = [data[2]],
                colSlide = [data[3]],
            ],
            active: true
          },
          {
            rowSlide : [
                colSlide = [data[4]],
                colSlide = [data[5]],
                colSlide = [data[6]],
                colSlide = [data[7]],
                ],
            active: false
          }
        ];
        this.mostVistedCompanies = processList;

        this.count++;
        this.observer.next(this.count);
      }
     }));
  }

  /**
   * on Company Click
   */
  onCompanyClick(id){ 
    const url = '/d2-detail-company/' + id;
    this.router.navigate([url]);
  }

}
