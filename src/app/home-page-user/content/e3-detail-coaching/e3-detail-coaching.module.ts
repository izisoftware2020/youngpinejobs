import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { E3DetailCoachingComponent } from './e3-detail-coaching.component';

@NgModule({
    declarations: [E3DetailCoachingComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: E3DetailCoachingComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class E3DetailCoachingModule { }
