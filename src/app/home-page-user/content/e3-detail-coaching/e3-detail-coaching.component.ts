import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Commentcoaching } from 'src/app/common/models/120commentcoaching.models';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-e3-detail-coaching',
  templateUrl: './e3-detail-coaching.component.html',
  styleUrls: ['./e3-detail-coaching.component.scss']
})
export class E3DetailCoachingComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];

  //data modelinformation
  information = {
    id: '',
    fullname: '',
    born: '',
    sex: '',
    phone: '',
    address: '',
    about: '',
    skill: '',
    avatar: '',
    email: '',
    numberview: ''
  }

  //data model
  packages: [] = [];

  //data model
  commentcoachings: [] = [];

  // idcoaching
  idcoaching: string;

  //commentcandidate
  commentcandidate: string = '';

  // pageination
  page: number = 1;
  totalRec: string;

  // id user login
  candidateId: number;

  constructor(private api: ApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  /**
    * ngOnDestroy
    */
  ngOnDestroy() {
    window.scroll({ left: 0, top: 0});
    
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.idcoaching = params['idcoaching'];
      this.getDataCoaching();
      this.updateViewCoaching();

      // assign candidateId with user login
      this.candidateId = this.api.getCandidateValue.id;
    });


  }

  /**
   * load Data Coaching
   */
  updateViewCoaching() {
    const param = {
      'id': this.idcoaching
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '93', true).subscribe(data => {
      if (data.length > 0) {
      }
    }));
  }

  /**
   * load Data Coaching
   */
  getDataCoaching() {
    const param = {
      'id': this.idcoaching,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '94', true).subscribe(data => {
      if (data.length > 0) {
        this.information = data[0];
        this.getDataPackage();
        this.getDataCommentcoaching();
      }
    }));
  }

  /**
   * load Data Package
   */
  getDataPackage() {
    const param = {
      'idcoaching': this.information.id,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '107', true).subscribe(data => {
      if (data.length > 0) {
        this.packages = data;
      }
    }));

  }

  /**
   * load Data Package
   */
  getDataCommentcoaching() {
    const param = {
      'idcoaching': this.information.id,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '127', true).subscribe(data => {
      if (data.length > 0) {
        this.commentcoachings = data;
        console.log(this.commentcoachings)
      }
    }));

  }

  /**
   * change Page
   * @param page 
   */
  changePage(page) {
    this.page = page;
    this.getDataCommentcoaching();
  }

  /**
   * upload Comments
   */
  uploadComment() {
    const param = {
      'idcandidate': this.candidateId,
      'idcoaching': this.idcoaching,
      'content': this.commentcandidate,
      'star': 5,
      'approve': 0
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '128', true).subscribe(data => {
      if (data) {
        this.api.showSuccess('Đã gửi');
      }
      this.getDataCommentcoaching()
    }));
    this.commentcandidate = '';
  }

  onBookNowClick(idpackage) {
    const url = 'e4-checkout-coaching/' + this.information.id + '/' + idpackage;
    this.router.navigate([url]);
  }

}
