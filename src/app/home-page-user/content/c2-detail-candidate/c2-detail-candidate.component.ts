import { Component, OnInit, Inject } from '@angular/core';
import { Subscription, Observable, Observer } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-c2-detail-candidate',
  templateUrl: './c2-detail-candidate.component.html',
  styleUrls: ['./c2-detail-candidate.component.scss']
})
export class C2DetailCandidateComponent implements OnInit {

  /** for table */
  subscription: Subscription[] = [];

  listBannerAds: any;


  /**
   * constructor
   * @param api 
   */
  constructor(private api: ApiService, private dialog: MatDialog, private router: Router,
    private activatedRoute: ActivatedRoute) {
    // add validate for controls

  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.activatedRoute.params.subscribe(params => {
      this.idcandidate = params['idcandidate'];
      this.onLoadCandidateData();
    });
    this.checkSavedCanidate();

    this.updateViewCandidate();

    // assign idconpany
    // this.api.getCompanyValue();
  }

  candidate = {
    id: 0,
    password: '',
    avatar: '',
    fullname: '',
    born: '',
    country: '',
    city: '',
    provine: '',
    address: '',
    sex: '',
    marriage: '',
    positionapply: '',
    academiclevel: '',
    yearsofexperience: '',
    workingplace: '',
    career: '',
    typeofwork: '',
    level: '',
    salary: '',
    minsalary: '',
    maxsalary: '',
    targetjob: '',
    descriptionstargetjob: '',
    school: '',
    department: '',
    certificate: '',
    specialized: '',
    classification: '',
    startadmission: '',
    endadmission: '',
    additioninfor: '',
    posisionold: '',
    companyold: '',
    startworked: '',
    endworked: '',
    descriptionsoldjob: '',
    descriptionskill: '',
    skill: '',
    language: '',
    infomation: '',
    cityname: '',
    districtname: '',
    numberview: 0,
    workingplacename: '',
    phone: 0,
    email: '',
    point: 0
  };

  idcandidate: number;

  /**
   * on LoadCandidate Data
   */
  onLoadCandidateData() {
    const param = { "id": this.idcandidate };
    this.subscription.push(this.api.excuteAllByWhat(param, '22', true).subscribe(data => {
      if (data.length > 0) {
        data[0].minsalary = data[0]['minsalary'] / 1000000;
        data[0].maxsalary = data[0]['maxsalary'] / 1000000;
        this.candidate = data[0];
      }
    }));
  }

  /**
   * Get Name Gender By Gender Number
   * @param genderNumber 
   */
  convertSexNumberToSexName(genderNumber) {
    return (genderNumber == '0') ? 'Nam' : 'Nữ'
  }

  /**
   * convertMarriageNumberToMarriageName
   * @param marriageNumber 
   */
  convertMarriageNumberToMarriageName(marriageNumber) {
    return (marriageNumber == '0') ? 'Độc thân' : 'Đã kết hôn'
  }

  /**
   * convert Type Of Work Number To Type Of Work Name
   * @param typeOfWorkNumber 
   */
  convertTOWNumberToTOWName(typeOfWorkNumber) {
    switch (typeOfWorkNumber) {

      case '1': {
        return 'Toàn thời gian cố định';
        break;
      }

      case '2': {
        return 'Bán thời gian cố định';
        break;
      }

      case '3': {
        return 'Toàn thời gian tạm thời';
        break;
      }

      case '4': {
        return 'Bán thòi gian tạm thời';
        break;
      }

      case '5': {
        return 'Theo hợp đồng / tư vấn';
        break;
      }

      case '6': {
        return 'Thực tập';
        break;
      }

      case '7': {
        return 'Khác';
        break;
      }

    }
  }

  /**
   * convertExpYeasNumberToExpYeasName
   * @param expYears 
   */
  convertExpYeasNumberToExpYeasName(expYears) {
    switch (expYears) {
      case '1': {
        return 'Chưa có kinh nghiệm';
        break;
      }

      case '2': {
        return 'Dưới 1 năm';
        break;
      }

      case '3': {
        return '1 năm';
        break;
      }

      case '4': {
        return '2 năm';
        break;
      }

      case '5': {
        return '3 năm';
        break;
      }

      case '6': {
        return '4 năm';
        break;
      }

      case '7': {
        return '5 năm';
        break;
      }

      case '8': {
        return 'Trên 5 năm';
        break;
      }

    }
  }

  /**
  * get Banner Ads
  */
  getBannerAds() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '69', true).subscribe(data => {
      if (data.length > 0) {
        // set data for listBannerAds	
        this.listBannerAds = data;

      }
    }))
  }

  candidatesave: any;
  isSaved;

  checkSavedCanidate() {
    const param = {
      idcandidate: this.idcandidate,
      idcompany: 47,
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '710', true).subscribe(data => {
      if(data.length > 0){
        this.isSaved=true; 
      }
      else {
        this.isSaved = false;
      }
    }));
  }


  saveOrDeleteProfile() {
    const param = {
      idcandidate: this.idcandidate,
      idcompany: 47,
    }

    if (this.isSaved == true) {
      this.subscription.push(this.api.excuteAllByWhat(param, '79', true).subscribe(data => {
        this.isSaved = false;
      }));
    }
    else {
      this.subscription.push(this.api.excuteAllByWhat(param, '78', true).subscribe(data => {
        this.isSaved = true
        if (data.length > 0)
          this.candidatesave = data;
      }));
    }
  }

  companyId = 1;
  
  /**
   * update Point Company when view information candidate
   */
  onUpdadeNumberViewCompany() {
    const param = { "companyid": this.companyId, "pointcandidate": this.candidate.point };
    this.api.excuteAllByWhat(param, '610', true).subscribe(data => {
    });
  }

  updateViewCandidate() {
    const param = {
      'id': this.idcandidate
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '26', true).subscribe(data => {
      if (data.length > 0) {
      }
    }));
  }

}

