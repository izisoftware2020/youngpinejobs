import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-f5-register-candidate',
  templateUrl: './f5-register-candidate.component.html',
  styleUrls: ['./f5-register-candidate.component.scss']
})
export class F5RegisterCandidateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
