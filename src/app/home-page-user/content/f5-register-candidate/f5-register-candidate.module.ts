import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { F5RegisterCandidateComponent } from './f5-register-candidate.component';

@NgModule({
    declarations: [F5RegisterCandidateComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: F5RegisterCandidateComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class F5RegisterCandidateModule { }
