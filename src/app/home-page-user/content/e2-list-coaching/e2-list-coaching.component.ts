import { Coaching } from './../../../common/models/90coaching.models';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-e2-list-coaching',
  templateUrl: './e2-list-coaching.component.html',
  styleUrls: ['./e2-list-coaching.component.scss']
})
export class E2ListCoachingComponent implements OnInit, OnDestroy {

  /** for table */
  subscription: Subscription[] = [];

  //data package
  package: any[] = [];

  //data skill
  skill: any[] = [];

  //data skill
  coaching: any[] = [];

  //set data packageId
  packageId: string = '';

  //set data coachingNameId
  coachingNameId: string = '';

  //set data skillId
  skillId: string = '';

  // pageination
  page: number = 1; 
  totalRec: string;

  constructor(private api: ApiService, private router: Router) {

  }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.getDataPackage();
    this.getDataSkill();
    this.onFillterClick();
  }

  /**
   * get DataNameCoaching
   */
  getDataPackage() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '100', true).subscribe(data => {

      //add value Website
      if (data.length > 0) {
        let temp = [
          {
            id: "0",
            name: "Gói dịch vụ"
          }
        ];

        data.forEach(item => {
          temp.push(item);
        });

        this.package = temp;
        // set first select websites combobox
        this.packageId = this.package[0].id;
      }
    }));
  }

  /**
   * get DataNameCoaching
   */
  getDataSkill() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '150', true).subscribe(data => {

      //add value Website
      if (data.length > 0) {
        let temp = [
          {
            id: "0",
            name: "Ngành nghề"
          }
        ];

        data.forEach(item => {
          temp.push(item);
        });

        this.skill = temp;
        // set first select websites combobox
        this.skillId = this.skill[0].id;
      }
    }));
  }

  /**
   * onFillterClick
   */
  onFillterClick() {
    const param = {
      "fullname": this.coachingNameId,
      "skill": this.skillId,
      "idpackage": this.packageId, 
    };
    this.api.excuteAllByWhat(param, '97', true).subscribe(data => {
      if (data.length > 0) {
        this.coaching = data; 
      }else{
        this.coaching = [];
      } 
    })
  }

  /**
   * change Page
   * @param page 
   */
  changePage(page) {
    this.page = page;
    this.onFillterClick();
  }

  /**
   * onBookNowClick
   * @param idcoaching 
   */
  onBookNowClick(idcoaching) {
    const url = '/e3-detail-coaching/' + idcoaching;
    this.router.navigate([url]);
  }

}
