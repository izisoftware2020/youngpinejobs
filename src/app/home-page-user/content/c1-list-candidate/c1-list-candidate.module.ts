import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { C1ListCandidateComponent } from './c1-list-candidate.component';

@NgModule({
    declarations: [C1ListCandidateComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        
        RouterModule.forChild([
            {
                path: '',
                component: C1ListCandidateComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class C1ListCandidateModule { }
