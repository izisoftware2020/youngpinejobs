import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { D2DetailCompanyComponent } from './d2-detail-company.component';

@NgModule({
    declarations: [D2DetailCompanyComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: D2DetailCompanyComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class D2DetailCompanyModule { }
