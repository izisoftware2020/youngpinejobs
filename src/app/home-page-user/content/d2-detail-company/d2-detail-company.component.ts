import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable, Observer  } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
  selector: 'app-d2-detail-company',
  templateUrl: './d2-detail-company.component.html',
  styleUrls: ['./d2-detail-company.component.scss']
})
export class D2DetailCompanyComponent implements OnInit {
  /** for table */
  subscription: Subscription[] = [];

  constructor(private activatedRoute: ActivatedRoute,
    private api: ApiService) { }

  selectedId;
  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.activatedRoute.params.subscribe(param=>{
      this.selectedId = param['idcompany'];
      
    });
    this.onUpdadeNumberViewCompany();
    this.onLoadDetailCompany();
    // <div [innerHTML]="theHtmlString"></div>
  }

  /**
   * onUpdadeNumberViewCompany
   */
  onUpdadeNumberViewCompany(){
    const param = {"companyid" : this.selectedId};
    this.api.excuteAllByWhat(param, '65', true).subscribe(data => {
    });
  }

  company : any=[];
  onLoadDetailCompany(){
    const param = {"companyid" : this.selectedId};
    this.subscription.push(this.api.excuteAllByWhat(param, '68', true).subscribe(data => {
      if (data.length > 0){
        this.company = data[0];
      }
    }));
  }
}
