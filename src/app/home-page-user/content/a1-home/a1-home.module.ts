import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { A1HomeComponent } from './a1-home.component';

@NgModule({
    declarations: [A1HomeComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: A1HomeComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class A1HomeModule { }
