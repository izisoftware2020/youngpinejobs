import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-a1-home',
  templateUrl: './a1-home.component.html',
  styleUrls: ['./a1-home.component.scss']
})
export class A1HomeComponent implements OnInit {

  subscription: Subscription[] = [];

  // binding data
  listAdsCompany: any;

  totalJob: any;

  totalCompany: any;

  totalApply: any;

  totalCandidate: any;

  listCareer: any;

  listCity: any;

  listHotJob: any;

  listJobAds: any;

  expYears: any[] = [
    { value: 'none', viewValue: 'Tất cả kinh nghiệm' },
    { value: '1', viewValue: 'Chưa có kinh nghiệp' },
    { value: '2', viewValue: 'Dưới 1 năm' },
    { value: '3', viewValue: '1 năm' },
    { value: '4', viewValue: '2 năm' },
    { value: '5', viewValue: '3 năm' },
  ];

  typeOfWork: any[] = [
    { value: 'none', viewValue: 'Loại hình công việc' },
    { value: '1', viewValue: 'Thực tập ' },
    { value: '2', viewValue: 'Toàn thời gian cố định' },
    { value: '3', viewValue: 'Toàn thời gian tạm thời' },
    { value: '4', viewValue: 'Bán thời gian cố định' },
    { value: '5', viewValue: 'Bán thời gian tạm thời' },
    { value: '6', viewValue: 'Hợp đồng' },
  ];

  sexs: any[] = [
    { value: '0', viewValue: 'Nam' },
    { value: '1', viewValue: 'Nữ' },
    { value: 'none', viewValue: 'Giới tính' }
  ];

  salaries: any[] = [
    { value: 'none', viewValue: 'Mức lương' },
    { value: '1', viewValue: '1-3 triệu' },
    { value: '2', viewValue: '3-5 triệu' },
    { value: '3', viewValue: '5-7 triệu' },
    { value: '4', viewValue: '7-10 triệu' },
    { value: '5', viewValue: '10-12 triệu' },
    { value: '6', viewValue: '12-15 triệu' },
    { value: '7', viewValue: '15-20 triệu' },
    { value: '8', viewValue: '20-25 triệu' },
    { value: '9', viewValue: '25-30 triệu' },
    { value: '10', viewValue: '30 triệu trở lên' },
  ];

  // career search
  career: string = '0';

  // city search
  city: string = '0';

  // salary search
  salary: string = 'none';

  // exp search
  exp: string = 'none';

  // typeofwork search
  typeofwork: string = 'none';

  // sex search
  sex: string = 'none';

  // keyword search
  keyword: string = '';

  constructor(private api: ApiService, private router: Router) { }


  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    //load data get Ads Company
    this.getAdsCompany();

    // count Jobs
    this.countJobs();

    // count Company
    this.countCompany();

    // count Apply
    this.countApply();

    //  count Candidate
    this.countCandidate();

    // get Career
    this.getCareer();

    // get City
    this.getCity();

    this.getJobHot();

    this.getJobAds();
  }

  onSearchJobClick() {
    this.keyword = this.keyword=='' ? 'none': this.keyword;
    const url = 'b1-list-work/' + this.career + '/' + this.city + '/' + this.salary + '/' + this.exp +
      '/' + this.typeofwork + '/' + this.sex + '/' + this.keyword;
    this.router.navigate([url]);

    // $('.close-find-advance').click();
  }

  onSearchCandidateClick() {
    this.keyword = this.keyword=='' ? 'none': this.keyword;
    const url = 'c1-list-candidate/' + this.career + '/' + this.city + '/' + this.salary + '/' + this.exp +
      '/' + this.typeofwork + '/' + this.sex + '/' + this.keyword;
    this.router.navigate([url]);
  }

  onSearchCompanyClick() {
    this.keyword = this.keyword=='' ? 'none': this.keyword;
    const url = 'd1-list-company/' + this.career + '/' + this.city + '/' + this.keyword;
    this.router.navigate([url]);
  }

  /**
   * get Ads Company
   */
  getAdsCompany() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '60', true).subscribe(data => {
      if (data.length > 0) {
        this.listAdsCompany = data;
      } else {
        this.listAdsCompany = null;
      }
    }));
  }

  /**
   * count jobs
   */
  countJobs() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '11', true).subscribe(data => {
      if (data.length > 0) {
        // assign totalJob
        this.totalJob = data[0]['COUNT(1)'];
      } else {
        this.totalJob = 0;
      }
    }));
  }

  /**
   * count Company
   */
  countCompany() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '61', true).subscribe(data => {
      if (data.length > 0) {
        // assign totalCompany
        this.totalCompany = data[0]['COUNT(1)'];
      } else {
        this.totalCompany = 0;
      }
    }));
  }

  /**
   * count Apply
   */
  countApply() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '30', true).subscribe(data => {
      if (data.length > 0) {
        // assign totalApply
        this.totalApply = data[0]['COUNT(1)'];
      } else {
        this.totalApply = 0;
      }
    }));
  }

  /**
   * count candidate
   */
  countCandidate() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '21', true).subscribe(data => {
      if (data.length > 0) {
        // assign totalJob
        this.totalCandidate = data[0]['COUNT(1)'];
      } else {
        this.totalCandidate = 0;
      }
    }));
  }

  /**
   * get Career
   */
  getCareer() {
    const param = {};
    this.subscription.push(this.api.excuteAllByWhat(param, '130', true).subscribe(data => {
      if (data.length > 0) {

        let temp = [
          {
            id: '0',
            name: 'Tất cả ngành nghề'
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });

        // set data for listCareer	
        this.listCareer = temp;
      } else {
        this.listCareer = 0;
      }
    }));
  }

  /**
  * get City
  */
  getCity() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '200', true).subscribe(data => {
      if (data.length > 0) {

        let temp = [
          {
            id: '0',
            name: 'Tất cả địa điểm'
          }
        ]
        data.forEach(item => {
          temp.push(item);
        });
        // set data for city	
        this.listCity = temp;
      }
    }))
  }

  /**
  * get JobHot
  */
  getJobHot() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '17', true).subscribe(data => {
      if (data.length > 0) {
        // set data for list jog hot	
        data.forEach(element => {
          element.salary = element.salary/1000000;
        });
        let colSlide = [];
        let processList = [
          {
            rowSlide: [
              colSlide = [data[0], data[1], data[2], data[3],],
              colSlide = [data[4], data[5], data[6], data[7]],
            ],
            active: true
          },
          {
            rowSlide: [
              colSlide = [data[8], data[9], data[10], data[11],],
              colSlide = [data[12], data[13], data[14], data[15]],
            ],
            active: false
          }
        ];

        this.listHotJob = processList;
        // console.log(this.listHotJob);
        
      }
    }))
  }

  /**
  * get Job Ads
  */
  getJobAds() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '14', true).subscribe(data => {
      if (data.length > 0) {
        
        // set data for list jog ads	
        let colSlide = [];
        let processList = [
          {
            rowSlide: [
              colSlide = [data[0], data[1], data[2], data[3],],
              colSlide = [data[4], data[5], data[6], data[7]],
            ],
            active: true
          },
          {
            rowSlide: [
              colSlide = [data[8], data[9], data[10], data[11],],
              colSlide = [data[12], data[13], data[14], data[15]],
            ],
            active: false
          }
        ]

        this.listJobAds = processList;
        
      }
    }))
  }

  /**
   * on Detail Company Click
   * @param idcompany 
   */
  onDetailCompanyClick(idcompany) {
    const url = '/d2-detail-company/' + idcompany;
    this.router.navigate([url]);
  }

  /**
   * on Detail Candidate Click
   * @param idjob
   */
  onDetailJobClick(idjob) {
    const url = '/b2-detail-work/' + idjob;
    this.router.navigate([url]);
  }

}
