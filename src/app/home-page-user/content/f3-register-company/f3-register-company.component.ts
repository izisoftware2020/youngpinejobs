import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
  selector: 'app-f3-register-company',
  templateUrl: './f3-register-company.component.html',
  styleUrls: ['./f3-register-company.component.scss']
})
export class F3RegisterCompanyComponent implements OnInit,OnDestroy {

  subscription: Subscription[] = [];

   // data source for combobox level
   personalscales: any[] = [
    { value: '0', viewValue: 'Quy mô công ty' },
    { value: '1', viewValue: 'Dưới 20 người' },
    { value: '2', viewValue: '20 - 150 người' },
    { value: '3', viewValue: '150 - 300 người' },
    { value: '4', viewValue: 'Trên 300 người' }, 
  ];

  // website
  cityId: string = '';

  //data combobox city
  citys: any[] = [];

  constructor(private api: ApiService) { }

  /**
    * ngOnDestroy
    */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.loadDataCity();
  }

  /**
   * load Data City
   */
  loadDataCity() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '147', true).subscribe(data => {

      if (data.length > 0) {
        let temp = [
          {
            id: "0",
            name: "Tỉnh/Thành phố"
          }
        ];

        data.forEach(item => {
          temp.push(item);
        });

        this.citys = temp;
        // set first select citys combobox
        this.cityId = this.citys[0].id; 
      
      }
      console.log('city', this.citys)
    }));

  }

}
