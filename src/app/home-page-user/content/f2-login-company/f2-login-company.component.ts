import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-f2-login-company',
  templateUrl: './f2-login-company.component.html',
  styleUrls: ['./f2-login-company.component.scss']
})
export class F2LoginCompanyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
