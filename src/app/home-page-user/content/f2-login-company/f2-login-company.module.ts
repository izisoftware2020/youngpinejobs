import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { F2LoginCompanyComponent } from './f2-login-company.component';

@NgModule({
    declarations: [F2LoginCompanyComponent],
    imports: [
        TransferHttpCacheModule,
        CommonModule,

        RouterModule.forChild([
            {
                path: '',
                component: F2LoginCompanyComponent,
                children: []
            }
        ]),
        FormsModule,
        ReactiveFormsModule,
    ]
})

export class F2LoginCompanyModule { }
