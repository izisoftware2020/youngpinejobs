import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-b2-detail-work',
  templateUrl: './b2-detail-work.component.html',
  styleUrls: ['./b2-detail-work.component.scss']
})
export class B2DetailWorkComponent implements OnInit {

  /** for table */
  subscription: Subscription[] = [];


  constructor(private api: ApiService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
    
    this.activatedRoute.params.subscribe(params => {
      this.idjob = params['idjob'];
    });

    this.onLoadDataJob();

    this.onLoadTotalJobOfCompany();
    this.checkSavedWork();
    this.checkApplyWork();
    this.updateViewJob();
  }

  job: any = [];

  totalJobOfCompany: any = [];

  idjob: number;

  applywork: any = [];

  /**
   * on Load Data Job
   */
  onLoadDataJob() {
    const param = { "id": this.idjob };
    this.subscription.push(this.api.excuteAllByWhat(param, '15', true).subscribe(data => {
      if (data.length > 0) {

        this.job = data[0];
        this.job.salary = (this.job.salary) / 1000000;
        console.log(this.job);
      }
    }));
  }

  /**
   * on LoadTotal Job Of Company
   */
  onLoadTotalJobOfCompany() {
    const param = { "id": this.idjob };
    this.subscription.push(this.api.excuteAllByWhat(param, '16', true).subscribe(data => {
      if (data.length > 0) {
        this.totalJobOfCompany = data[0]['totaljob'];
      }
    }));
  }

  /**
   * Get Name Gender By Gender Number
   * @param genderNumber 
   */
  convertSexNumberToSexName(genderNumber) {
    switch (genderNumber) {

      case '0': {
        return 'Nam';
      }

      case '1': {
        return 'Nữ';
      }

      case '3': {
        return 'Không yêu cầu';
      }
    }
  }

  /**
   * convert Type Of Work Number To Type Of Work Name
   * @param typeOfWorkNumber 
   */
  convertTOWNumberToTOWName(typeOfWorkNumber) {
    switch (typeOfWorkNumber) {

      case '1': {
        return 'Toàn thời gian cố định';
      }

      case '2': {
        return 'Bán thời gian cố định';
      }

      case '3': {
        return 'Toàn thời gian tạm thời';
      }

      case '4': {
        return 'Bán thòi gian tạm thời';
      }

      case '5': {
        return 'Theo hợp đồng / tư vấn';
      }

      case '6': {
        return 'Thực tập';
      }

      case '7': {
        return 'Khác';
      }

    }
  }

  /**
   * convertExpYeasNumberToExpYeasName
   * @param expYears 
   */
  convertExpYeasNumberToExpYeasName(expYears) {
    switch (expYears) {
      case '1': {
        return 'Chưa có kinh nghiệm';
      }

      case '2': {
        return 'Dưới 1 năm';
      }

      case '3': {
        return '1 năm';
      }

      case '4': {
        return '2 năm';
      }

      case '5': {
        return '3 năm';
      }

      case '6': {
        return '4 năm';
      }

      case '7': {
        return '5 năm';
      }

      case '8': {
        return 'Trên 5 năm';
      }

    }
  }
  applysave: any;
  isSaved;

  /**
   * check Saved Work
   */
  checkSavedWork() {
    const param = {
      idjobs: this.idjob,
      idcandidate: 47,
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '410', true).subscribe(data => {
      if (data.length > 0) {
        this.isSaved = true;
        console.log("true")
      }
      else {
        this.isSaved = false;
        console.log("false")
      }
    }));
  }

  

  /**
   * save Or Delet eWork
   */
  saveOrDeleteWork() {
    const param = {
      idjobs: this.idjob,
      idcandidate: 47,
    }
    if (this.isSaved == true) {
      this.subscription.push(this.api.excuteAllByWhat(param, '49', true).subscribe(data => {
        this.isSaved = false;
      }));
    }
    else {
      this.subscription.push(this.api.excuteAllByWhat(param, '48', true).subscribe(data => {
        this.isSaved = true
        if (data.length > 0)
          this.applysave = data;
      }));
    }
  }
  
  isApllied;

  /**
   * check Apply Work
   */
  checkApplyWork() {
    const param = {
      idjobs: this.idjob,
      idcandidate: 47,
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '37', true).subscribe(data => {
      if (data.length > 0) {
        this.isApllied = true;
        console.log("true")
      }
      else {
        this.isApllied = false;
        console.log("false")
      }
    }));
  }

  /**
   * apply Or Unapply Work
   */
  applyOrUnapplyWork() {
    const param = {
      idjobs: this.idjob,
      idcandidate: 47,
    }
    if (this.isApllied == true) {
      this.subscription.push(this.api.excuteAllByWhat(param, '36', true).subscribe(data => {
        this.isApllied = false;
      }));
    }
    else {
      this.subscription.push(this.api.excuteAllByWhat(param, '35', true).subscribe(data => {
        this.isApllied = true
        if (data.length > 0)
          this.applywork = data;
      }));
    }
  }

  /**
   * update View Job
   */
  updateViewJob() {
    const param = {
      'id': this.idjob
    }
    this.subscription.push(this.api.excuteAllByWhat(param, '19', true).subscribe(data => {
      if (data.length > 0) {
      }
    }));
  }

}
