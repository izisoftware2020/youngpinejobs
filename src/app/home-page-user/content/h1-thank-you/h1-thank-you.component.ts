import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-h1-thank-you',
  templateUrl: './h1-thank-you.component.html',
  styleUrls: ['./h1-thank-you.component.scss']
})
export class H1ThankYouComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  }

}
