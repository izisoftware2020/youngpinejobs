import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { H1ThankYouComponent } from './h1-thank-you.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: H1ThankYouComponent,
        children: []
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class H1ThankYouModule { }
