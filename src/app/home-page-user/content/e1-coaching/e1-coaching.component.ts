import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-e1-coaching',
  templateUrl: './e1-coaching.component.html',
  styleUrls: ['./e1-coaching.component.scss']
})
export class E1CoachingComponent implements OnInit, OnDestroy {
  /** for table */
  subscription: Subscription[] = [];

  //data Step
  stepA = 'How can a career coach partner with you?';
  stepB = 'What is your price range?';
  stepC = 'What is your current career stage?';
  stepD = 'Are you targeting a specific industry?';
  stepE = 'Are you a new manager or leader?';

  //data suggest
  suggest = '';  

  // add class active
  activeA = true;
  activeB = false;
  activeC = false;
  activeD = false;
  activeE = false;

  constructor(private router: Router,private api: ApiService,private route: ActivatedRoute) { } 
   
  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  ngOnInit(): void {
    window.scroll({ left: 0, top: 0});
  } 

  //onStepA
  onStepA(input) {
    this.suggest = this.stepA + ':' + input;
    this.activeA = false;
    this.activeB = true;
  }

  //onStepB
  onStepB(input) {
    this.suggest += '|' + this.stepB + ':' + input;
    this.activeB = false;
    this.activeC = true;
  }

  //onStepC
  onStepC(input) {
    this.suggest += '|' + this.stepC + ':' + input;
    this.activeC = false;
    this.activeD = true;

  }

  //onStepD
  onStepD(input) {
    this.suggest += '|' + this.stepD + ':' + input;
    this.activeD = false;
    this.activeE = true; 
  }

  //ButtonBack B
  onButtonBackB(){
    this.activeA = true;
    this.activeB = false;
  }

  //ButtonBack C
  onButtonBackC(){
    this.activeB = true;
    this.activeC = false;
  }

  //ButtonBack B
  onButtonBackD(){
    this.activeC = true;
    this.activeD = false;
  }

  //ButtonBack E
  onButtonBackE(){
    this.activeD = true;
    this.activeE = false;
  }

  //onStepE
  onStepE(input) {
    this.suggest += '|' + this.stepE + ':' + input; 

    //save suggest 
    localStorage.removeItem('suggest');
    localStorage.setItem('suggest', this.suggest);

    const url = '/e2-list-coaching';
    this.router.navigate([url]);  
    window.scroll({ top:0 , behavior: 'smooth' });
  }

  onScrollTab() {
    setTimeout(() => {
      window.scroll({ left: 0, top:600 , behavior: 'smooth' });
    }, 100);
  }
}
