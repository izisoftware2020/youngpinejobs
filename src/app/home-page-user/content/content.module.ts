import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ApiService } from '../../common/api-service/api.service';
import { H1ThankYouModule } from './h1-thank-you/h1-thank-you.module';
import { H1ThankYouComponent } from './h1-thank-you/h1-thank-you.component';


@NgModule({
    declarations: [
        ContentComponent,
    
    ],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ContentComponent,
                children: [
                    {
                        path: '', 
                        loadChildren: () =>
                            import('./a1-home/a1-home.module').then(m => m.A1HomeModule)
                    },
                    {
                        path: 'a1-home', 
                        loadChildren: () =>
                            import('./a1-home/a1-home.module').then(m => m.A1HomeModule)
                    },
                    {
                        path: 'b1-list-work/:career/:city/:salary/:exp/:typeofwork/:sex/:keyword',
                        loadChildren: () =>
                            import('./b1-list-work/b1-list-work.module').then(m => m.B1ListWorkModule)
                    },
                    {
                        path: 'b2-detail-work/:idjob',
                        loadChildren: () =>
                            import('./b2-detail-work/b2-detail-work.module').then(m => m.B2DetailWorkModule)
                    },
                    {
                        path: 'c1-list-candidate/:career/:city/:salary/:exp/:typeofwork/:sex/:keyword',
                        loadChildren: () =>
                            import('./c1-list-candidate/c1-list-candidate.module').then(m => m.C1ListCandidateModule)
                    },
                    {
                        path: 'c2-detail-candidate/:idcandidate',
                        loadChildren: () =>
                            import('./c2-detail-candidate/c2-detail-candidate.module').then(m => m.C2DetailCandidateModule)
                    },
                    {
                        path: 'd1-list-company/:career/:city/:keyword',
                        loadChildren: () =>
                            import('./d1-list-company/d1-list-company.module').then(m => m.D1ListCompanyModule)
                    },
                    {
                        path: 'd2-detail-company/:idcompany',
                        loadChildren: () =>
                            import('./d2-detail-company/d2-detail-company.module').then(m => m.D2DetailCompanyModule)
                    },
                    {
                        path: 'e1-coaching',
                        loadChildren: () =>
                            import('./e1-coaching/e1-coaching.module').then(m => m.E1CoachingModule)
                    },
                    {
                        path: 'e2-list-coaching',
                        loadChildren: () =>
                            import('./e2-list-coaching/e2-list-coaching.module').then(m => m.E2ListCoachingModule)
                    },
                    {
                        path: 'e3-detail-coaching/:idcoaching',
                        loadChildren: () =>
                            import('./e3-detail-coaching/e3-detail-coaching.module').then(m => m.E3DetailCoachingModule)
                    },
                    {
                        path: 'e4-checkout-coaching/:idcoaching/:idpackage',
                        loadChildren: () =>
                            import('./e4-checkout-coaching/e4-checkout-coaching.module').then(m => m.E4CheckoutCoachingModule)
                    },
                    {
                        path: 'f1-company',
                        loadChildren: () =>
                            import('./f1-company/f1-company.module').then(m => m.F1CompanyModule)
                    },
                    {
                        path: 'f2-login-company',
                        loadChildren: () =>
                            import('./f2-login-company/f2-login-company.module').then(m => m.F2LoginCompanyModule)
                    },
                    {
                        path: 'f3-register-company',
                        loadChildren: () =>
                            import('./f3-register-company/f3-register-company.module').then(m => m.F3RegisterCompanyModule)
                    },
                    {
                        path: 'f4-login-candidate',
                        loadChildren: () =>
                            import('./f4-login-candidate/f4-login-candidate.module').then(m => m.F4LoginCandidateModule)
                    },
                    {
                        path: 'f5-register-candidate',
                        loadChildren: () =>
                            import('./f5-register-candidate/f5-register-candidate.module').then(m => m.F5RegisterCandidateModule)
                    },
                    {
                        path: 'g1-contact',
                        loadChildren: () =>
                            import('./g1-contact/g1-contact.module').then(m => m.G1ContactModule)
                    },
                    {
                        path: 'h1-thank-you',
                        loadChildren: () =>
                            import('./h1-thank-you/h1-thank-you.module').then(m => m.H1ThankYouModule)
                    },
                ]
            }
        ]),
        
    ],
    providers: [ApiService],
    entryComponents: []
})
export class ContentModule { }
