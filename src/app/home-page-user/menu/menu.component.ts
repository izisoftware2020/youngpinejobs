import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Router } from '@angular/router';
import $ from "jquery";
import { Subscription, Observable, Observer } from 'rxjs';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
    subscription: Subscription[] = [];

    // public topMenus: any[] = [];
    // public mainMenus: any[] = [];
    // public banners: any[] = [];
    // public notifications: any[] = [];
    // public events: any[] = [];
    // public associations: any[] = [];
    // isChangeRoute: boolean = true;

    // language: string;

    // observable
    observable: Observable<any>;
    observer: Observer<any>;

    /**
     * constructor
     * @param api 
     */
    constructor(public api: ApiService,
        private router: Router) {

        // xử lý bất đồng bộ	
        this.observable = Observable.create((observer: any) => {
            this.observer = observer;
        });

    }

    /**
     * ngOnInit
     */
    ngOnInit() {
        // // load language
        // this.language = this.api.getLanguage();

        // // first load
        // this.subscription.push(this.api.sendDataService.language$.subscribe(data => {
        //     // load data menu
        //     this.loadDataMenu();

        //     // load data banner
        //     this.loadDataBanner();

        //     // load data news
        //     this.loadDataNews();
        // }));

        // // animation for change page
        // this.animationChangePage();


        // this.hideOrHiddenButtonToTop();
    }

    /**
     * ngAfterViewInit
     */
    ngAfterViewInit() {
        // // process click of menu response
        // this.observable.subscribe(data => {
        //     if (data) {
        //         $(document).ready(function () {
        //             $(".icon-menu-responsive").click(function () {
        //                 $(".respon-menu").slideToggle();
        //                 return false;
        //             });

        //             $(".menu-slide").click(function () {
        //                 // $(".respon-menu>li>ul").slideUp();
        //                 var target = $(this)
        //                     .parent()
        //                     .children(".slideContent");

        //                 $(target).slideToggle();
        //                 return false;
        //             });
        //         });
        //     }
        // });
    }

    /**
     * ngOnDestroy
     */
    ngOnDestroy() {
        this.subscription.forEach(item => {
            item.unsubscribe();
        });
    }

    // /**
    //  * loadDataMenu
    //  */
    // loadDataMenu() {
    //     // init array
    //     this.topMenus = [];
    //     this.mainMenus = [];

    //     // load data top menu
    //     const param = {
    //         'siteid': 1,
    //         'position': 1,
    //         'lang': this.api.getLanguage()
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param, '10', true).subscribe(data => {
    //         if (data && data.length > 0) {
    //             this.topMenus = data;
    //         } else {
    //             this.topMenus = [];
    //         }
    //     }));

    //     // load data main menu
    //     const param2 = {
    //         'siteid': 1,
    //         'position': 2,
    //         'lang': this.api.getLanguage()
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param2, '11', true).subscribe(data => {
    //         if (data && data.length > 0) {
    //             this.mainMenus = data;
    //             // load child main data
    //             this.subscription.push(this.api.excuteAllByWhat(param2, '12', true).subscribe(data1 => {
    //                 if (data1 && data1.length > 0) {
    //                     // add child menu to mainMenus
    //                     this.mainMenus.forEach(item => {
    //                         item.childs = [];
    //                         data1.forEach(ele => {
    //                             if (item.id == ele.ptypeid) {
    //                                 item.childs.push(ele);
    //                             }
    //                         });
    //                     });

    //                     // data is loading finish
    //                     this.observer.next(true);
    //                 }
    //             }));
    //         } else {
    //             this.mainMenus = [];
    //         }
    //     }));
    // }

    // /**
    //  * load Data Banner
    //  */
    // loadDataBanner() {
    //     // load data banner
    //     const param = {
    //         'siteid': 1,
    //         'lang': this.api.getLanguage()
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param, '80', true).subscribe(data => {
    //         if (data) {
    //             this.banners = data;
    //             this.banners.forEach((element, index) => {
    //                 if (index == 0) {
    //                     element.style = 'carousel-item active';
    //                 } else {
    //                     element.style = 'carousel-item';
    //                 }
    //                 element.pathimage = 'http://hce.edu.vn' + element.pathimage;
    //             });
    //         }
    //     }));
    // }

    // /**
    //  * load Data news
    //  */
    // loadDataNews() {
    //     // load data notification
    //     const param = {
    //         'siteid': 1,
    //         'lang': this.api.getLanguage(),
    //         'typeid': 63, // 63 là thông báo
    //         'offset': 0,
    //         'limit': 10,
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param, '100', true).subscribe(data => {
    //         if (data && data.length > 0) {
    //             this.notifications = data;
    //         } else {
    //             this.notifications = [];
    //         }
    //     }));

    //     // load data event
    //     const param1 = {
    //         'siteid': 1,
    //         'lang': this.api.getLanguage(),
    //         'typeid': 64, // 64 là sự kiện hoạt động
    //         'offset': 0,
    //         'limit': 10,
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param1, '100', true).subscribe(data => {
    //         if (data && data.length > 0) {
    //             this.events = data;
    //         } else {
    //             this.events = [];
    //         }
    //     }));

    //     // load data associations
    //     const param2 = {
    //         'siteid': 1,
    //         'lang': this.api.getLanguage(),
    //         'typeid': 783, // 783 là Đoàn - Hội
    //         'offset': 0,
    //         'limit': 10,
    //     };
    //     this.subscription.push(this.api.excuteAllByWhat(param2, '100', true).subscribe(data => {
    //         if (data && data.length > 0) {
    //             this.associations = data;
    //         } else {
    //             this.associations = [];
    //         }
    //     }));
    // }

    // /**
    //  * on Menu Item Click
    //  * @param item 
    //  */
    // onMenuItemClick(item) {
    //     console.log(item);

    //     switch (item.keyword) {
    //         case 'home':
    //             // go to home
    //             this.router.navigate(['/']);
    //             break;

    //         case 'feedback':
    //             // go to feedback
    //             this.router.navigate(['/feedback'], { skipLocationChange: true });
    //             break;

    //         case 'search':
    //             // go to search
    //             this.router.navigate(['/search'], { skipLocationChange: true });
    //             break;

    //         case 'sitemap':
    //             // go to sitemap
    //             this.router.navigate(['/sitemap'], { skipLocationChange: true });
    //             break;

    //         case 'contents':
    //             // go to contents
    //             // check is parent or child
    //             if (item.ptypeid == 0) {
    //                 this.router.navigate(['/list/' + item.keyword + '/' + item.title + '/' + item.id], { skipLocationChange: true });
    //             } else {
    //                 this.router.navigate(['/detail/' + item.keyword + 'child/' + item.title + '/' + item.id + '/' + item.typeid], { skipLocationChange: true });
    //             }
    //             break;

    //         case 'news':
    //             // go to news
    //             if (item.ptypeid == 0) {
    //                 this.router.navigate(['/list/' + item.keyword + '/' + item.title + '/' + item.id], { skipLocationChange: true });
    //             } else {
    //                 this.router.navigate(['/list/' + item.keyword + 'child/' + item.title + '/' + item.id], { skipLocationChange: true });
    //             }
    //             break;

    //         case 'noticeboard':
    //             // go to news
    //             // check is parent or child
    //             if (item.ptypeid == 0) {
    //                 this.router.navigate(['/list/' + item.keyword + '/' + item.title + '/' + item.id], { skipLocationChange: true });
    //             } else {
    //                 this.router.navigate(['/list/' + item.keyword + 'child/' + item.title + '/' + item.id], { skipLocationChange: true });
    //             }
    //             break;
    //     }

    //     // notification to animation on menu component
    //     this.api.sendDataService.setDataSend('true');
    // }

    // /**
    //  * on news Click 
    //  * @param url 
    //  */
    // onTitleClick(news) {
    //     const url = '/detail/news/' + news.title.replace('/', '') + '/' + news.id + '/' + news.typeid;
    //     this.router.navigate([url], { skipLocationChange: true });

    //     // notification to animation on menu component
    //     this.api.sendDataService.setDataSend('true');
    // }

    // /**
    //  * animation Change Page
    //  */
    // animationChangePage() {
    //     this.api.sendDataService.data$.subscribe(data => {
    //         // animation 
    //         setTimeout(() => {
    //             window.scroll({ left: 0, top: 0, behavior: 'smooth' });
    //         }, 100);
    //     });
    // }

    // /**
    //  * on Login Click
    //  */
    // onLoginClick() {
    //     const url = '/login';
    //     this.router.navigate([url], { skipLocationChange: true });
    // }

    // /**
    //  * on vietnamese Click
    //  */
    // onVnClick() {
    //     this.api.language = 'vn';
    //     this.api.cookie.setCookie('language', 'vn');
    //     this.language = this.api.getLanguage();

    //     // notification change language
    //     this.api.sendDataService.setDataLanguage('vn');
    // }

    // /**
    //  * on English Click
    //  */
    // onEnClick() {
    //     this.api.language = 'en';
    //     this.api.cookie.setCookie('language', 'en');
    //     this.language = this.api.getLanguage();

    //     // notification change language
    //     this.api.sendDataService.setDataLanguage('en');
    // }

    // /**
    //  * on France Click
    //  */
    // onFrClick() {
    //     this.api.language = 'fr';
    //     this.api.cookie.setCookie('language', 'fr');
    //     this.language = this.api.getLanguage();

    //     // notification change language
    //     this.api.sendDataService.setDataLanguage('fr');
    // }

    // /**
    //  * hide Or Hidden Button To Top
    //  */
    // hideOrHiddenButtonToTop() {
    //     //Get the button
    //     var mybutton = document.getElementById("myBtn");

    //     // When the user scrolls down 20px from the top of the document, show the button
    //     window.onscroll = function () { scrollFunction() };

    //     function scrollFunction() {
    //         if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    //             mybutton.style.display = "block";
    //         } else {
    //             mybutton.style.display = "none";
    //         }
    //     }
    // }

    // onToTopClick() {
    //     window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    // }
}
