import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotpasswordcompanyComponent } from './forgotpasswordcompany.component';

describe('ForgotpasswordcompanyComponent', () => {
  let component: ForgotpasswordcompanyComponent;
  let fixture: ComponentFixture<ForgotpasswordcompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgotpasswordcompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotpasswordcompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
