import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../common/api-service/api.service';
import { ForgotpasswordcompanyComponent } from './forgotpasswordcompany.component';



@NgModule({
  declarations: [ForgotpasswordcompanyComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    MatFormFieldModule,
    RouterModule.forChild([
      {
        path: '', component: ForgotpasswordcompanyComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ApiService],
})
export class ForgotpasswordcompanyModule { }


