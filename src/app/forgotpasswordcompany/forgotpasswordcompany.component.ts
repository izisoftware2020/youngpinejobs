import { Router } from "@angular/router";
import { ApiService } from "../common/api-service/api.service";
import { Subscription } from "rxjs/internal/Subscription";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { OnInit, Component } from "@angular/core";
import { Company } from "../common/models/60company.models";

@Component({
  selector: "app-forgotpasswordcompany",
  templateUrl: "./forgotpasswordcompany.component.html",
  styleUrls: ["./forgotpasswordcompany.component.scss"],
})
export class ForgotpasswordcompanyComponent implements OnInit {

  constructor(private router: Router, private api: ApiService) {}

  input: Company;

  ngOnInit(): void {
    this.input = {
      email: "",
      password: "",
    };
  }

  /**
   * on Login Click
   */
  onLoginClick() {
    this.api.excuteAllByWhat(this.input, "205").subscribe((data) => {
      console.log(this.input);

      if (data.length > 0) {
        localStorage.setItem("companySubject", JSON.stringify(data[0]));
        this.api.companySubject.next(data[0]);

        this.api.showSuccess("Đăng nhập Thành Công ");
        this.router.navigate(["/manager/company/a1-information-company"]);
      } else {
        this.api.showError("Email hoặc Password đã sai");
      }
    });
  }
}
