import { Component, OnInit } from "@angular/core";
import { Candidate } from "../common/models/20candidate.models";
import { ApiService } from "../common/api-service/api.service";
import { Router } from "@angular/router";

@Component({
    selector: "app-logincandidate",
    templateUrl: "./logincandidate.component.html",
    styleUrls: ["./logincandidate.component.scss"],
})
export class LoginCandidateComponent implements OnInit {
    constructor(private api: ApiService, private router: Router) { }

    input: Candidate;

    ngOnInit(): void {
        this.input = {
            email: "",
            password: "",
        };
    }

    onLoginClick() {
        this.api.excuteAllByWhat(this.input, "204").subscribe((data) => {
            if (data.length > 0) {
                console.log("hung", data);
                
                localStorage.setItem('candidateSubject', JSON.stringify(data[0]));
                this.api.candidateSubject.next(data[0]);

                this.api.showSuccess("Đăng nhập Thành Công ");
                this.router.navigate(['/manager/candidate/b1-infomation-personal'])
            } else {
                this.api.showError("Email hoặc Password đã sai");
            }
        });
    }
}
