import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginCandidateComponent } from './logincandidate.component';
import { RouterModule } from '@angular/router';
import { ApiService } from '../common/api-service/api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [LoginCandidateComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: LoginCandidateComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ApiService],
})
export class LogincandidateModule { }
