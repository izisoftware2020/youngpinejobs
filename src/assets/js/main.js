//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
$(document).ready(function() {
  $(".icon-menu-responsive").click(function() {
    $(".respon-menu").slideToggle();
    return false;
  });

  $(".menu-slide").click(function() {
    // $(".respon-menu>li>ul").slideUp();
    var target = $(this)
      .parent()
      .children(".slideContent");

    $(target).slideToggle();
    return false;
  });
});

$(window).resize(function() {
  if ($(document).width() >= 768) {
    $(".respon-menu").css("display", "none");
  }
});

//scroll_link e1-coaching
$(document).ready(function(){
  let scroll_link = $('.scroll');
 
   //smooth scrolling -----------------------
   scroll_link.click(function(e){
       e.preventDefault();
       let url = $('body').find($(this).attr('href')).offset().top;
       $('html, body').animate({
         scrollTop : url
       },700);
       $(this).parent().addClass('active');
       $(this).parent().siblings().removeClass('active');
       return false;	
    });
 });