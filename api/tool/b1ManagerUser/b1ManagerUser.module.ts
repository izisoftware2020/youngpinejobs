	import { NgModule } from '@angular/core';	
	import { CommonModule } from '@angular/common';	
	import { B1ManagerUserComponent, B1ManagerUserDialog } from './b1ManagerUser.component';	
import { TransferHttpCacheModule } from '@nguniversal/common';	
import { RouterModule } from '@angular/router';	
import { FormsModule, ReactiveFormsModule } from '@angular/forms';	
import {	
  MatInputModule,	
  MatDatepickerModule,	
  MatNativeDateModule,	
  MatSelectModule,	
  MatRadioModule,	
  MatDialogModule,	
  MatCardModule,	
  MatSortModule,	
  MatTableModule,	
  MatPaginatorModule	
} from '@angular/material';	
	
@NgModule({	
  declarations: [B1ManagerUserComponent, B1ManagerUserDialog],	
  imports: [	
    TransferHttpCacheModule,	
    CommonModule,	
    RouterModule.forChild([	
      {	
        path: '', component: B1ManagerUserComponent, children: [	
        ],	
      }	
    ]),	
    FormsModule,	
    ReactiveFormsModule,	
	
    MatInputModule,	
    MatDatepickerModule,	
    MatNativeDateModule,	
    MatSelectModule,	
    MatRadioModule,	
    MatDialogModule,	
    MatCardModule,	
    MatSortModule,	
    MatTableModule,	
    MatPaginatorModule,	
  ],	
  entryComponents: [B1ManagerUserDialog]	
})	
export class B1ManagerUserModule { }	
