	import { NgModule } from '@angular/core';	
	import { CommonModule } from '@angular/common';	
	import { B3ManagerTransctionComponent, B3ManagerTransctionDialog } from './b3ManagerTransction.component';	
import { TransferHttpCacheModule } from '@nguniversal/common';	
import { RouterModule } from '@angular/router';	
import { FormsModule, ReactiveFormsModule } from '@angular/forms';	
import {	
  MatInputModule,	
  MatDatepickerModule,	
  MatNativeDateModule,	
  MatSelectModule,	
  MatRadioModule,	
  MatDialogModule,	
  MatCardModule,	
  MatSortModule,	
  MatTableModule,	
  MatPaginatorModule	
} from '@angular/material';	
	
@NgModule({	
  declarations: [B3ManagerTransctionComponent, B3ManagerTransctionDialog],	
  imports: [	
    TransferHttpCacheModule,	
    CommonModule,	
    RouterModule.forChild([	
      {	
        path: '', component: B3ManagerTransctionComponent, children: [	
        ],	
      }	
    ]),	
    FormsModule,	
    ReactiveFormsModule,	
	
    MatInputModule,	
    MatDatepickerModule,	
    MatNativeDateModule,	
    MatSelectModule,	
    MatRadioModule,	
    MatDialogModule,	
    MatCardModule,	
    MatSortModule,	
    MatTableModule,	
    MatPaginatorModule,	
  ],	
  entryComponents: [B3ManagerTransctionDialog]	
})	
export class B3ManagerTransctionModule { }	
