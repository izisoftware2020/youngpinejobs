import { Component, OnInit, Inject, ViewChild } from '@angular/core';	
import { ApiService } from '../../../common/api-service/api.service';	
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';	
import { Observable, Observer, Subscription } from 'rxjs';	
	
@Component({	
  selector: 'app-b3ManagerTransction',	
  templateUrl: './b3ManagerTransction.component.html',	
  styleUrls: ['./b3ManagerTransction.component.scss']	
})	
export class B3ManagerTransctionComponent implements OnInit {	
	
  /** for table */	
  subscription: Subscription[] = [];	
	
  displayedColumns: string[] = ['id', 'idaccount', 'idstaff', 'status', 'type', 'startdate', 'money', 'comment'];	
	
  dataSource: MatTableDataSource<any>;	
	
  @ViewChild(MatPaginator) paginator: MatPaginator;	
  @ViewChild(MatSort) sort: MatSort;	
	
  applyFilter(filterValue: string) {	
    this.dataSource.filter = filterValue.trim().toLowerCase();	
	
    if (this.dataSource.paginator) {	
      this.dataSource.paginator.firstPage();	
    }	
  }	
  /** for table */	
	
  constructor(	
    private api: ApiService,	
    public dialog: MatDialog	
  ) { }	
	
	
  ngOnInit() {	
	
    // get trains	
    this.getB3ManagerTransction();	
  }	
	
  /**	
   * get Data getB3ManagerTransction  	
   */	
  getB3ManagerTransction() {	
    this.api.excuteAllByWhat({ 'idCompany': this.api.idCompany }, 'searchDataB3ManagerTransctionOfCompany')	
      .subscribe(data => {	
        data = this.api.convertToData(data);	
	
        // set data for table	
        this.dataSource = new MatTableDataSource(data);	
        this.dataSource.paginator = this.paginator;	
        this.dataSource.sort = this.sort;	
      })	
	
  }	
	
  /**	
   * on insert data	
   * @param event 	
   */	
  onInsertData() {	
    const dialogRef = this.dialog.open(B3ManagerTransctionDialog, {	
      width: '400px',	
      data: { type: 0, id: 0 }	
    });	
	
    dialogRef.afterClosed().subscribe(result => { 	
      if (result) { 	
        this.getB3ManagerTransction();	
      }	
    });	
  }	
	
  /**	
   * on update data	
   * @param event 	
   */	
  onUpdateData(row) {	
    const dialogRef = this.dialog.open(B3ManagerTransctionDialog, {	
      width: '400px',	
      data: { type: 1, input: row }	
    });	
	
    dialogRef.afterClosed().subscribe(result => { 	
      if (result) { 	
        this.getB3ManagerTransction();	
      }	
    });	
  }	
}	
	
	
/**	
 * Component show thông tin để insert hoặc update	
 */	
@Component({	
  selector: 'b3ManagerTransction-dialog',	
  templateUrl: 'b3ManagerTransction-dialog.html',	
  styleUrls: ['./b3ManagerTransction.component.scss']	
})	
export class B3ManagerTransctionDialog implements OnInit {	
	
  observable: Observable<any>;	
  observer: Observer<any>;	
  type: number;	
  idCompany: number;	
	
  // init input value	
  input: any = {	
    idaccount: '',	
    idstaff: '',	
    status: '',	
    type: '',	
    startdate: '',	
    money: '',	
    comment: '',	
  };	
	
  constructor(	
    public dialogRef: MatDialogRef<B3ManagerTransctionDialog>,	
    @Inject(MAT_DIALOG_DATA) public data: any,	
    private api: ApiService	
  ) {	
    this.type = data.type;	
    this.input.idCompany = this.api.idCompany;	
	
    // nếu là update	
    if (this.type == 1) {	
      this.input = data.input;	
    }	
	
    console.log('data nhan duoc ', this.data);	
	
    // xử lý bất đồng bộ	
    this.observable = Observable.create((observer: any) => {	
      this.observer = observer;	
    });	
  }	
	
  /**	
   * ngOnInit	
   */	
  ngOnInit() {	
	
  }	
	
	
  /**	
   * on ok click	
   */	
  onOkClick(): void {	
    // convert data time	
    // this.input.born = new Date(this.input.born);	
    // this.input.born = this.api.formatDate(this.input.born);	
	
      this.api.excuteAllByWhat(this.input, 'createDataB3ManagerTransction').subscribe(data => {	
        this.dialogRef.close(true);	
        this.api.showSuccess("Xử Lý Thành Công!");	
      });	
  }	
	
  /**	
   * onDeleteClick	
   */	
  onDeleteClick() {	
    this.api.excuteAllByWhat(this.input, 'deleteDataB3ManagerTransction').subscribe(data => {	
      this.dialogRef.close(true);	
      this.api.showSuccess("Xử Lý Xóa Thành Công!");	
    });	
  }	
}	
