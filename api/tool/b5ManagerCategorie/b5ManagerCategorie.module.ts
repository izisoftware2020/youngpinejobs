	import { NgModule } from '@angular/core';	
	import { CommonModule } from '@angular/common';	
	import { B5ManagerCategorieComponent, B5ManagerCategorieDialog } from './b5ManagerCategorie.component';	
import { TransferHttpCacheModule } from '@nguniversal/common';	
import { RouterModule } from '@angular/router';	
import { FormsModule, ReactiveFormsModule } from '@angular/forms';	
import {	
  MatInputModule,	
  MatDatepickerModule,	
  MatNativeDateModule,	
  MatSelectModule,	
  MatRadioModule,	
  MatDialogModule,	
  MatCardModule,	
  MatSortModule,	
  MatTableModule,	
  MatPaginatorModule	
} from '@angular/material';	
	
@NgModule({	
  declarations: [B5ManagerCategorieComponent, B5ManagerCategorieDialog],	
  imports: [	
    TransferHttpCacheModule,	
    CommonModule,	
    RouterModule.forChild([	
      {	
        path: '', component: B5ManagerCategorieComponent, children: [	
        ],	
      }	
    ]),	
    FormsModule,	
    ReactiveFormsModule,	
	
    MatInputModule,	
    MatDatepickerModule,	
    MatNativeDateModule,	
    MatSelectModule,	
    MatRadioModule,	
    MatDialogModule,	
    MatCardModule,	
    MatSortModule,	
    MatTableModule,	
    MatPaginatorModule,	
  ],	
  entryComponents: [B5ManagerCategorieDialog]	
})	
export class B5ManagerCategorieModule { }	
