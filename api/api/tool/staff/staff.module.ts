	import { NgModule } from '@angular/core';	
	import { CommonModule } from '@angular/common';	
	import { StaffComponent, StaffDialog } from './staff.component';	
import { TransferHttpCacheModule } from '@nguniversal/common';	
import { RouterModule } from '@angular/router';	
import { FormsModule, ReactiveFormsModule } from '@angular/forms';	
import {	
  MatInputModule,	
  MatDatepickerModule,	
  MatNativeDateModule,	
  MatSelectModule,	
  MatRadioModule,	
  MatDialogModule,	
  MatCardModule,	
  MatSortModule,	
  MatTableModule,	
  MatPaginatorModule	
} from '@angular/material';	
	
@NgModule({	
  declarations: [StaffComponent, StaffDialog],	
  imports: [	
    TransferHttpCacheModule,	
    CommonModule,	
    RouterModule.forChild([	
      {	
        path: '', component: StaffComponent, children: [	
        ],	
      }	
    ]),	
    FormsModule,	
    ReactiveFormsModule,	
	
    MatInputModule,	
    MatDatepickerModule,	
    MatNativeDateModule,	
    MatSelectModule,	
    MatRadioModule,	
    MatDialogModule,	
    MatCardModule,	
    MatSortModule,	
    MatTableModule,	
    MatPaginatorModule,	
  ],	
  entryComponents: [StaffDialog]	
})	
export class StaffModule { }	
