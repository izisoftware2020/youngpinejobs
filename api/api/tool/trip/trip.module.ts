	import { NgModule } from '@angular/core';	
	import { CommonModule } from '@angular/common';	
	import { TripComponent, TripDialog } from './trip.component';	
import { TransferHttpCacheModule } from '@nguniversal/common';	
import { RouterModule } from '@angular/router';	
import { FormsModule, ReactiveFormsModule } from '@angular/forms';	
import {	
  MatInputModule,	
  MatDatepickerModule,	
  MatNativeDateModule,	
  MatSelectModule,	
  MatRadioModule,	
  MatDialogModule,	
  MatCardModule,	
  MatSortModule,	
  MatTableModule,	
  MatPaginatorModule	
} from '@angular/material';	
	
@NgModule({	
  declarations: [TripComponent, TripDialog],	
  imports: [	
    TransferHttpCacheModule,	
    CommonModule,	
    RouterModule.forChild([	
      {	
        path: '', component: TripComponent, children: [	
        ],	
      }	
    ]),	
    FormsModule,	
    ReactiveFormsModule,	
	
    MatInputModule,	
    MatDatepickerModule,	
    MatNativeDateModule,	
    MatSelectModule,	
    MatRadioModule,	
    MatDialogModule,	
    MatCardModule,	
    MatSortModule,	
    MatTableModule,	
    MatPaginatorModule,	
  ],	
  entryComponents: [TripDialog]	
})	
export class TripModule { }	
